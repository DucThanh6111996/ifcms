use ifwcms;

CREATE TABLE `HRM_CANDIDATE` (
	`id` bigint(20) auto_increment,
    `code` varchar(50),
    `fullname` varchar(50),
    `dob` date,
    `phone` varchar(50),
    `gender` varchar(1),
    `province_id` char(6),
    `district_id` char(6),
    `ward_id` char(6),
    `avatar` bigint(20),
    primary key (`id`),
    foreign key (`avatar`) references `IMAGE` (`id`)
);

CREATE TABLE `HRM_CANDIDATE_SKILL`(
	`skill_id` bigint(20),
    `candidate_id` bigint(20),
    primary key (`skill_id`, `candidate_id`),
    foreign key (`skill_id`) references `SKILL` (`id`),
    foreign key (`candidate_id`) references `HRM_CANDIDATE` (`id`)
);

CREATE TABLE `HRM_CANDIDATE_EXPERIENCE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `company` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `level` bigint(20) DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `candidate_id` bigint(20) DEFAULT NULL,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `level` (`level`),
  KEY `candidate_id` (`candidate_id`),
  CONSTRAINT `HRM_CANDIDATE_EXPERIENCE_ibfk_1` FOREIGN KEY (`level`) REFERENCES `APP_PARAMS` (`id`),
  CONSTRAINT `HRM_CANDIDATE_EXPERIENCE_ibfk_2` FOREIGN KEY (`candidate_id`) REFERENCES `HRM_CANDIDATE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_bin

CREATE TABLE `HRM_CANDIDATE_PROJECT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `description` text COLLATE utf8_bin,
  `member` int(3) DEFAULT NULL,
  `level` bigint(20) DEFAULT NULL,
  `technology` text COLLATE utf8_bin,
  `candidate_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `level` (`level`),
  KEY `candidate_id` (`candidate_id`),
  CONSTRAINT `HRM_CANDIDATE_PROJECT_ibfk_1` FOREIGN KEY (`level`) REFERENCES `APP_PARAMS` (`id`),
  CONSTRAINT `HRM_CANDIDATE_PROJECT_ibfk_2` FOREIGN KEY (`candidate_id`) REFERENCES `HRM_CANDIDATE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_bin

CREATE TABLE `IMAGE` (
	`id` bigint(20) auto_increment,
    `path` varchar(255),
    primary key (`id`)
);

ALTER TABLE `HRM_CANDIDATE`
ADD COLUMN `email` varchar(255);

