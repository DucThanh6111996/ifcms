create table CUSTOMER (id bigint not null auto_increment, code varchar(255), name varchar(255), email varchar(255),phone varchar (20), primary key (id)) engine=MyISAM;
create table BRANCH_CUSTOMER (id bigint not null auto_increment, code varchar(255), name varchar(255), primary key (id));
alter table BRANCH_CUSTOMER add constraint FK3qewvpr31kys9c7trhukkpfg4 foreign key (customer_id) references CUSTOMER (id);
ALTER TABLE `BRANCH_CUSTOMER` CHANGE `customer_id` `customer_id` VARCHAR(20) NULL DEFAULT NULL;