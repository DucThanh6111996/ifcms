function showHidenExperience() {
    document.querySelectorAll('.experience-item').forEach(function (element, index) {
        if (element.style.display === "none") {
            element.style.display = "block";
        } else {
            element.style.display = "none";
        }
    });
    if (document.querySelectorAll('.experience-item').length == 0) {
        addExperienceForm();
    }
}
function showHidenProject() {
    document.querySelectorAll('.project-item').forEach(function (element, index) {
        if (element.style.display === "none") {
            element.style.display = "block";
        } else {
            element.style.display = "none";
        }
    });
    if (document.querySelectorAll('.project-item').length == 0) {
        addProjectForm();
    }
}
$(document).ready(function () {
    $('#cbbSkill').select2({
        placeholder: "Chọn kỹ năng",
    });
});