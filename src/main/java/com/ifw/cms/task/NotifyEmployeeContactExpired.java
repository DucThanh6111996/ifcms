package com.ifw.cms.task;

import com.ifw.cms.service.HrmEmployeeService;
import com.ifw.cms.service.MailService;
import com.ifw.cms.service.dto.HrmEmployeeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class NotifyEmployeeContactExpired {

    @Autowired
    HrmEmployeeService hrmEmployeeService;

    @Autowired
    MailService mailService;

    @Scheduled (cron = "0 30 9 1 * ?") // sending email on 9:30 on 1st of every month
    public void sendMailNotifyToEmployee(){

        // Lấy danh sách nhân viên sắp hết hạn hợp đồng
        List<HrmEmployeeDTO> listEmployeeExpired = hrmEmployeeService.getEmployeeExpired();

        // Lấy danh sách quản trị viên
        List<HrmEmployeeDTO> listAdmin = hrmEmployeeService.getAdmin();

        // Gui mail
        for (HrmEmployeeDTO admin : listAdmin) {
            mailService.sendMailExpired(admin, listEmployeeExpired);
        }
    }
}
