package com.ifw.cms.task;

import com.ifw.cms.service.CustomerService;
import com.ifw.cms.service.MailService;
import com.ifw.cms.service.dto.CustomerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class NotifyCustomerExpired {

    @Autowired
    CustomerService customerService;

    @Autowired
    MailService mailService;

    @Scheduled(cron = "0 30 9 1 * ?")
    public void sendMailNotifyToCustomer() {
        // danh sach khach hang
        List<CustomerDTO> listCustomer = customerService.getListCustomer();

       // List<CustomerDTO> listContract = customerService.getListContract();
        // Gui mail
        for (CustomerDTO customer : listCustomer) {
            mailService.sendMailCustomer(customer);
        }
    }
}
