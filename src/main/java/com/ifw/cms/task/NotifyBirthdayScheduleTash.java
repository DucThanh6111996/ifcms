package com.ifw.cms.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class NotifyBirthdayScheduleTash {

    public NotifyBirthdayScheduleTash() {
    }
    // Send mail notify birth on 8:00 AM on every 1st of month

    @Scheduled (cron = "0 0 8 1 * ?")
    public void sendMailNotifyBirthday() {
    }

}
