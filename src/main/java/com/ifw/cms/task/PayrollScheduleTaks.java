package com.ifw.cms.task;

import com.ifw.cms.entity.HrmEmployee;
import com.ifw.cms.entity.enumeration.PaymentStatus;
import com.ifw.cms.repository.HrmEmployeeRepository;
import com.ifw.cms.repository.HrmPayoutRepository;
import com.ifw.cms.service.HrmPayoutService;
import com.ifw.cms.service.MailService;
import com.ifw.cms.service.dto.HrmPayoutDTO;
import com.ifw.cms.utils.EncryptUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class PayrollScheduleTaks {
  private static final Logger log = LoggerFactory.getLogger(PayrollScheduleTaks.class);
  private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

  private final HrmEmployeeRepository hrmEmployeeRepository;
  private final HrmPayoutRepository hrmPayoutRepository;
  private final HrmPayoutService hrmPayoutService;
  private final MailService mailService;

  public PayrollScheduleTaks(HrmEmployeeRepository hrmEmployeeRepository,
                             HrmPayoutService hrmPayoutService,
                             HrmPayoutRepository hrmPayoutRepository,
                             MailService mailService) {
    this.hrmEmployeeRepository = hrmEmployeeRepository;
    this.hrmPayoutService = hrmPayoutService;
    this.hrmPayoutRepository = hrmPayoutRepository;
    this.mailService = mailService;
  }

  // Sending email on 8h:00:00 on day 1,6,11,16,21,26 of every month
  @Scheduled(cron = "0 0 8 1,6,11,16,21,26 * ?")
  //@Scheduled(fixedRate = 5000)
  public void reportCurrentTime() {
    Calendar cal = Calendar.getInstance();
    int d = cal.get(Calendar.DAY_OF_MONTH);
    int startPeriod = -1, endPeriod = -1;

    if (d >= 1 && d <= 5) {
      startPeriod = 1;
      endPeriod = 5;
    } else if (d > 5 && d <= 10) {
      startPeriod = 6;
      endPeriod = 10;
    } else if (d > 10 && d <= 15) {
      startPeriod = 11;
      endPeriod = 15;
    } else if (d > 15 && d <= 20) {
      startPeriod = 16;
      endPeriod = 20;
    } else if (d > 20 && d <= 25) {
      startPeriod = 21;
      endPeriod = 25;
    } else if (d > 25 && d <= 31) {
      startPeriod = 26;
      endPeriod = 31;
    }

    // move back to last month
    cal.add(Calendar.MONTH, -1);
    int currMonth = cal.get(Calendar.MONTH);
    currMonth = currMonth + 1;

    String prdId = String.valueOf(cal.get(Calendar.YEAR)) + String.valueOf(currMonth < 10 ? "0" + currMonth : currMonth);
    log.info("Period ID: {}", prdId);

    List<HrmEmployee> ems = hrmEmployeeRepository.findUserInPaymentPeriod(Long.valueOf(startPeriod), Long.valueOf(endPeriod));
    List<HrmEmployee> toPayList = new ArrayList();
    BigDecimal total = new BigDecimal(0);

    if (ems != null && ems.size() > 0) {
      for (HrmEmployee e : ems) {
        //check period id
        if (hrmPayoutRepository.findByPrdId(prdId, e.getCode()) < 1) {
          log.info("Employee name {}", e.getFullname());
          HrmPayoutDTO p = new HrmPayoutDTO();
          p.setEmployeeId(e.getId());
          p.setPayTo(e.getCode());
          p.setAmount(e.getSlrAmtEnc());
          p.setStatus(PaymentStatus.UNPAID);
          p.setPayType("SALARY");
          p.setDescription("TRA LUONG THANG: " + currMonth);
          p.setPrdId(prdId);

          hrmPayoutService.save(p);
          if (StringUtils.isNotEmpty(e.getSlrAmtEnc())) {
            e.setSlrAmtEnc(EncryptUtils.decrypt(e.getSlrAmtEnc()));
            total = total.add(new BigDecimal(e.getSlrAmtEnc()));
          }
          toPayList.add(e);
        }
      }

      // send email notification
      if (!toPayList.isEmpty()) {
        mailService.sendAPaymentByPeriodEmail(toPayList, total);
      }
    }
    log.info("The time is now {}", dateFormat.format(new Date()));
  }
}
