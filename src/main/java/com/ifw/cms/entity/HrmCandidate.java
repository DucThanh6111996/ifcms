package com.ifw.cms.entity;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "HRM_CANDIDATE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class HrmCandidate implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "fullname", length = 50, nullable = false)
    private String fullname;

    @NotNull
    @Size(max = 50)
    @Column(name = "code", length = 50, nullable = false)
    private String code;

    @Column(name = "dob")
    private LocalDate dob;

    @NotNull
    @Size(max = 1)
    @Column(name = "gender",length = 1, nullable = false)
    private String gender;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;

    @OneToOne
    @JoinColumn(unique = true, name = "province_id")
    private Province province;

    @OneToOne
    @JoinColumn(unique = true, name = "district_id")
    private District district;

    @OneToOne
    @JoinColumn(unique = true, name = "ward_id")
    private Ward ward;

    @OneToOne
    @JoinColumn(unique = true, name = "avatar", referencedColumnName = "id")
    private AppImage image;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "HRM_CANDIDATE_SKILL",
            joinColumns = @JoinColumn(name = "candidate_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "skill_id", referencedColumnName = "id"))
    private Set<Skill> skills = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "candidate_id")
    private Set<HrmCandidateExperience> experiences = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "candidate_id")
    private Set<HrmCandidateProject> projects = new HashSet<>();

    public HrmCandidate() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Ward getWard() {
        return ward;
    }

    public void setWard(Ward ward) {
        this.ward = ward;
    }

    public AppImage getImage() {
        return image;
    }

    public void setImage(AppImage image) {
        this.image = image;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    public Set<HrmCandidateExperience> getExperiences() {
        return experiences;
    }

    public void setExperiences(Set<HrmCandidateExperience> experiences) {
        this.experiences = experiences;
    }

    public Set<HrmCandidateProject> getProjects() {
        return projects;
    }

    public void setProjects(Set<HrmCandidateProject> projects) {
        this.projects = projects;
    }

    @Override
    public String toString() {
        return "HrmCandidate{" +
                "id=" + id +
                ", fullname='" + fullname + '\'' +
                ", code='" + code + '\'' +
                ", dob=" + dob +
                ", gender='" + gender + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", province=" + province +
                ", district=" + district +
                ", ward=" + ward +
                ", image=" + image +
                ", skills=" + skills +
                ", experiences=" + experiences +
                ", projects=" + projects +
                '}';
    }
}
