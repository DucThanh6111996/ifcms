package com.ifw.cms.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

/**
 * A HrmEmployeeDocs.
 */
@Entity
@Table(name = "HRM_EMPLOYEE_DOCS")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class HrmEmployeeDocs implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    @Column(name = "syll")
    private byte[] syll;

    @Column(name = "syll_content_type")
    private String syllContentType;

    @Lob
    @Column(name = "cmnd")
    private byte[] cmnd;

    @Column(name = "cmnd_content_type")
    private String cmndContentType;

    @Lob
    @Column(name = "sohk")
    private byte[] sohk;

    @Column(name = "sohk_content_type")
    private String sohkContentType;

    @Lob
    @Column(name = "giay_ks")
    private byte[] giayKs;

    @Column(name = "giay_ks_content_type")
    private String giayKsContentType;

    @Lob
    @Column(name = "giay_kham_sk")
    private byte[] giayKhamSk;

    @Column(name = "giay_kham_sk_content_type")
    private String giayKhamSkContentType;

    @Column(name = "khac")
    private String khac;

    @ManyToOne
    @JsonIgnoreProperties("hrmEmployeeDocs")
    private HrmEmployee employee;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getSyll() {
        return syll;
    }

    public HrmEmployeeDocs syll(byte[] syll) {
        this.syll = syll;
        return this;
    }

    public void setSyll(byte[] syll) {
        this.syll = syll;
    }

    public String getSyllContentType() {
        return syllContentType;
    }

    public HrmEmployeeDocs syllContentType(String syllContentType) {
        this.syllContentType = syllContentType;
        return this;
    }

    public void setSyllContentType(String syllContentType) {
        this.syllContentType = syllContentType;
    }

    public byte[] getCmnd() {
        return cmnd;
    }

    public HrmEmployeeDocs cmnd(byte[] cmnd) {
        this.cmnd = cmnd;
        return this;
    }

    public void setCmnd(byte[] cmnd) {
        this.cmnd = cmnd;
    }

    public String getCmndContentType() {
        return cmndContentType;
    }

    public HrmEmployeeDocs cmndContentType(String cmndContentType) {
        this.cmndContentType = cmndContentType;
        return this;
    }

    public void setCmndContentType(String cmndContentType) {
        this.cmndContentType = cmndContentType;
    }

    public byte[] getSohk() {
        return sohk;
    }

    public HrmEmployeeDocs sohk(byte[] sohk) {
        this.sohk = sohk;
        return this;
    }

    public void setSohk(byte[] sohk) {
        this.sohk = sohk;
    }

    public String getSohkContentType() {
        return sohkContentType;
    }

    public HrmEmployeeDocs sohkContentType(String sohkContentType) {
        this.sohkContentType = sohkContentType;
        return this;
    }

    public void setSohkContentType(String sohkContentType) {
        this.sohkContentType = sohkContentType;
    }

    public byte[] getGiayKs() {
        return giayKs;
    }

    public HrmEmployeeDocs giayKs(byte[] giayKs) {
        this.giayKs = giayKs;
        return this;
    }

    public void setGiayKs(byte[] giayKs) {
        this.giayKs = giayKs;
    }

    public String getGiayKsContentType() {
        return giayKsContentType;
    }

    public HrmEmployeeDocs giayKsContentType(String giayKsContentType) {
        this.giayKsContentType = giayKsContentType;
        return this;
    }

    public void setGiayKsContentType(String giayKsContentType) {
        this.giayKsContentType = giayKsContentType;
    }

    public byte[] getGiayKhamSk() {
        return giayKhamSk;
    }

    public HrmEmployeeDocs giayKhamSk(byte[] giayKhamSk) {
        this.giayKhamSk = giayKhamSk;
        return this;
    }

    public void setGiayKhamSk(byte[] giayKhamSk) {
        this.giayKhamSk = giayKhamSk;
    }

    public String getGiayKhamSkContentType() {
        return giayKhamSkContentType;
    }

    public HrmEmployeeDocs giayKhamSkContentType(String giayKhamSkContentType) {
        this.giayKhamSkContentType = giayKhamSkContentType;
        return this;
    }

    public void setGiayKhamSkContentType(String giayKhamSkContentType) {
        this.giayKhamSkContentType = giayKhamSkContentType;
    }

    public String getKhac() {
        return khac;
    }

    public HrmEmployeeDocs khac(String khac) {
        this.khac = khac;
        return this;
    }

    public void setKhac(String khac) {
        this.khac = khac;
    }

    public HrmEmployee getEmployee() {
        return employee;
    }

    public HrmEmployeeDocs employee(HrmEmployee hrmEmployee) {
        this.employee = hrmEmployee;
        return this;
    }

    public void setEmployee(HrmEmployee hrmEmployee) {
        this.employee = hrmEmployee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HrmEmployeeDocs)) {
            return false;
        }
        return id != null && id.equals(((HrmEmployeeDocs) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "HrmEmployeeDocs{" +
            "id=" + getId() +
            ", syll='" + getSyll() + "'" +
            ", syllContentType='" + getSyllContentType() + "'" +
            ", cmnd='" + getCmnd() + "'" +
            ", cmndContentType='" + getCmndContentType() + "'" +
            ", sohk='" + getSohk() + "'" +
            ", sohkContentType='" + getSohkContentType() + "'" +
            ", giayKs='" + getGiayKs() + "'" +
            ", giayKsContentType='" + getGiayKsContentType() + "'" +
            ", giayKhamSk='" + getGiayKhamSk() + "'" +
            ", giayKhamSkContentType='" + getGiayKhamSkContentType() + "'" +
            ", khac='" + getKhac() + "'" +
            "}";
    }
}
