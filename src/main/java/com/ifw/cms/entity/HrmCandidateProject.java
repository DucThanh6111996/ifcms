package com.ifw.cms.entity;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "HRM_CANDIDATE_PROJECT")
@Cache(usage  = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class HrmCandidateProject {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 255, min = 1)
    @JoinColumn(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "member")
    private Integer member;

    @OneToOne
    @JoinColumn(unique = true, name = "level")
    private AppParams level;

    @JoinColumn(name = "technology")
    private String technology;

    @ManyToOne
    @JoinColumn(name = "candidate_id")
    private HrmCandidate hrmCandidate;

    public HrmCandidateProject() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getMember() {
        return member;
    }

    public void setMember(Integer member) {
        this.member = member;
    }

    public AppParams getLevel() {
        return level;
    }

    public void setLevel(AppParams level) {
        this.level = level;
    }

    public HrmCandidate getHrmCandidate() {
        return hrmCandidate;
    }

    public void setHrmCandidate(HrmCandidate hrmCandidate) {
        this.hrmCandidate = hrmCandidate;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }
}
