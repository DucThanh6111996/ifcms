package com.ifw.cms.entity;


import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * A CmActionLog.
 */
@Entity
@Table(name = "CM_ACTION_LOG")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CmActionLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 20)
    @Column(name = "action_type", length = 20, nullable = false)
    private String actionType;

    @Size(max = 20)
    @Column(name = "action_group", length = 20)
    private String actionGroup;

    @NotNull
    @Size(max = 20)
    @Column(name = "object_type", length = 20, nullable = false)
    private String objectType;

    @NotNull
    @Size(max = 20)
    @Column(name = "object_id", length = 20, nullable = false)
    private String objectId;

    @Size(max = 255)
    @Column(name = "old_value", length = 255)
    private String oldValue;

    @Size(max = 255)
    @Column(name = "new_value", length = 255)
    private String newValue;

    @Size(max = 255)
    @Column(name = "extra_value", length = 255)
    private String extraValue;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActionType() {
        return actionType;
    }

    public CmActionLog actionType(String actionType) {
        this.actionType = actionType;
        return this;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getActionGroup() {
        return actionGroup;
    }

    public CmActionLog actionGroup(String actionGroup) {
        this.actionGroup = actionGroup;
        return this;
    }

    public void setActionGroup(String actionGroup) {
        this.actionGroup = actionGroup;
    }

    public String getObjectType() {
        return objectType;
    }

    public CmActionLog objectType(String objectType) {
        this.objectType = objectType;
        return this;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectId() {
        return objectId;
    }

    public CmActionLog objectId(String objectId) {
        this.objectId = objectId;
        return this;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getOldValue() {
        return oldValue;
    }

    public CmActionLog oldValue(String oldValue) {
        this.oldValue = oldValue;
        return this;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public CmActionLog newValue(String newValue) {
        this.newValue = newValue;
        return this;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getExtraValue() {
        return extraValue;
    }

    public CmActionLog extraValue(String extraValue) {
        this.extraValue = extraValue;
        return this;
    }

    public void setExtraValue(String extraValue) {
        this.extraValue = extraValue;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CmActionLog)) {
            return false;
        }
        return id != null && id.equals(((CmActionLog) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "CmActionLog{" +
            "id=" + getId() +
            ", actionType='" + getActionType() + "'" +
            ", actionGroup='" + getActionGroup() + "'" +
            ", objectType='" + getObjectType() + "'" +
            ", objectId='" + getObjectId() + "'" +
            ", oldValue='" + getOldValue() + "'" +
            ", newValue='" + getNewValue() + "'" +
            ", extraValue='" + getExtraValue() + "'" +
            "}";
    }
}
