package com.ifw.cms.entity;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "CUSTOMER")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    @Size(max = 50)
    @Column(name = "code")
    private String code;

    @NotEmpty
    @Size(max = 50)
    @Column(name = "name")
    private String name;

    @NotEmpty
    @Size(max = 50)
    @Column(name = "email")
    private String email;

    @NotEmpty
    @Size(max = 50)
    @Column(name = "phone")
    private String phone;


    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "customer_id")
    private Set<CustomerBranch> customerBranches = new HashSet<>();

    public Customer() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Set<CustomerBranch> getCustomerBranches() {
        return customerBranches;
    }

    public void setCustomerBranches(Set<CustomerBranch> customerBranches) {
        this.customerBranches = customerBranches;
    }
}
