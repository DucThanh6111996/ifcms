package com.ifw.cms.entity.enumeration;

/**
 * The PaymentStatus enumeration.
 */
public enum PaymentStatus {
    PAID, UNPAID, PENDING, HOLD
}
