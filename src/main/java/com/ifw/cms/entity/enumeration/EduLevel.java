package com.ifw.cms.entity.enumeration;

/**
 * The EduLevel enumeration.
 */
public enum EduLevel {
    TRUNGCAP, CAODANG, DAIHOC, KHAC
}
