package com.ifw.cms.entity.enumeration;

/**
 * The SalaryPaymentType enumeration.
 */
public enum SalaryPaymentType {
    GROSS, NET
}
