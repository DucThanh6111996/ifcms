package com.ifw.cms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Dashboard {
  @Id
  private Long id;
  @Column(name = "total_emp")
  private Long totalEmp;
  @Column(name = "total_emp_female")
  private Long totalEmpFemale;
  @Column(name = "total_emp_male")
  private Long totalEmpMale;
  @Column(name = "total_dev")
  private Long totalDev;
  @Column(name = "total_test")
  private Long totalTest;
  @Column(name = "total_office")
  private Long totalOffice;
  @Column(name = "kei")
  private String kei;
  @Column(name = "val")
  private String val;

  /**
   * @return the totalEmp
   */
  public Long getTotalEmp() {
    return totalEmp;
  }

  /**
   * @param totalEmp the totalEmp to set
   */
  public void setTotalEmp(Long totalEmp) {
    this.totalEmp = totalEmp;
  }

  /**
   * @return the totalEmpFemale
   */
  public Long getTotalEmpFemale() {
    return totalEmpFemale;
  }

  /**
   * @param totalEmpFemale the totalEmpFemale to set
   */
  public void setTotalEmpFemale(Long totalEmpFemale) {
    this.totalEmpFemale = totalEmpFemale;
  }

  /**
   * @return the totalEmpMale
   */
  public Long getTotalEmpMale() {
    return totalEmpMale;
  }

  /**
   * @param totalEmpMale the totalEmpMale to set
   */
  public void setTotalEmpMale(Long totalEmpMale) {
    this.totalEmpMale = totalEmpMale;
  }

  /**
   * @return the totalDev
   */
  public Long getTotalDev() {
    return totalDev;
  }

  /**
   * @param totalDev the totalDev to set
   */
  public void setTotalDev(Long totalDev) {
    this.totalDev = totalDev;
  }

  /**
   * @return the totalTest
   */
  public Long getTotalTest() {
    return totalTest;
  }

  /**
   * @param totalTest the totalTest to set
   */
  public void setTotalTest(Long totalTest) {
    this.totalTest = totalTest;
  }

  /**
   * @return the totalOffice
   */
  public Long getTotalOffice() {
    return totalOffice;
  }

  /**
   * @param totalOffice the totalOffice to set
   */
  public void setTotalOffice(Long totalOffice) {
    this.totalOffice = totalOffice;
  }

  /**
   * @return the kei
   */
  public String getKei() {
    return kei;
  }

  /**
   * @param kei the kei to set
   */
  public void setKei(String kei) {
    this.kei = kei;
  }

  /**
   * @return the val
   */
  public String getVal() {
    return val;
  }

  /**
   * @param val the val to set
   */
  public void setVal(String val) {
    this.val = val;
  }


}
