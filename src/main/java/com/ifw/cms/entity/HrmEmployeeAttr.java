package com.ifw.cms.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A HrmEmployeeAttr.
 */
@Entity
@Table(name = "HRM_EMPLOYEE_ATTR")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class HrmEmployeeAttr implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 10)
    @Column(name = "jhi_key", length = 10, nullable = false)
    private String key;

    @NotNull
    @Size(max = 500)
    @Column(name = "jhi_value", length = 500, nullable = false)
    private String value;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "hrm_employee_attr_param",
               joinColumns = @JoinColumn(name = "hrm_employee_attr_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "param_id", referencedColumnName = "id"))
    private Set<AppParams> params = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("attrs")
    private HrmEmployee employeeId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public HrmEmployeeAttr key(String key) {
        this.key = key;
        return this;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public HrmEmployeeAttr value(String value) {
        this.value = value;
        return this;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Set<AppParams> getParams() {
        return params;
    }

    public HrmEmployeeAttr params(Set<AppParams> appParams) {
        this.params = appParams;
        return this;
    }

    public HrmEmployeeAttr addParam(AppParams appParams) {
        this.params.add(appParams);
        return this;
    }

    public HrmEmployeeAttr removeParam(AppParams appParams) {
        this.params.remove(appParams);
        return this;
    }

    public void setParams(Set<AppParams> appParams) {
        this.params = appParams;
    }

    public HrmEmployee getEmployeeId() {
        return employeeId;
    }

    public HrmEmployeeAttr employeeId(HrmEmployee hrmEmployee) {
        this.employeeId = hrmEmployee;
        return this;
    }

    public void setEmployeeId(HrmEmployee hrmEmployee) {
        this.employeeId = hrmEmployee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HrmEmployeeAttr)) {
            return false;
        }
        return id != null && id.equals(((HrmEmployeeAttr) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "HrmEmployeeAttr{" +
            "id=" + getId() +
            ", key='" + getKey() + "'" +
            ", value='" + getValue() + "'" +
            "}";
    }
}
