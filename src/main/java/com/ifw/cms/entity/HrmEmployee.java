package com.ifw.cms.entity;


import com.ifw.cms.entity.enumeration.EduLevel;
import com.ifw.cms.entity.enumeration.SalaryPaymentType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A HrmEmployee.
 */
@Entity
@Table(name = "HRM_EMPLOYEE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class HrmEmployee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 50)
    @Column(name = "code", length = 50, nullable = false)
    private String code;

    @NotNull
    @Size(max = 255)
    @Column(name = "fullname", length = 255, nullable = false)
    private String fullname;

    @Enumerated(EnumType.STRING)
    @Column(name = "sal_pmt_type")
    private SalaryPaymentType salPmtType;

    @Column(name = "support_amt", precision = 21, scale = 2)
    private BigDecimal supportAmt;

    @Column(name = "cnt_vld_frm_dt")
    private LocalDate cntVldFrmDt;

    @Column(name = "cnt_vld_to_dt")
    private LocalDate cntVldToDt;

    @Column(name = "slr_before")
    private String slrBefore;

    @Column(name = "slr_amt_enc")
    private String slrAmtEnc;

    @Column(name = "dob")
    private LocalDate dob;

    @Enumerated(EnumType.STRING)
    @Column(name = "edu")
    private EduLevel edu;

    @NotNull
    @Size(max = 1)
    @Column(name = "gender", length = 1, nullable = false)
    private String gender;

    @Size(max = 50)
    @Column(name = "id_no", length = 50)
    private String idNo;

    @Column(name = "id_issue_dtm")
    private LocalDate idIssueDtm;

    @NotNull
    @Size(max = 1)
    @Column(name = "status", length = 1, nullable = false)
    private String status;

    @Size(max = 50)
    @Column(name = "email", length = 50)
    private String email;

    @Size(max = 50)
    @Column(name = "phone", length = 50)
    private String phone;

    @Size(max = 50)
    @Column(name = "phone_2", length = 50)
    private String phone2;

    @Size(max = 50)
    @Column(name = "account_no", length = 50)
    private String accountNo;

    @Column(name = "salary_date")
    private Long salaryDate;

    @Column(name = "is_insource", length = 1)
    private Boolean isInsource;

    @Size(max = 50)
    @Column(name = "tax_no", length = 50)
    private String taxNo;

    @Column(name = "tgtg_bhxh")
    private Long tgtgBhxh;

    @Size(max = 50)
    @Column(name = "bhxh_no", length = 50)
    private String bhxhNo;

    @Size(max = 50)
    @Column(name = "bhyt_no", length = 50)
    private String bhytNo;

    @Size(max = 50)
    @Column(name = "bh_that_nghiep", length = 50)
    private String bhThatNghiep;

    @Column(name = "last_update")
    private LocalDate lastUpdate;

    @Size(max = 50)
    @Column(name = "update_by", length = 50)
    private String updateBy;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "cv")
    private String cv;

    @Column(name = "cv_content_type")
    private String cvContentType;

    @OneToOne
    @JoinColumn(unique = true, name = "level_id")
    private AppParams level;

    @OneToOne
    @JoinColumn(unique = true, name = "role_id")
    private AppParams role;

    @OneToOne
    @JoinColumn(unique = true, name = "branch_id")
    private AppParams branch;

    @OneToOne
    @JoinColumn(unique = true, name = "unit_id")
    private AppParams unit;

    @OneToOne
    @JoinColumn(unique = true, name = "contract_type_id")
    private AppParams contractType;

    @OneToOne
    @JoinColumn(unique = true, name = "perm_province_id")
    private CmProvince permProvince;

    @OneToOne
    @JoinColumn(unique = true, name = "perm_district_id")
    private CmProvince permDistrict;

    @OneToOne
    @JoinColumn(unique = true, name = "perm_town_id")
    private CmProvince permTown;

    @OneToMany(mappedBy = "employee")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<HrmEmployeeDocs> hrmEmployeeDocs = new HashSet<>();

    @OneToMany(mappedBy = "employee")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<HrmPayout> hrmPayouts = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "HRM_EMPLOYEE_SKILL",
            joinColumns = @JoinColumn(name = "hrm_employee_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "skill_id", referencedColumnName = "id"))
    private Set<Skill> skills = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public HrmEmployee code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFullname() {
        return fullname;
    }

    public HrmEmployee fullname(String fullname) {
        this.fullname = fullname;
        return this;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public SalaryPaymentType getSalPmtType() {
        return salPmtType;
    }

    public HrmEmployee salPmtType(SalaryPaymentType salPmtType) {
        this.salPmtType = salPmtType;
        return this;
    }

    public void setSalPmtType(SalaryPaymentType salPmtType) {
        this.salPmtType = salPmtType;
    }

    public BigDecimal getSupportAmt() {
        return supportAmt;
    }

    public HrmEmployee supportAmt(BigDecimal supportAmt) {
        this.supportAmt = supportAmt;
        return this;
    }

    public void setSupportAmt(BigDecimal supportAmt) {
        this.supportAmt = supportAmt;
    }

    public LocalDate getCntVldFrmDt() {
        return cntVldFrmDt;
    }

    public HrmEmployee cntVldFrmDt(LocalDate cntVldFrmDt) {
        this.cntVldFrmDt = cntVldFrmDt;
        return this;
    }

    public void setCntVldFrmDt(LocalDate cntVldFrmDt) {
        this.cntVldFrmDt = cntVldFrmDt;
    }

    public LocalDate getCntVldToDt() {
        return cntVldToDt;
    }

    public HrmEmployee cntVldToDt(LocalDate cntVldToDt) {
        this.cntVldToDt = cntVldToDt;
        return this;
    }

    public void setCntVldToDt(LocalDate cntVldToDt) {
        this.cntVldToDt = cntVldToDt;
    }

    public String getSlrBefore() {
        return slrBefore;
    }

    public HrmEmployee slrBefore(String slrBefore) {
        this.slrBefore = slrBefore;
        return this;
    }

    public void setSlrBefore(String slrBefore) {
        this.slrBefore = slrBefore;
    }

    public String getSlrAmtEnc() {
        return slrAmtEnc;
    }

    public HrmEmployee slrAmtEnc(String slrAmtEnc) {
        this.slrAmtEnc = slrAmtEnc;
        return this;
    }

    public void setSlrAmtEnc(String slrAmtEnc) {
        this.slrAmtEnc = slrAmtEnc;
    }

    public LocalDate getDob() {
        return dob;
    }

    public HrmEmployee dob(LocalDate dob) {
        this.dob = dob;
        return this;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public EduLevel getEdu() {
        return edu;
    }

    public HrmEmployee edu(EduLevel edu) {
        this.edu = edu;
        return this;
    }

    public void setEdu(EduLevel edu) {
        this.edu = edu;
    }

    public String getGender() {
        return gender;
    }

    public HrmEmployee gender(String gender) {
        this.gender = gender;
        return this;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIdNo() {
        return idNo;
    }

    public HrmEmployee idNo(String idNo) {
        this.idNo = idNo;
        return this;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public LocalDate getIdIssueDtm() {
        return idIssueDtm;
    }

    public HrmEmployee idIssueDtm(LocalDate idIssueDtm) {
        this.idIssueDtm = idIssueDtm;
        return this;
    }

    public void setIdIssueDtm(LocalDate idIssueDtm) {
        this.idIssueDtm = idIssueDtm;
    }

    public String getStatus() {
        return status;
    }

    public HrmEmployee status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public HrmEmployee email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public HrmEmployee phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone2() {
        return phone2;
    }

    public HrmEmployee phone2(String phone2) {
        this.phone2 = phone2;
        return this;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public Long getSalaryDate() {
        return salaryDate;
    }

    public HrmEmployee salaryDate(Long salaryDate) {
        this.salaryDate = salaryDate;
        return this;
    }

    public void setSalaryDate(Long salaryDate) {
        this.salaryDate = salaryDate;
    }

    public Boolean getIsInsource() {
        return isInsource;
    }

    public HrmEmployee isInsource(Boolean isInsource) {
        this.isInsource = isInsource;
        return this;
    }

    public void setIsInsource(Boolean isInsource) {
        this.isInsource = isInsource;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public HrmEmployee taxNo(String taxNo) {
        this.taxNo = taxNo;
        return this;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public Long getTgtgBhxh() {
        return tgtgBhxh;
    }

    public HrmEmployee tgtgBhxh(Long tgtgBhxh) {
        this.tgtgBhxh = tgtgBhxh;
        return this;
    }

    public void setTgtgBhxh(Long tgtgBhxh) {
        this.tgtgBhxh = tgtgBhxh;
    }

    public String getBhxhNo() {
        return bhxhNo;
    }

    public HrmEmployee bhxhNo(String bhxhNo) {
        this.bhxhNo = bhxhNo;
        return this;
    }

    public void setBhxhNo(String bhxhNo) {
        this.bhxhNo = bhxhNo;
    }

    public String getBhytNo() {
        return bhytNo;
    }

    public HrmEmployee bhytNo(String bhytNo) {
        this.bhytNo = bhytNo;
        return this;
    }

    public void setBhytNo(String bhytNo) {
        this.bhytNo = bhytNo;
    }

    public String getBhThatNghiep() {
        return bhThatNghiep;
    }

    public HrmEmployee bhThatNghiep(String bhThatNghiep) {
        this.bhThatNghiep = bhThatNghiep;
        return this;
    }

    public void setBhThatNghiep(String bhThatNghiep) {
        this.bhThatNghiep = bhThatNghiep;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public HrmEmployee lastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
        return this;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public HrmEmployee updateBy(String updateBy) {
        this.updateBy = updateBy;
        return this;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public HrmEmployee endDate(LocalDate endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getCv() {
        return cv;
    }

    public HrmEmployee cv(String cv) {
        this.cv = cv;
        return this;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public String getCvContentType() {
        return cvContentType;
    }

    public HrmEmployee cvContentType(String cvContentType) {
        this.cvContentType = cvContentType;
        return this;
    }

    public void setCvContentType(String cvContentType) {
        this.cvContentType = cvContentType;
    }

    public AppParams getLevel() {
        return level;
    }

    public HrmEmployee level(AppParams appParams) {
        this.level = appParams;
        return this;
    }

    public void setLevel(AppParams appParams) {
        this.level = appParams;
    }

    public AppParams getRole() {
        return role;
    }

    public HrmEmployee role(AppParams appParams) {
        this.role = appParams;
        return this;
    }

    public void setRole(AppParams appParams) {
        this.role = appParams;
    }

    public AppParams getBranch() {
        return branch;
    }

    public HrmEmployee branch(AppParams appParams) {
        this.branch = appParams;
        return this;
    }

    public void setBranch(AppParams appParams) {
        this.branch = appParams;
    }

    public AppParams getUnit() {
        return unit;
    }

    public HrmEmployee unit(AppParams appParams) {
        this.unit = appParams;
        return this;
    }

    public void setUnit(AppParams appParams) {
        this.unit = appParams;
    }

    public AppParams getContractType() {
        return contractType;
    }

    public HrmEmployee contractType(AppParams appParams) {
        this.contractType = appParams;
        return this;
    }

    public void setContractType(AppParams appParams) {
        this.contractType = appParams;
    }

    public CmProvince getPermProvince() {
        return permProvince;
    }

    public HrmEmployee permProvince(CmProvince cmProvince) {
        this.permProvince = cmProvince;
        return this;
    }

    public void setPermProvince(CmProvince cmProvince) {
        this.permProvince = cmProvince;
    }

    public CmProvince getPermDistrict() {
        return permDistrict;
    }

    public HrmEmployee permDistrict(CmProvince cmProvince) {
        this.permDistrict = cmProvince;
        return this;
    }

    public void setPermDistrict(CmProvince cmProvince) {
        this.permDistrict = cmProvince;
    }

    public CmProvince getPermTown() {
        return permTown;
    }

    public HrmEmployee permTown(CmProvince cmProvince) {
        this.permTown = cmProvince;
        return this;
    }

    public void setPermTown(CmProvince cmProvince) {
        this.permTown = cmProvince;
    }

    public Set<HrmEmployeeDocs> getHrmEmployeeDocs() {
        return hrmEmployeeDocs;
    }

    public HrmEmployee hrmEmployeeDocs(Set<HrmEmployeeDocs> hrmEmployeeDocs) {
        this.hrmEmployeeDocs = hrmEmployeeDocs;
        return this;
    }

    public HrmEmployee addHrmEmployeeDocs(HrmEmployeeDocs hrmEmployeeDocs) {
        this.hrmEmployeeDocs.add(hrmEmployeeDocs);
        hrmEmployeeDocs.setEmployee(this);
        return this;
    }

    public HrmEmployee removeHrmEmployeeDocs(HrmEmployeeDocs hrmEmployeeDocs) {
        this.hrmEmployeeDocs.remove(hrmEmployeeDocs);
        hrmEmployeeDocs.setEmployee(null);
        return this;
    }

    public void setHrmEmployeeDocs(Set<HrmEmployeeDocs> hrmEmployeeDocs) {
        this.hrmEmployeeDocs = hrmEmployeeDocs;
    }

    public Set<HrmPayout> getHrmPayouts() {
        return hrmPayouts;
    }

    public HrmEmployee hrmPayouts(Set<HrmPayout> hrmPayouts) {
        this.hrmPayouts = hrmPayouts;
        return this;
    }

    public HrmEmployee addHrmPayout(HrmPayout hrmPayout) {
        this.hrmPayouts.add(hrmPayout);
        hrmPayout.setEmployee(this);
        return this;
    }

    public HrmEmployee removeHrmPayout(HrmPayout hrmPayout) {
        this.hrmPayouts.remove(hrmPayout);
        hrmPayout.setEmployee(null);
        return this;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public void setHrmPayouts(Set<HrmPayout> hrmPayouts) {
        this.hrmPayouts = hrmPayouts;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public HrmEmployee skills(Set<Skill> skills) {
        this.skills = skills;
        return this;
    }

    public HrmEmployee addSkill(Skill skill) {
        this.skills.add(skill);
        skill.getEmployees().add(this);
        return this;
    }

    public HrmEmployee removeSkill(Skill skill) {
        this.skills.remove(skill);
        skill.getEmployees().remove(this);
        return this;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HrmEmployee)) {
            return false;
        }
        return id != null && id.equals(((HrmEmployee) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "HrmEmployee{" +
                "id=" + getId() +
                ", code='" + getCode() + "'" +
                ", fullname='" + getFullname() + "'" +
                ", salPmtType='" + getSalPmtType() + "'" +
                ", supportAmt=" + getSupportAmt() +
                ", cntVldFrmDt='" + getCntVldFrmDt() + "'" +
                ", cntVldToDt='" + getCntVldToDt() + "'" +
                ", slrBefore='" + getSlrBefore() + "'" +
                ", slrAmtEnc='" + getSlrAmtEnc() + "'" +
                ", dob='" + getDob() + "'" +
                ", edu='" + getEdu() + "'" +
                ", gender='" + getGender() + "'" +
                ", idNo='" + getIdNo() + "'" +
                ", idIssueDtm='" + getIdIssueDtm() + "'" +
                ", status='" + getStatus() + "'" +
                ", email='" + getEmail() + "'" +
                ", phone='" + getPhone() + "'" +
                ", phone2='" + getPhone2() + "'" +
                ", salaryDate='" + getSalaryDate() + "'" +
                ", isInsource='" + getIsInsource() + "'" +
                ", taxNo='" + getTaxNo() + "'" +
                ", tgtgBhxh=" + getTgtgBhxh() +
                ", bhxhNo='" + getBhxhNo() + "'" +
                ", bhytNo='" + getBhytNo() + "'" +
                ", bhThatNghiep='" + getBhThatNghiep() + "'" +
                ", lastUpdate='" + getLastUpdate() + "'" +
                ", updateBy='" + getUpdateBy() + "'" +
                ", endDate='" + getEndDate() + "'" +
                ", cv='" + getCv() + "'" +
                ", cvContentType='" + getCvContentType() + "'" +
                "}";
    }
}
