package com.ifw.cms.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class EmployeePayout {
  @Id
  private String id;
  @Column(name = "fullname")
  private String fullname;
  @Column(name = "code")
  private String code;
  @Column(name = "email")
  private String email;
  @Column(name = "phone")
  private String phone;
  @Column(name = "status")
  private String status;
  @Column(name = "pay_type")
  private String pay_type;
  @Column(name = "prd_id")
  private String prdId;
  @Column(name = "description")
  private String description;
  @Column(name = "amount")
  private String amount;
  @Column(name = "salary_date")
  private String salaryDate;

  /**
   * @return the id
   */
  public String getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(String id) {
    this.id = id;
  }

  /**
   * @return the fullname
   */
  public String getFullname() {
    return fullname;
  }

  /**
   * @param fullname the fullname to set
   */
  public void setFullname(String fullname) {
    this.fullname = fullname;
  }

  /**
   * @return the code
   */
  public String getCode() {
    return code;
  }

  /**
   * @param code the code to set
   */
  public void setCode(String code) {
    this.code = code;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the phone
   */
  public String getPhone() {
    return phone;
  }

  /**
   * @param phone the phone to set
   */
  public void setPhone(String phone) {
    this.phone = phone;
  }

  /**
   * @return the status
   */
  public String getStatus() {
    return status;
  }

  /**
   * @param status the status to set
   */
  public void setStatus(String status) {
    this.status = status;
  }

  /**
   * @return the pay_type
   */
  public String getPay_type() {
    return pay_type;
  }

  /**
   * @param pay_type the pay_type to set
   */
  public void setPay_type(String pay_type) {
    this.pay_type = pay_type;
  }

  /**
   * @return the prdId
   */
  public String getPrdId() {
    return prdId;
  }

  /**
   * @param prdId the prdId to set
   */
  public void setPrdId(String prdId) {
    this.prdId = prdId;
  }

  /**
   * @return the description
   */
  public String getDescription() {
    return description;
  }

  /**
   * @param description the description to set
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @return the amount
   */
  public String getAmount() {
    return amount;
  }

  /**
   * @param amount the amount to set
   */
  public void setAmount(String amount) {
    this.amount = amount;
  }

  /**
   * @return the salaryDate
   */
  public String getSalaryDate() {
    return salaryDate;
  }

  /**
   * @param salaryDate the salaryDate to set
   */
  public void setSalaryDate(String salaryDate) {
    this.salaryDate = salaryDate;
  }

}
