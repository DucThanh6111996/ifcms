package com.ifw.cms.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.ifw.cms.entity.enumeration.PaymentStatus;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * A HrmPayout.
 */
@Entity
@Table(name = "HRM_PAYOUT")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class HrmPayout implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "pay_type", nullable = false)
    private String payType;

    @NotNull
    @Column(name = "pay_to", nullable = false)
    private String payTo;

    @NotNull
    @Column(name = "amount", nullable = false)
    private String amount;

    @Column(name = "prd_id")
    private String prdId;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private PaymentStatus status;

    @Column(name = "description")
    private String description;

    @ManyToOne
    @JsonIgnoreProperties("hrmPayouts")
    private HrmEmployee employee;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPayType() {
        return payType;
    }

    public HrmPayout payType(String payType) {
        this.payType = payType;
        return this;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPayTo() {
        return payTo;
    }

    public HrmPayout payTo(String payTo) {
        this.payTo = payTo;
        return this;
    }

    public void setPayTo(String payTo) {
        this.payTo = payTo;
    }

    public String getAmount() {
        return amount;
    }

    public HrmPayout amount(String amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPrdId() {
        return prdId;
    }

    public HrmPayout prdId(String prdId) {
        this.prdId = prdId;
        return this;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public PaymentStatus getStatus() {
        return status;
    }

    public HrmPayout status(PaymentStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(PaymentStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public HrmPayout description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public HrmEmployee getEmployee() {
        return employee;
    }

    public HrmPayout employee(HrmEmployee hrmEmployee) {
        this.employee = hrmEmployee;
        return this;
    }

    public void setEmployee(HrmEmployee hrmEmployee) {
        this.employee = hrmEmployee;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HrmPayout)) {
            return false;
        }
        return id != null && id.equals(((HrmPayout) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "HrmPayout{" +
            "id=" + getId() +
            ", payType='" + getPayType() + "'" +
            ", payTo='" + getPayTo() + "'" +
            ", amount='" + getAmount() + "'" +
            ", prdId='" + getPrdId() + "'" +
            ", status='" + getStatus() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
