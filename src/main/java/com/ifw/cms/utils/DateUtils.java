package com.ifw.cms.utils;

import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateUtils {
  public static final String DATE_FORMAT_DDMMYYYY = "dd/MM/yyyy";

  /**
   * Convert String to LocalDate
   * @param pattern
   * @param date
   * @return
   */
  public static LocalDate convertStringToLocalDate(String pattern, String date) {
    if (StringUtils.isEmpty(date) || date.trim().length() != pattern.trim().length()) {
      return null;
    }

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT_DDMMYYYY);

    return LocalDate.parse(date, formatter);
  }
}
