package com.ifw.cms.filestorage;

import java.nio.file.Path;
import java.util.stream.Stream;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by ITSOL SWIB Dev Team.
 */
public interface FileStorage {
    public void store(MultipartFile file, String newName);
    public Resource loadFile(String filename);
    public void storeAvatar(MultipartFile file, String name);
    public Resource loadAvatar(String name);
    public void deleteAll();
    public void init();
    public Stream<Path> loadFiles();
}
