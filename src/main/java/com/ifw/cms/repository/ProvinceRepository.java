package com.ifw.cms.repository;


import com.ifw.cms.entity.Province;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Skill entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProvinceRepository extends JpaRepository<Province, Long> {
    List<Province> findAllByIdOrName(Long id, String name);
    Optional<Province> findById(Long id);
}
