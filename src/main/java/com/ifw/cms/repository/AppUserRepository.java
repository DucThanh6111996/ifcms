package com.ifw.cms.repository;

import com.ifw.cms.entity.AppUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by ITSOL SWIB Dev Team.
 */
@Repository
public interface AppUserRepository extends PagingAndSortingRepository<AppUser, Long> {
    @Query(value = "select u from AppUser u where lower(u.userName) like concat('%',lower(:key), '%') order by u.userName")
    Page<AppUser> findPaginatedUsers(Pageable pageable, @Param("key") String key);

    @Query(value = "select u from AppUser u where u.userName = :userName")
    AppUser findByUserName(@Param("userName") String userName);
}
