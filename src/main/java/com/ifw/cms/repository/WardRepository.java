package com.ifw.cms.repository;

import com.ifw.cms.entity.Ward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Skill entity.
 */
@SuppressWarnings("unused")
@Repository
public interface WardRepository extends JpaRepository<Ward, Long> {

    @Query(value = "select p from Ward p where p.district.id = :districtId order by p.name asc")
    List<Ward> findByDistrictId(@Param("districtId") Long districtId);
    
    Optional<Ward> findById(Long id);

}
