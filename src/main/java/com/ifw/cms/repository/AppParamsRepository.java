package com.ifw.cms.repository;

import com.ifw.cms.entity.AppParams;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the AppParams entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AppParamsRepository extends JpaRepository<AppParams, Long> {
  @Query("SELECT a FROM AppParams a WHERE a.groupCode = :groupCode and a.status = 'A' ORDER BY a.name")
  List<AppParams> findByGroupCode(@Param("groupCode") String groupCode);

  @Query("SELECT a FROM AppParams a WHERE a.groupCode = :groupCode" +
          " and (lower(a.fullname) like concat('%',lower(:searchKey), '%') " +
          " or lower(a.code) like concat('%',lower(:searchKey), '%')  " +
          " or lower(a.name) like concat('%',lower(:searchKey), '%')) " +
          " and a.status = 'A' ORDER BY a.name")
  Page<AppParams> findByGroupCodePaged(@Param("groupCode") String groupCode, @Param("searchKey") String key, Pageable pageable);

  @Query("SELECT a FROM AppParams a WHERE a.code = :code")
  Optional<AppParams> findByCode(@Param("code") String code);

  @Query("SELECT a FROM AppParams a WHERE a.code = :code AND a.groupCode = :groupCode")
  AppParams findByCodeAndGroupCode(@Param("code") String code, @Param("groupCode") String groupCode);

  @Query("Select count(*) from AppParams a where a.code = :code")
  int countCode(String code);

  List<AppParams> findAllByCodeOrName(String code, String name);



}
