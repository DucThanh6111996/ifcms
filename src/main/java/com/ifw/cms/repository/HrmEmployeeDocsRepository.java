package com.ifw.cms.repository;

import com.ifw.cms.entity.HrmEmployeeDocs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the HrmEmployeeDocs entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HrmEmployeeDocsRepository extends JpaRepository<HrmEmployeeDocs, Long> {

}
