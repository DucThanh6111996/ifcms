package com.ifw.cms.repository;

import com.ifw.cms.service.dto.*;

import java.util.List;

/**
 * ThanhDT  Employee Statistic By Role And Unit
 **/

public interface HrmEmployeeStatisticRepository {
    List<EmployeeStatisticDto> statisticByUnit();
    List<EmployeeRoleDTO> statisticByRole();
    List<ExpEmployeeDTO> statisticByExpEmployee();
    List<InsourceEmployeeDTO> statisticByInsourceEmployee();
    List<HrmEmployeeDTO> findEmployeeExpired();
    List<HrmEmployeeDTO> findAdmin();
}
