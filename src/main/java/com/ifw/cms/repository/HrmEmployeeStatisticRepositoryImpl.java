package com.ifw.cms.repository;

import com.ifw.cms.service.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * ThanhDT
 * GetData from DB
 **/


@Repository
public class HrmEmployeeStatisticRepositoryImpl implements HrmEmployeeStatisticRepository {
    private Logger logger = LoggerFactory.getLogger(HrmEmployeeStatisticRepositoryImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<EmployeeStatisticDto> statisticByUnit() {
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append("SELECT APP.CODE code ");
        sqlBuilder.append(", APP.NAME name ");
        sqlBuilder.append(", COUNT(EMP.ID) totalEmployee ");
        sqlBuilder.append("FROM APP_PARAMS APP  ");
        sqlBuilder.append("JOIN HRM_EMPLOYEE EMP ON APP.ID=EMP.UNIT_ID AND APP.GROUP_CODE='UNIT' ");
        sqlBuilder.append("GROUP BY APP.CODE, APP.NAME");

        List<EmployeeStatisticDto> data = jdbcTemplate.query(sqlBuilder.toString(), new EmployeeStatisticMapper());
        return data;
    }

    class EmployeeStatisticMapper implements RowMapper<EmployeeStatisticDto> {

        @Nullable
        @Override
        public EmployeeStatisticDto mapRow(ResultSet resultSet, int i) throws SQLException {
            EmployeeStatisticDto employeeStatisticDto = new EmployeeStatisticDto();
            employeeStatisticDto.setCode(resultSet.getString("code"));
            employeeStatisticDto.setName(resultSet.getString("name"));
            employeeStatisticDto.setTotalEmployee(resultSet.getInt("totalEmployee"));
            return employeeStatisticDto;
        }
    }

    @Override
    public List<EmployeeRoleDTO> statisticByRole() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT APP.CODE code ");
        stringBuilder.append(", APP.NAME name ");
        stringBuilder.append(", COUNT(EMP.ID) countEmployeeRole ");
        stringBuilder.append("FROM APP_PARAMS APP  ");
        stringBuilder.append("JOIN HRM_EMPLOYEE EMP ON APP.ID=EMP.ROLE_ID ");
        stringBuilder.append("GROUP BY APP.CODE, APP.NAME");

        List<EmployeeRoleDTO> data = jdbcTemplate.query(stringBuilder.toString(), new EmployeeStatisticByRoleMapper());
        return data;
    }

    class EmployeeStatisticByRoleMapper implements RowMapper<EmployeeRoleDTO> {

        @Nullable
        @Override
        public EmployeeRoleDTO mapRow(ResultSet resultSet, int i) throws SQLException {
            EmployeeRoleDTO employeeRoleDTO = new EmployeeRoleDTO();
            employeeRoleDTO.setCode(resultSet.getString("code"));
            employeeRoleDTO.setName(resultSet.getString("name"));
            employeeRoleDTO.setCountRoleEmployee(resultSet.getInt("countEmployeeRole"));
            return employeeRoleDTO;
        }
    }

    @Override
    public List<ExpEmployeeDTO> statisticByExpEmployee() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT APP.CODE code ");
        stringBuilder.append(", APP.NAME name ");
        stringBuilder.append(", COUNT(EMP.ID) countExpEmployee ");
        stringBuilder.append("FROM APP_PARAMS APP ");
        stringBuilder.append("JOIN HRM_EMPLOYEE EMP ON APP.ID=EMP.CONTRACT_TYPE_ID ");
        stringBuilder.append(" WHERE CURDATE() BETWEEN DATE_ADD(EMP.END_DATE,INTERVAL-30 DAY) AND EMP.END_DATE ");
        stringBuilder.append(" GROUP BY APP.CODE, APP.NAME ");

        List<ExpEmployeeDTO> data = jdbcTemplate.query(stringBuilder.toString(), new ExpEmployeeMapper());
        return data;
    }

    class ExpEmployeeMapper implements RowMapper<ExpEmployeeDTO> {

        @Nullable
        @Override
        public ExpEmployeeDTO mapRow(ResultSet resultSet, int i) throws SQLException {
            ExpEmployeeDTO expEmployeeDTO = new ExpEmployeeDTO();
            expEmployeeDTO.setCode(resultSet.getString("code"));
            expEmployeeDTO.setName(resultSet.getString("name"));
            expEmployeeDTO.setCountExpEmployee(resultSet.getInt("countExpEmployee"));
            return expEmployeeDTO;
        }
    }

    @Override
    public List<InsourceEmployeeDTO> statisticByInsourceEmployee() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SELECT APP.CODE CODE ");
        stringBuilder.append(", APP.NAME name ");
        stringBuilder.append(", COUNT(EMP.ID) numberOfEmployee ");
        stringBuilder.append("FROM APP_PARAMS APP ");
        stringBuilder.append("JOIN HRM_EMPLOYEE EMP ON APP.ID=EMP.UNIT_ID AND EMP.IS_INSOURCE=1 ");
        stringBuilder.append("GROUP BY APP.CODE,APP.NAME ");
        List<InsourceEmployeeDTO> data = jdbcTemplate.query(stringBuilder.toString(), new InsourceEmployeeMapper());
        return data;
    }

    class InsourceEmployeeMapper implements RowMapper<InsourceEmployeeDTO> {

        @Nullable
        @Override
        public InsourceEmployeeDTO mapRow(ResultSet resultSet, int i) throws SQLException {
            InsourceEmployeeDTO insourceEmployeeDTO = new InsourceEmployeeDTO();
            insourceEmployeeDTO.setCode(resultSet.getString("code"));
            insourceEmployeeDTO.setName(resultSet.getString("name"));
            insourceEmployeeDTO.setNumberOfEmployee(resultSet.getInt("numberOfEmployee"));
            return insourceEmployeeDTO;
        }
    }

    /**
     * Lấy danh sách nhân viên sắp hết hạn hợp đồng
     **/

    @Override
    public List<HrmEmployeeDTO> findEmployeeExpired() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" SELECT EMP.EMAIL email ");
        stringBuilder.append(", EMP.FULLNAME fullname ");
        stringBuilder.append(", EMP.DOB dob ");
        stringBuilder.append(", EMP.PHONE phone ");
        stringBuilder.append(", EMP.END_DATE endDate");
        stringBuilder.append(" FROM HRM_EMPLOYEE EMP ");
        stringBuilder.append(" WHERE END_DATE-CURRENT_DATE < ? ");

        InputStream inputStream = null;
        List<HrmEmployeeDTO> rs = new ArrayList<>();
        // read file config
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }

            // get the property value and print it out
            String time = prop.getProperty("time");

            rs = jdbcTemplate.query(stringBuilder.toString(),
                    new Object[]{time}, new HrmEmployeeMapper());
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return rs;
    }

    class HrmEmployeeMapper implements RowMapper<HrmEmployeeDTO> {
        @Nullable
        @Override
        public HrmEmployeeDTO mapRow(ResultSet resultSet, int i) throws SQLException {
            HrmEmployeeDTO hrmEmployeeDTO = new HrmEmployeeDTO();
            hrmEmployeeDTO.setEmail(resultSet.getString("email"));
            hrmEmployeeDTO.setFullname(resultSet.getString("fullName"));
            hrmEmployeeDTO.setPhone(resultSet.getString("phone"));
            if (resultSet.getDate("endDate") == null) {
                logger.error("Empty !");
            } else {
                hrmEmployeeDTO.setEndDate(convertToLocalDateViaMilisecond(resultSet.getDate("enddate")));
            }
            if (resultSet.getDate("dob") == null) {
                logger.error("Empty !");
            } else {
                hrmEmployeeDTO.setDob(convertToLocalDateViaMilisecond(resultSet.getDate("dob")));
            }
            return hrmEmployeeDTO;
        }
    }

    public LocalDate convertToLocalDateViaMilisecond(Date dateToConvert) {
        logger.info("Log" + dateToConvert);
        return Instant.ofEpochMilli(dateToConvert.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    /**
     * Lấy ra quản trị viên
     **/
    @Override
    public List<HrmEmployeeDTO> findAdmin() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" SELECT ");
        stringBuilder.append(" EMAIL email");
        stringBuilder.append(", FULLNAME fullName ");
        stringBuilder.append(", DOB dob ");
        stringBuilder.append(", PHONE phone ");
        stringBuilder.append(", END_DATE endDate ");
        stringBuilder.append(" FROM HRM_EMPLOYEE EMP ");
        stringBuilder.append(" JOIN  APP_ROLES ROL ON EMP.ROLE_ID = ROL.ROLE_ID = 1 ");
        List<HrmEmployeeDTO> list = jdbcTemplate.query(stringBuilder.toString(), new HrmEmployeeMapper());
        return list;
    }
} 
