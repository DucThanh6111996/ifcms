package com.ifw.cms.repository;

import com.ifw.cms.entity.CmActionLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the CmActionLog entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CmActionLogRepository extends JpaRepository<CmActionLog, Long> {

}
