package com.ifw.cms.repository;

import com.ifw.cms.entity.EmployeePayout;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the EmployeePayout entity.
 */
@Repository
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public interface EmployeePayoutRepository extends JpaRepository<EmployeePayout, Long> {
  @Query(value = "select e.id, e.fullname, e.code, e.email, e.phone, p.status, p.pay_type, p.prd_id, p.description, p.amount, e.salary_date " +
  " from hrm_payout p left join hrm_employee e " +
  " on p.pay_to = e.code " +
  " and p.prd_id = :prdId " +
  " and p.status = 'UNPAID'",
  nativeQuery = true)
  List<EmployeePayout> findUnpaidByPrdId(@Param("prdId") String prdId);
}
