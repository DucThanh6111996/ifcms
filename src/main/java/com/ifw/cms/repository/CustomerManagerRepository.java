package com.ifw.cms.repository;

import com.ifw.cms.service.dto.CustomerDTO;

import java.util.List;

public interface CustomerManagerRepository {

    List<CustomerDTO> getAllEmailCustomer();

    List<CustomerDTO> getAllContract();
}
