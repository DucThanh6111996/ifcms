package com.ifw.cms.repository;

import com.ifw.cms.entity.District;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Skill entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DistrictRepository extends JpaRepository<District, Long> {

    @Query(value = "select p from District p where p.province.id = :provinceId order by p.name asc")
    List<District> findByProvinceId(@Param("provinceId") Long provinceId);

    Optional<District> findById(Long id);
}
