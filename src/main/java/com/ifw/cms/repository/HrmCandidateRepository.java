package com.ifw.cms.repository;

import com.ifw.cms.entity.HrmCandidate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface HrmCandidateRepository extends JpaRepository<HrmCandidate, Long> {

    List<HrmCandidate> findAllByCodeOrPhoneOrEmail(String code, String phone, String email);

    @Query(value = "select distinct a from HrmCandidate a" +
            " where (lower(a.code) like concat('%',lower(:code), '%')" +
            " or lower(a.fullname) like concat('%',lower(:fullname), '%')" +
            "or lower(a.email) like concat('%',lower(:email), '%'))")
    Page<HrmCandidate> search(Pageable pageable,@Param("code") String code, @Param("fullname") String fullname, @Param("email") String email);

    @Query("SELECT a FROM HrmCandidate a WHERE a.code = :code")
    Optional<HrmCandidate> findByCode(@Param("code") String code);

}
