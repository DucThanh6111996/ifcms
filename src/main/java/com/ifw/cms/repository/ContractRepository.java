package com.ifw.cms.repository;

import com.ifw.cms.entity.Contract;
import com.ifw.cms.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ContractRepository extends JpaRepository<Contract, Long> {
    Optional<Contract> findByCode(String code);
    List<Contract> findAll();
    List<Contract> findAllByCode(String code);

    @Query(value = "select distinct a from Contract a" +
            " where (lower(a.code) like concat('%',lower(:code), '%')" +
            " or lower(a.name) like concat('%',lower(:name), '%'))")
    Page<Contract> search(Pageable pageable, @Param("code") String code, @Param("name") String name);
}
