package com.ifw.cms.repository;

import com.ifw.cms.entity.AppParamsExtra;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the AppParamsExtra entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AppParamsExtraRepository extends JpaRepository<AppParamsExtra, Long> {

}
