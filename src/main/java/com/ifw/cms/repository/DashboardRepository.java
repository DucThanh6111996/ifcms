package com.ifw.cms.repository;

import com.ifw.cms.entity.Dashboard;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the HrmEmployee entity.
 */
@Repository
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public interface DashboardRepository extends JpaRepository<Dashboard, Long> {
  @Query(value = "select 1 as id, count(1) as total_emp, SUM(CASE WHEN gender = 'F' THEN 1 ELSE 0 END) AS total_emp_female, " +
    " SUM(CASE WHEN gender = 'M' THEN 1 ELSE 0 END) AS total_emp_male, "+
    " SUM(CASE WHEN role.code = 'DEV' THEN 1 ELSE 0 END) AS total_dev, "+
    " SUM(CASE WHEN role.code = 'TESTER' THEN 1 ELSE 0 END) AS total_test, " +
    " count(office.id) AS total_office, '' kei, '' val from HRM_EMPLOYEE em " +
    " left join (select * from APP_PARAMS where group_code = 'EM_ROLE' and status = 'A') role " +
    " on em.role_id = role.id " +
    " left join (select * from APP_PARAMS where group_code = 'UNIT' and status = 'A' and code in ('HAN-BO')) office " +
    " on em.unit_id = office.id " +
    " where  em.status in ('A', 'B', 'D')",
  nativeQuery = true)
  Dashboard getStat();
}
