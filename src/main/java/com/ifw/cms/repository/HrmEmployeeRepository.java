package com.ifw.cms.repository;

import com.ifw.cms.entity.HrmEmployee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the HrmEmployee entity.
 */
@Repository
public interface HrmEmployeeRepository extends JpaRepository<HrmEmployee, Long> {

    @Query(value = "select distinct hrmEmployee from HrmEmployee hrmEmployee left join fetch hrmEmployee.skills",
            countQuery = "select count(distinct hrmEmployee) from HrmEmployee hrmEmployee")
    Page<HrmEmployee> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct hrmEmployee from HrmEmployee hrmEmployee left join fetch hrmEmployee.skills")
    List<HrmEmployee> findAllWithEagerRelationships();

    @Query("select hrmEmployee from HrmEmployee hrmEmployee left join fetch hrmEmployee.skills where hrmEmployee.id =:id")
    Optional<HrmEmployee> findOneWithEagerRelationships(@Param("id") Long id);

    @Query(value = "select distinct a from HrmEmployee a" +
            " where (lower(a.fullname) like concat('%',lower(:fullname), '%')" +
            " or lower(a.email) like concat('%',lower(:email), '%'))" +
            " and (a.status =:status or :status is null or :status = '') ")
    Page<HrmEmployee> search(Pageable pageable, @Param("fullname") String fullname,
                             @Param("email") String email, @Param("status") String status);

    @Query("select distinct e from HrmEmployee e where (e.salaryDate >= :start and e.salaryDate <= :end) ")
    List<HrmEmployee> findUserInPaymentPeriod(@Param("start") Long start, @Param("end") Long end);

    @Query("select count(e) from HrmEmployee e where e.code = :code or e.email = :email or e.phone = :phone")
    int countAllByFied(String code, String email, String phone);

    /**
     * ThanhDT
     **/
    Page<HrmEmployee> findByEndDateBefore(Pageable pageable, LocalDate endDate);

    @Query("select distinct e from HrmEmployee e where e.code = :code or e.email = :email or e.phone = :phone")
    List<HrmEmployee> findUserByCondition(String code, String email, String phone);
}
