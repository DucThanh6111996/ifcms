package com.ifw.cms.repository;

import com.ifw.cms.entity.HrmEmployeeAttr;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the HrmEmployeeAttr entity.
 */
@Repository
public interface HrmEmployeeAttrRepository extends JpaRepository<HrmEmployeeAttr, Long> {

    @Query(value = "select distinct hrmEmployeeAttr from HrmEmployeeAttr hrmEmployeeAttr left join fetch hrmEmployeeAttr.params",
        countQuery = "select count(distinct hrmEmployeeAttr) from HrmEmployeeAttr hrmEmployeeAttr")
    Page<HrmEmployeeAttr> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct hrmEmployeeAttr from HrmEmployeeAttr hrmEmployeeAttr left join fetch hrmEmployeeAttr.params")
    List<HrmEmployeeAttr> findAllWithEagerRelationships();

    @Query("select hrmEmployeeAttr from HrmEmployeeAttr hrmEmployeeAttr left join fetch hrmEmployeeAttr.params where hrmEmployeeAttr.id =:id")
    Optional<HrmEmployeeAttr> findOneWithEagerRelationships(@Param("id") Long id);

    @Query("select e from HrmEmployeeAttr e where employeeId.id = :id")
    List<HrmEmployeeAttr> findByEmployeeId(@Param("id") Long id);

}
