package com.ifw.cms.repository;

import com.ifw.cms.entity.HrmPayout;
import com.ifw.cms.entity.enumeration.PaymentStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the HrmPayout entity.
 */
@Repository
public interface HrmPayoutRepository extends JpaRepository<HrmPayout, Long> {
  @Query(value = "select count(1) from HrmPayout h where h.prdId = :prdId and h.payTo = :payTo")
  Integer findByPrdId(@Param("prdId") String prdId, @Param("payTo") String payTo);

  @Query(value = "select p from HrmPayout p where p.status = :status")
  Page<HrmPayout> findByStatus(Pageable pageable, @Param("status") PaymentStatus status);
}
