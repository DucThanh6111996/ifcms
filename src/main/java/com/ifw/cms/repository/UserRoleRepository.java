package com.ifw.cms.repository;

import com.ifw.cms.entity.AppUser;
import com.ifw.cms.entity.UserRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ITSOL SWIB Dev Team.
 */
@Repository
public interface UserRoleRepository extends PagingAndSortingRepository<UserRole, Long> {

    @Transactional
    @Modifying
    @Query(value = "delete from UserRole u where u.appUser.userId = :userId")
    int deleteRoleByUserId(@Param("userId") Long userId);

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO USER_ROLE (USER_ID, ROLE_ID) VALUES (:userId, :roleId)", nativeQuery = true)
    void addUserRole(@Param("userId") Long userId, @Param("roleId") Long roleId);
}
