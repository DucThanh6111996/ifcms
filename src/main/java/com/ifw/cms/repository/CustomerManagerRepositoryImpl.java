package com.ifw.cms.repository;


import com.ifw.cms.service.dto.CustomerDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

@Repository
public class CustomerManagerRepositoryImpl implements CustomerManagerRepository {

    private Logger logger = LoggerFactory.getLogger(HrmEmployeeStatisticRepositoryImpl.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<CustomerDTO> getAllEmailCustomer() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" SELECT ");
        stringBuilder.append(" C.NAME name , C.EMAIL email, C.CODE code, C.PHONE phone, money, start, end, number");
        stringBuilder.append(" FROM CONTRACT CT ");
        stringBuilder.append(" LEFT JOIN BIDDING_DOCUMENTS BD ON CT.BIDDING_DOC_ID = BD.ID ");
        stringBuilder.append(" LEFT JOIN CUSTOMER C ON BD.CUSTOMER_ID = C.ID ");
        stringBuilder.append(" WHERE CT.END - CT.START < 7 ");

        List<CustomerDTO> data = jdbcTemplate.query(stringBuilder.toString(), new CustomerManagerMapper());
        return data;
    }

    class CustomerManagerMapper implements RowMapper<CustomerDTO> {

        @Nullable
        @Override
        public CustomerDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
            CustomerDTO customerDTO = new CustomerDTO();
            customerDTO.setName(rs.getString("name"));
            customerDTO.setEmail(rs.getString("email"));
            customerDTO.setCode(rs.getString("code"));
            customerDTO.setMoney(rs.getString("money"));
            if (rs.getDate("start") == null){
                logger.error("Empty !");
            }else {
                customerDTO.setStart(convertToLocalDateViaMilisecond(rs.getDate("start")));
            }

            if (rs.getDate("end") == null){
                logger.error("Empty ! ");
            }else {
                customerDTO.setEnd(convertToLocalDateViaMilisecond(rs.getDate("end")));
            }
            return customerDTO;
        }
    }

    public LocalDate convertToLocalDateViaMilisecond(Date dateToConvert) {
        logger.info("Log" + dateToConvert);
        return Instant.ofEpochMilli(dateToConvert.getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    @Override
    public List<CustomerDTO> getAllContract() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" SELECT ");
        stringBuilder.append(" C.CODE code, C.NAME name , email, start, end, money ");
        stringBuilder.append(" FROM CONTRACT CT ");
        stringBuilder.append(" LEFT JOIN BIDDING_DOCUMENTS BD ON CT.BIDDING_DOC_ID = BD.ID ");
        stringBuilder.append(" LEFT JOIN CUSTOMER C ON BD.CUSTOMER_ID = C.ID ");
        stringBuilder.append(" WHERE CT.END - CT.START < 7 ");

        List<CustomerDTO> listData = jdbcTemplate.query(stringBuilder.toString(), new CustomerManagerMapper());
        return listData;
    }
}
