package com.ifw.cms.repository;

import com.ifw.cms.entity.BiddingDocuments;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BiddingDocumentsRepository extends JpaRepository<BiddingDocuments, Long> {
    List<BiddingDocuments> findAll();

    List<BiddingDocuments> findAllByCode(String code);

    @Query(value = "select distinct a from BiddingDocuments a" +
            " where (lower(a.code) like concat('%',lower(:code), '%')" +
            " or lower(a.name) like concat('%',lower(:name), '%'))")
    Page<BiddingDocuments> search(Pageable pageable, @Param("code") String code, @Param("name") String name);

    @Query("SELECT a FROM BiddingDocuments a WHERE  a.status = 1 ORDER BY a.code ")
    List<BiddingDocuments> findByStatus();
}
