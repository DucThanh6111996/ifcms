package com.ifw.cms.repository;

import com.ifw.cms.entity.CustomerBranch;
import com.ifw.cms.service.dto.CustomerBranchDTO;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CustomerBranchRepository extends JpaRepository<CustomerBranch, Long> {
    List<CustomerBranch> findByCustomerId(Long id);

}
