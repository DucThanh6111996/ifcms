package com.ifw.cms.repository;

import com.ifw.cms.entity.CustomerPayment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CustomerPaymentRepository extends JpaRepository<CustomerPayment,Long> {
    List<CustomerPayment> findAll();
    List<CustomerPayment> findAllByCode(String code);

    @Query(value = "select distinct a from CustomerPayment a" +
            " where (lower(a.code) like concat('%',lower(:code), '%')" +
            " or lower(a.name) like concat('%',lower(:name), '%'))")
    Page<CustomerPayment> search(Pageable pageable, @Param("code") String code, @Param("name") String name);
}
