package com.ifw.cms.repository;

import com.ifw.cms.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Optional<Customer> findByCode(String code);
    List<Customer> findAll();
    List<Customer> findAllByCodeOrName(String code, String name);

    @Query(value = "select distinct a from Customer a" +
            " where (lower(a.code) like concat('%',lower(:code), '%')" +
            " or lower(a.name) like concat('%',lower(:name), '%'))")
    Page<Customer> search(Pageable pageable, @Param("code") String code, @Param("name") String name);
}
