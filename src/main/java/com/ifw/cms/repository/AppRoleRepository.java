package com.ifw.cms.repository;

import com.ifw.cms.entity.AppRole;
import com.ifw.cms.entity.AppUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ITSOL SWIB Dev Team.
 */
@Repository
public interface AppRoleRepository extends PagingAndSortingRepository<AppRole, Long> {
    @Query(value = "select u from AppRole u")
    Page<AppRole> findPaginatedUsers(Pageable pageable);

    @Query(value = "select u from AppRole u where u.roleName = :roleName")
    AppRole findByRoleName(@Param("roleName") String roleName);

    @Query(value = "select a.ROLE_ID as ROLE_ID, a.ROLE_NAME as ROLE_NAME from APP_ROLES a INNER JOIN USER_ROLE u " +
            " on a.ROLE_ID = u.ROLE_ID where u.USER_ID = :userId", nativeQuery = true)
    List<AppRole> findByUserRole(@Param("userId") Long userId);

    @Query(value = "select u from AppRole u")
    List<AppRole> findAllRoles();
}
