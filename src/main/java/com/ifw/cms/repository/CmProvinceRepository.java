package com.ifw.cms.repository;

import com.ifw.cms.entity.CmProvince;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the CmProvince entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CmProvinceRepository extends JpaRepository<CmProvince, Long> {
    @Query("select p from CmProvince p order by name")
    List<CmProvince> findAll();
}
