package com.ifw.cms.service;


import com.ifw.cms.entity.HrmCandidate;
import com.ifw.cms.service.dto.HrmCandidateDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface HrmCandidateService {
    /**
     * Save a HrmCandidate
     * @param hrmCandidateDTO
     * @return
     */
    HrmCandidateDTO save(HrmCandidateDTO hrmCandidateDTO);

    /**
     * Get candidate
     * @return
     */
    Page<HrmCandidateDTO> findAll(Pageable pageable);

    Page<HrmCandidateDTO> search(Pageable pageable, String code, String fullname, String email);

    /**
     *
     * @param id
     * @return
     */
    HrmCandidateDTO findById(Long id);

    Optional<HrmCandidate> findByCode(String Code);


    void deleteById(Long id);

    /**
     *
     * @param code
     * @param phone
     * @param email
     * @return
     */
    List<Integer> checkFieldExists(String code, String phone, String email);

}
