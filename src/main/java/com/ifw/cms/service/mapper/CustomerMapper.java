package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.Customer;
import com.ifw.cms.entity.CustomerBranch;
import com.ifw.cms.service.dto.CustomerDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {CustomerBranchMapper.class})
public interface CustomerMapper extends EntityMapper<CustomerDTO, Customer> {

    @Mapping(source = "customer.id", target = "branchId")
    CustomerDTO toDto(Customer customer);

    //    @Mapping(source = "branchId", target = "customerBranches")
    Customer toEntity(CustomerDTO customerDTO );

    default CustomerBranch fromId(Long id) {
        if (id == null) {
            return null;
        }

        CustomerBranch customerBranch = new CustomerBranch();
        customerBranch.setId(id);
        return customerBranch;
    }

    default Customer getCustomer(Long id) {
        if (id == null) {
            return null;
        }
        Customer customer = new Customer();
        customer.setId(id);
        return customer;
    }
}
