package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.HrmEmployee;
import com.ifw.cms.service.dto.HrmEmployeeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 */
@Mapper(componentModel = "spring", uses = {AppParamsMapper.class, CmProvinceMapper.class, SkillMapper.class})
public interface HrmEmployeeMapper extends EntityMapper<HrmEmployeeDTO, HrmEmployee> {

    @Mapping(source = "level.id", target = "levelId")
    @Mapping(source = "role.id", target
            = "roleId")
    @Mapping(source = "branch.id", target = "branchId")
    @Mapping(source = "unit.id", target = "unitId")
    @Mapping(source = "contractType.id", target = "contractTypeId")
    @Mapping(source = "permProvince.id", target = "permProvinceId")
    @Mapping(source = "permDistrict.id", target = "permDistrictId")
    @Mapping(source = "permTown.id", target = "permTownId")
    HrmEmployeeDTO toDto(HrmEmployee hrmEmployee);

    @Mapping(source = "levelId", target = "level")
    @Mapping(source = "roleId", target = "role")
    @Mapping(source = "branchId", target = "branch")
    @Mapping(source = "unitId", target = "unit")
    @Mapping(source = "contractTypeId", target = "contractType")
    @Mapping(source = "permProvinceId", target = "permProvince")
    @Mapping(source = "permDistrictId", target = "permDistrict")
    @Mapping(source = "permTownId", target = "permTown")
    @Mapping(target = "hrmEmployeeDocs", ignore = true)
    @Mapping(target = "hrmPayouts", ignore = true)
    HrmEmployee toEntity(HrmEmployeeDTO hrmEmployeeDTO);

    default HrmEmployee fromId(Long id) {
        if (id == null) {
            return null;
        }
        HrmEmployee hrmEmployee = new HrmEmployee();
        hrmEmployee.setId(id);
        return hrmEmployee;
    }
}
