package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.Contract;
import com.ifw.cms.entity.CustomerBranch;
import com.ifw.cms.service.dto.ContractDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {BiddingDocumentsMapper.class,CustomerBranch.class})
public interface ContractMapper extends EntityMapper<ContractDTO, Contract>{

    @Mapping(source = "biddingDocumentsId", target = "biddingDocumentsId")
    @Mapping(source = "number", target = "number")
    ContractDTO toDto(Contract contract);

    @Mapping(source = "biddingDocumentsId", target = "biddingDocumentsId")
    @Mapping(source = "number", target = "number")

    Contract toEntity(ContractDTO contractDTO );

    default Contract fromId(Long id) {
        if (id == null) {
            return null;
        }

        Contract contract = new Contract();
        contract.setId(id);
        return contract;
    }

    default CustomerBranch getCustomerBranch(Long id) {
        if (id == null) {
            return null;
        }
        CustomerBranch customerBranch = new CustomerBranch();
        customerBranch.setId(id);
        return customerBranch;
    }
}
