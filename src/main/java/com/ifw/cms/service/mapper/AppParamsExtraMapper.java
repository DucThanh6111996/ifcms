package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.AppParamsExtra;
import com.ifw.cms.service.dto.AppParamsExtraDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 */
@Mapper(componentModel = "spring", uses = {AppParamsMapper.class})
public interface AppParamsExtraMapper extends EntityMapper<AppParamsExtraDTO, AppParamsExtra> {

    @Mapping(source = "param.id", target = "paramId")
    AppParamsExtraDTO toDto(AppParamsExtra appParamsExtra);

    @Mapping(source = "paramId", target = "param")
    AppParamsExtra toEntity(AppParamsExtraDTO appParamsExtraDTO);

    default AppParamsExtra fromId(Long id) {
        if (id == null) {
            return null;
        }
        AppParamsExtra appParamsExtra = new AppParamsExtra();
        appParamsExtra.setId(id);
        return appParamsExtra;
    }
}
