package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.AppUser;
import com.ifw.cms.service.dto.UserVM;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * Created by ITSOL SWIB Dev Team.
 */
@Mapper
public interface UserMapper {
    UserVM toVM(AppUser user);
    AppUser toEntity(UserVM vm);
    List<UserVM> toListVM(List<AppUser> user);
    List<AppUser> toListEntity(List<UserVM> vm);
}
