package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.*;
import com.ifw.cms.service.dto.CustomerPaymentDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


/**
 * Ninh Trang An 28/10/2019
 */

@Mapper(componentModel = "spring", uses = {CtmInstallmentPaymentMapper.class, Customer.class,Contract.class})
public interface CustomerPaymentMapper  extends EntityMapper<CustomerPaymentDTO, CustomerPayment>{

    @Mapping(source = "type", target = "type")
    @Mapping(source = "ctmInstallmentPayments", target = "ctmInstallmentPayments")
    @Mapping(source = "customer.id", target = "customerId")
    @Mapping(source = "contract.id", target = "contractId")
    CustomerPaymentDTO toDto(CustomerPayment customerPayment);

    @Mapping(source = "customerId", target = "customer")
    @Mapping(source = "contractId", target = "contract")
    CustomerPayment toEntity(CustomerPaymentDTO customerPaymentDTO);

    default CustomerPayment fromId(Long id) {
        if (id == null) {
            return null;
        }
        CustomerPayment customerPayment = new CustomerPayment();
        customerPayment.setId(id);
        return customerPayment;
    }

    default Customer getCustomer(Long id) {
        if (id == null) {
            return null;
        }

        Customer customer = new Customer();
        customer.setId(id);
        return customer;
    }
    default Contract getContract(Long id) {
        if (id == null) {
            return null;
        }

        Contract contract = new Contract();
        contract.setId(id);
        return contract;
    }
}
