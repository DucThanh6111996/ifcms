package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.BiddingDocuments;
import com.ifw.cms.entity.Customer;
import com.ifw.cms.entity.CustomerBranch;
import com.ifw.cms.service.dto.BiddingDocumentsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {Customer.class,CustomerBranch.class})
public interface BiddingDocumentsMapper extends EntityMapper<BiddingDocumentsDTO, BiddingDocuments> {
    @Mapping(source = "customer.id", target = "customerId")
    @Mapping(source = "customerBranchId", target = "customerBranchId")
    BiddingDocumentsDTO toDto(BiddingDocuments biddingDocuments);

    @Mapping(source = "customerId", target = "customer")
    @Mapping(source = "customerBranchId", target = "customerBranchId")
    BiddingDocuments toEntity(BiddingDocumentsDTO biddingDocumentsDTO );

    default BiddingDocuments fromId(Long id) {
        if (id == null) {
            return null;
        }

        BiddingDocuments biddingDocuments = new BiddingDocuments();
        biddingDocuments.setId(id);
        return biddingDocuments;
    }
    default Customer getCustomer(Long id) {
        if (id == null) {
            return null;
        }
        Customer customer = new Customer();
        customer.setId(id);
        return customer;
    }

    default CustomerBranch getCustomerBranch(Long id) {
        if (id == null) {
            return null;
        }
        CustomerBranch customerBranch = new CustomerBranch();
        customerBranch.setId(id);
        return customerBranch;
    }

}
