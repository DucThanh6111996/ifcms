package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.CmActionLog;
import com.ifw.cms.service.dto.CmActionLogDTO;
import org.mapstruct.Mapper;

/**
 */
@Mapper(componentModel = "spring", uses = {})
public interface CmActionLogMapper extends EntityMapper<CmActionLogDTO, CmActionLog> {



    default CmActionLog fromId(Long id) {
        if (id == null) {
            return null;
        }
        CmActionLog cmActionLog = new CmActionLog();
        cmActionLog.setId(id);
        return cmActionLog;
    }
}
