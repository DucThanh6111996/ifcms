package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.HrmPayout;
import com.ifw.cms.service.dto.HrmPayoutDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 */
@Mapper(componentModel = "spring", uses = {HrmEmployeeMapper.class})
public interface HrmPayoutMapper extends EntityMapper<HrmPayoutDTO, HrmPayout> {

    @Mapping(source = "employee.id", target = "employeeId")
    HrmPayoutDTO toDto(HrmPayout hrmPayout);

    @Mapping(source = "employeeId", target = "employee")
    HrmPayout toEntity(HrmPayoutDTO hrmPayoutDTO);

    default HrmPayout fromId(Long id) {
        if (id == null) {
            return null;
        }
        HrmPayout hrmPayout = new HrmPayout();
        hrmPayout.setId(id);
        return hrmPayout;
    }
}
