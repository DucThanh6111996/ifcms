package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.CtmInstallmentPayment;
import com.ifw.cms.entity.Customer;
import com.ifw.cms.entity.CustomerBranch;
import com.ifw.cms.service.dto.CustomerBranchDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {Customer.class})
public interface CustomerBranchMapper extends EntityMapper<CustomerBranchDTO, CustomerBranch> {
    @Mapping(source = "customer.id", target = "customerId")
    CustomerBranchDTO toDto(CustomerBranch customerBranch);

    @Mapping(source = "customerId", target = "customer")
    CustomerBranch toEntity(CustomerBranchDTO customerBranchDTO);

    default CustomerBranch fromId(Long id) {
        if (id == null) {
            return null;
        }
        CustomerBranch customerBranch = new CustomerBranch();
        customerBranch.setId(id);
        return customerBranch;
    }

    default Customer getCustomer(Long id) {
        if (id == null) {
            return null;
        }
        Customer customer = new Customer();
        customer.setId(id);
        return customer;
    }
}
