package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.Skill;
import com.ifw.cms.service.dto.SkillDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 */
@Mapper(componentModel = "spring", uses = {})
public interface SkillMapper extends EntityMapper<SkillDTO, Skill> {


        @Mapping(target = "employees", ignore = true)
    Skill toEntity(SkillDTO skillDTO);

    default Skill fromId(Long id) {
        if (id == null) {
            return null;
        }
        Skill skill = new Skill();
        skill.setId(id);
        return skill;
    }
}
