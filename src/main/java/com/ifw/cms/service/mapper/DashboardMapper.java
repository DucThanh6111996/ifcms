package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.Dashboard;
import com.ifw.cms.service.dto.DashboardDTO;
import org.mapstruct.Mapper;

/**
 */
@Mapper(componentModel = "spring", uses = {})
public interface DashboardMapper extends EntityMapper<DashboardDTO, Dashboard> {

}
