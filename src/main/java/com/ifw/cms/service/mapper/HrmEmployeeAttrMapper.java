package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.HrmEmployeeAttr;
import com.ifw.cms.service.dto.HrmEmployeeAttrDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

/**
 */
@Mapper(componentModel = "spring", uses = {AppParamsMapper.class, HrmEmployeeMapper.class})
public interface HrmEmployeeAttrMapper extends EntityMapper<HrmEmployeeAttrDTO, HrmEmployeeAttr> {

    @Mapping(source = "employeeId.id", target = "employeeIdId")
    HrmEmployeeAttrDTO toDto(HrmEmployeeAttr hrmEmployeeAttr);

    @Mapping(source = "employeeIdId", target = "employeeId")
    HrmEmployeeAttr toEntity(HrmEmployeeAttrDTO hrmEmployeeAttrDTO);

    default HrmEmployeeAttr fromId(Long id) {
        if (id == null) {
            return null;
        }
        HrmEmployeeAttr hrmEmployeeAttr = new HrmEmployeeAttr();
        hrmEmployeeAttr.setId(id);
        return hrmEmployeeAttr;
    }

    public abstract List<HrmEmployeeAttrDTO> mapToListDTO(List<HrmEmployeeAttr> hrmEmployeeAttrs);
}
