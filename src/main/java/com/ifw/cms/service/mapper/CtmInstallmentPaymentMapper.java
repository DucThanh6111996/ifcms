package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.CtmInstallmentPayment;
import com.ifw.cms.entity.CustomerPayment;
import com.ifw.cms.service.dto.CtmInstallmentPaymentDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {CustomerPayment.class})
public interface CtmInstallmentPaymentMapper  extends EntityMapper<CtmInstallmentPaymentDTO, CtmInstallmentPayment>{
//    @Mapping(source = "customerPayment.id", target = "customerPaymentId")
    CtmInstallmentPaymentDTO toDto(CtmInstallmentPayment ctmInstallmentPayment);

//    @Mapping(source = "customerPaymentId", target = "customerPayment")
    CtmInstallmentPayment toEntity(CtmInstallmentPaymentDTO ctmInstallmentPaymentDTO);

    default CtmInstallmentPayment fromId(Long id) {
        if (id == null) {
            return null;
        }
        CtmInstallmentPayment customerPayment = new CtmInstallmentPayment();
        customerPayment.setId(id);
        return customerPayment;
    }

    default CustomerPayment getCustomerPayment(Long id) {
        if (id == null) {
            return null;
        }
        CustomerPayment customerPayment = new CustomerPayment();
        customerPayment.setId(id);
        return customerPayment;
    }
}
