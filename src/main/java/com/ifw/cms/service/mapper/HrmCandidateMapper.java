package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.District;
import com.ifw.cms.entity.HrmCandidate;
import com.ifw.cms.entity.Province;
import com.ifw.cms.entity.Ward;
import com.ifw.cms.service.dto.HrmCandidateDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * CuongDX 23/09/2019
 */

@Mapper(componentModel = "spring", uses = {AppParamsMapper.class, SkillMapper.class, Province.class, District.class, Ward.class})
public interface HrmCandidateMapper extends EntityMapper<HrmCandidateDTO, HrmCandidate>{

    @Mapping(source = "phone", target = "phone")
    @Mapping(source = "email", target = "email")
    @Mapping(source = "province.id", target = "provinceId")
    @Mapping(source = "district.id", target = "districtId")
    @Mapping(source = "ward.id", target = "wardId")
    @Mapping(target = "image", ignore = true)
    HrmCandidateDTO toDto(HrmCandidate hrmCandidate);

    @Mapping(source = "provinceId", target = "province")
    @Mapping(source = "districtId", target = "district")
    @Mapping(source = "wardId", target = "ward")
    @Mapping(target = "image", ignore = true)
    HrmCandidate toEntity(HrmCandidateDTO hrmCandidateDTO);

    default HrmCandidate fromId(Long id) {
        if (id == null) {
            return null;
        }
        HrmCandidate hrmCandidate = new HrmCandidate();
        hrmCandidate.setId(id);
        return hrmCandidate;
    }

    default Province getProvince(Long id) {
        if (id == null) {
            return null;
        }
        Province province = new Province();
        province.setId(id);
        return province;
    }

    default District getDistrict(Long id) {
        if (id == null) {
            return null;
        }
        District district = new District();
        district.setId(id);
        return district;
    }

    default Ward getWard(Long id) {
        if (id == null) {
            return null;
        }
        Ward ward = new Ward();
        ward.setId(id);
        return ward;
    }
}
