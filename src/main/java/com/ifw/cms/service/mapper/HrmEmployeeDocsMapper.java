package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.HrmEmployeeDocs;
import com.ifw.cms.service.dto.HrmEmployeeDocsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 */
@Mapper(componentModel = "spring", uses = {HrmEmployeeMapper.class})
public interface HrmEmployeeDocsMapper extends EntityMapper<HrmEmployeeDocsDTO, HrmEmployeeDocs> {

    @Mapping(source = "employee.id", target = "employeeId")
    HrmEmployeeDocsDTO toDto(HrmEmployeeDocs hrmEmployeeDocs);

    @Mapping(source = "employeeId", target = "employee")
    HrmEmployeeDocs toEntity(HrmEmployeeDocsDTO hrmEmployeeDocsDTO);

    default HrmEmployeeDocs fromId(Long id) {
        if (id == null) {
            return null;
        }
        HrmEmployeeDocs hrmEmployeeDocs = new HrmEmployeeDocs();
        hrmEmployeeDocs.setId(id);
        return hrmEmployeeDocs;
    }
}
