package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.AppParams;
import com.ifw.cms.service.dto.AppParamsDTO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 */
@Mapper(componentModel = "spring", uses = {})
public interface AppParamsMapper extends EntityMapper<AppParamsDTO, AppParams> {



    default AppParams fromId(Long id) {
        if (id == null) {
            return null;
        }
        AppParams appParams = new AppParams();
        appParams.setId(id);
        return appParams;
    }

    public abstract List<AppParamsDTO> mapToDTO(List<AppParams> appParams);
    AppParamsDTO toDto(AppParams appParams);
    AppParams toEntity(AppParamsDTO appParamsDTO);


}
