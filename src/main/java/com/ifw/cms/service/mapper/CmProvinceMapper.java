package com.ifw.cms.service.mapper;

import com.ifw.cms.entity.CmProvince;
import com.ifw.cms.service.dto.CmProvinceDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

/**
 */
@Mapper(componentModel = "spring", uses = {})
public interface CmProvinceMapper extends EntityMapper<CmProvinceDTO, CmProvince> {

    @Mapping(source = "parent.id", target = "parentId")
    CmProvinceDTO toDto(CmProvince cmProvince);

    @Mapping(source = "parentId", target = "parent")
    CmProvince toEntity(CmProvinceDTO cmProvinceDTO);

    List<CmProvinceDTO> toListDTO(List<CmProvince> lstCmProvince);

    default CmProvince fromId(Long id) {
        if (id == null) {
            return null;
        }
        CmProvince cmProvince = new CmProvince();
        cmProvince.setId(id);
        return cmProvince;
    }
}
