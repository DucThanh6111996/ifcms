package com.ifw.cms.service;

import com.ifw.cms.entity.CmProvince;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.ifw.cms.service.dto.*;

import java.util.List;
import java.util.Optional;

/**
 */
public interface CmProvinceService {

    /**
     * Save a cmProvince.
     *
     * @param cmProvinceDTO the entity to save.
     * @return the persisted entity.
     */
    CmProvinceDTO save(CmProvinceDTO cmProvinceDTO);

    /**
     * Get all the cmProvinces.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CmProvinceDTO> findAll(Pageable pageable);

    /**
     * Find all without paging
     * @return
     */
    List<CmProvinceDTO> findAll();


    /**
     * Get the "id" cmProvince.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CmProvinceDTO> findOne(Long id);

    /**
     * Delete the "id" cmProvince.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
