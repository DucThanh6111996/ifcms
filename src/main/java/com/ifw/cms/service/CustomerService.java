package com.ifw.cms.service;

import com.ifw.cms.entity.Customer;
import com.ifw.cms.service.dto.CustomerDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
    CustomerDTO save(CustomerDTO customerDTO);
    void delete (Long id);
    Page<CustomerDTO> findAll(Pageable pageable);
    CustomerDTO findById(Long id);
    Optional<Customer> findByCode(String code);
    Page<CustomerDTO> search(Pageable pageable, String code, String name);

    List<Integer> checkFieldExists(String code, String name);

    // danh sach khach hang
    List<CustomerDTO> getListCustomer();

    List<CustomerDTO> getListContract();
}
