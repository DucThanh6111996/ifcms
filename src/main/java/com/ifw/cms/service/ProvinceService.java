package com.ifw.cms.service;

import com.ifw.cms.entity.Province;

import java.util.List;
import java.util.Optional;

public interface ProvinceService {
    Province save(Province province);
    void delete (Long id);
    Optional<Province> findById(Long id);
    List<Integer> checkFieldExists(Long id, String name);
}
