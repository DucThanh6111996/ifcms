package com.ifw.cms.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.ifw.cms.service.dto.*;
import java.util.List;
import java.util.Optional;

/**
 */
public interface HrmEmployeeAttrService {

    /**
     * Save a hrmEmployeeAttr.
     *
     * @param hrmEmployeeAttrDTO the entity to save.
     * @return the persisted entity.
     */
    HrmEmployeeAttrDTO save(HrmEmployeeAttrDTO hrmEmployeeAttrDTO);

    /**
     * Get all the hrmEmployeeAttrs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<HrmEmployeeAttrDTO> findAll(Pageable pageable);

    /**
     * Get all the hrmEmployeeAttrs with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<HrmEmployeeAttrDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" hrmEmployeeAttr.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HrmEmployeeAttrDTO> findOne(Long id);

    /**
     * Delete the "id" hrmEmployeeAttr.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Find attributes by employee id
     * @param id
     * @return
     */
    List<HrmEmployeeAttrDTO> findByEmployeeId(Long id);
}
