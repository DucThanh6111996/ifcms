package com.ifw.cms.service;

import com.ifw.cms.entity.enumeration.PaymentStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.ifw.cms.service.dto.*;
import java.util.Optional;

/**
 */
public interface HrmPayoutService {

    /**
     * Save a hrmPayout.
     *
     * @param hrmPayoutDTO the entity to save.
     * @return the persisted entity.
     */
    HrmPayoutDTO save(HrmPayoutDTO hrmPayoutDTO);

    /**
     * Get all the hrmPayouts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<HrmPayoutDTO> findAll(Pageable pageable);


    Page<HrmPayoutDTO> findByStatus(Pageable pageable, PaymentStatus status);

    /**
     * Get the "id" hrmPayout.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HrmPayoutDTO> findOne(Long id);

    /**
     * Delete the "id" hrmPayout.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
