package com.ifw.cms.service;

import com.ifw.cms.service.dto.UserVM;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by ITSOL SWIB Dev Team.
 */
@Service
public interface UserService {
    Page<UserVM> findAllUsers(Pageable pageable, String key);

    UserVM save(UserVM user);
}
