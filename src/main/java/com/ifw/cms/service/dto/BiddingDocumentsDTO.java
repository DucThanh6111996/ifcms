package com.ifw.cms.service.dto;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class BiddingDocumentsDTO implements Serializable {

    private Long id;

    @NotEmpty
    private String code;

    @NotEmpty
    private String name;

    @NotEmpty
    private String note;

    @NotEmpty
    private String status;

    private Long customerId;

    @NotNull
    private Long customerBranchId;


    public BiddingDocumentsDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getCustomerBranchId() {
        return customerBranchId;
    }

    public void setCustomerBranchId(Long customerBranchId) {
        this.customerBranchId = customerBranchId;
    }
}
