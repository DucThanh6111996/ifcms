package com.ifw.cms.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 */
public class CmActionLogDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 20)
    private String actionType;

    @Size(max = 20)
    private String actionGroup;

    @NotNull
    @Size(max = 20)
    private String objectType;

    @NotNull
    @Size(max = 20)
    private String objectId;

    @Size(max = 255)
    private String oldValue;

    @Size(max = 255)
    private String newValue;

    @Size(max = 255)
    private String extraValue;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getActionGroup() {
        return actionGroup;
    }

    public void setActionGroup(String actionGroup) {
        this.actionGroup = actionGroup;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getExtraValue() {
        return extraValue;
    }

    public void setExtraValue(String extraValue) {
        this.extraValue = extraValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CmActionLogDTO cmActionLogDTO = (CmActionLogDTO) o;
        if (cmActionLogDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cmActionLogDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CmActionLogDTO{" +
            "id=" + getId() +
            ", actionType='" + getActionType() + "'" +
            ", actionGroup='" + getActionGroup() + "'" +
            ", objectType='" + getObjectType() + "'" +
            ", objectId='" + getObjectId() + "'" +
            ", oldValue='" + getOldValue() + "'" +
            ", newValue='" + getNewValue() + "'" +
            ", extraValue='" + getExtraValue() + "'" +
            "}";
    }
}
