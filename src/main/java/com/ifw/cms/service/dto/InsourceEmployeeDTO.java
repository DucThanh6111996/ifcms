package com.ifw.cms.service.dto;

public class InsourceEmployeeDTO {
    private String code;
    private String name;
    private Integer numberOfEmployee;

    public InsourceEmployeeDTO(){

    }

    public InsourceEmployeeDTO(String code, String name, Integer numberOfEmployee) {
        this.code = code;
        this.name = name;
        this.numberOfEmployee = numberOfEmployee;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumberOfEmployee() {
        return numberOfEmployee;
    }

    public void setNumberOfEmployee(Integer numberOfEmployee) {
        this.numberOfEmployee = numberOfEmployee;
    }
}
