package com.ifw.cms.service.dto;

import com.ifw.cms.entity.enumeration.EduLevel;
import com.ifw.cms.entity.enumeration.SalaryPaymentType;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 *
 */
public class HrmEmployeeDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 50, min = 1)
    private String code;

    @NotNull
    @Size(max = 255, min = 1)
    private String fullname;

    private SalaryPaymentType salPmtType;

    private BigDecimal supportAmt;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private LocalDate cntVldFrmDt;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private LocalDate cntVldToDt;

    private String slrBefore;

    private String slrAmtEnc;

    @Size(max = 50)
    private String accountNo;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private LocalDate dob;

    private EduLevel edu;

    @NotNull
    @Size(max = 1)
    private String gender;

    @Size(max = 50)
    private String idNo;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private LocalDate idIssueDtm;

    @NotNull
    @Size(max = 1)
    private String status;

    @NotEmpty
    @Size(max = 50)
    private String email;

    @Size(max = 50)
    private String phone;

    @Size(max = 50)
    private String phone2;

    private Long salaryDate;

    private Boolean isInsource;

    @Size(max = 50)
    private String taxNo;

    private Long tgtgBhxh;

    @Size(max = 50)
    private String bhxhNo;

    @Size(max = 50)
    private String bhytNo;

    @Size(max = 50)
    private String bhThatNghiep;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private LocalDate lastUpdate;

    @Size(max = 50)
    private String updateBy;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private LocalDate endDate;

    private String cv;

    private MultipartFile[] cvFiles;

    private String cvContentType;

    private Long levelId;

    private Long roleId;

    private Long branchId;

    private Long unitId;

    private Long contractTypeId;

    private Long permProvinceId;

    private Long permDistrictId;

    private Long permTownId;

    private Set<SkillDTO> skills = new HashSet<>();

    private AppParamsDTO level;

    private AppParamsDTO role;

    private AppParamsDTO branch;

    private AppParamsDTO unit;

    private AppParamsDTO contractType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public SalaryPaymentType getSalPmtType() {
        return salPmtType;
    }

    public void setSalPmtType(SalaryPaymentType salPmtType) {
        this.salPmtType = salPmtType;
    }

    public BigDecimal getSupportAmt() {
        return supportAmt;
    }

    public void setSupportAmt(BigDecimal supportAmt) {
        this.supportAmt = supportAmt;
    }

    public LocalDate getCntVldFrmDt() {
        return cntVldFrmDt;
    }

    public void setCntVldFrmDt(LocalDate cntVldFrmDt) {
        this.cntVldFrmDt = cntVldFrmDt;
    }

    public LocalDate getCntVldToDt() {
        return cntVldToDt;
    }

    public void setCntVldToDt(LocalDate cntVldToDt) {
        this.cntVldToDt = cntVldToDt;
    }

    public String getSlrBefore() {
        return slrBefore;
    }

    public void setSlrBefore(String slrBefore) {
        this.slrBefore = slrBefore;
    }

    public String getSlrAmtEnc() {
        return slrAmtEnc;
    }

    public void setSlrAmtEnc(String slrAmtEnc) {
        this.slrAmtEnc = slrAmtEnc;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public EduLevel getEdu() {
        return edu;
    }

    public void setEdu(EduLevel edu) {
        this.edu = edu;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public LocalDate getIdIssueDtm() {
        return idIssueDtm;
    }

    public void setIdIssueDtm(LocalDate idIssueDtm) {
        this.idIssueDtm = idIssueDtm;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public Long getSalaryDate() {
        return salaryDate;
    }

    public void setSalaryDate(Long salaryDate) {
        this.salaryDate = salaryDate;
    }

    public Boolean getIsInsource() {
        return isInsource;
    }

    public void setIsInsource(Boolean isInsource) {
        this.isInsource = isInsource;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public Long getTgtgBhxh() {
        return tgtgBhxh;
    }

    public void setTgtgBhxh(Long tgtgBhxh) {
        this.tgtgBhxh = tgtgBhxh;
    }

    public String getBhxhNo() {
        return bhxhNo;
    }

    public void setBhxhNo(String bhxhNo) {
        this.bhxhNo = bhxhNo;
    }

    public String getBhytNo() {
        return bhytNo;
    }

    public void setBhytNo(String bhytNo) {
        this.bhytNo = bhytNo;
    }

    public String getBhThatNghiep() {
        return bhThatNghiep;
    }

    public void setBhThatNghiep(String bhThatNghiep) {
        this.bhThatNghiep = bhThatNghiep;
    }

    public LocalDate getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDate lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public String getCv() {
        return cv;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public String getCvContentType() {
        return cvContentType;
    }

    public void setCvContentType(String cvContentType) {
        this.cvContentType = cvContentType;
    }

    public Long getLevelId() {
        return levelId;
    }

    public void setLevelId(Long appParamsId) {
        this.levelId = appParamsId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long appParamsId) {
        this.roleId = appParamsId;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long appParamsId) {
        this.branchId = appParamsId;
    }

    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long appParamsId) {
        this.unitId = appParamsId;
    }

    public Long getContractTypeId() {
        return contractTypeId;
    }

    public void setContractTypeId(Long appParamsId) {
        this.contractTypeId = appParamsId;
    }

    public Long getPermProvinceId() {
        return permProvinceId;
    }

    public void setPermProvinceId(Long cmProvinceId) {
        this.permProvinceId = cmProvinceId;
    }

    public Long getPermDistrictId() {
        return permDistrictId;
    }

    public void setPermDistrictId(Long cmProvinceId) {
        this.permDistrictId = cmProvinceId;
    }

    public Long getPermTownId() {
        return permTownId;
    }

    public void setPermTownId(Long cmProvinceId) {
        this.permTownId = cmProvinceId;
    }

    public Set<SkillDTO> getSkills() {
        return skills;
    }

    public void setSkills(Set<SkillDTO> skills) {
        this.skills = skills;
    }

    public AppParamsDTO getLevel() {
        return level;
    }

    public void setLevel(AppParamsDTO level) {
        this.level = level;
    }

    public AppParamsDTO getRole() {
        return role;
    }

    public void setRole(AppParamsDTO role) {
        this.role = role;
    }

    public AppParamsDTO getBranch() {
        return branch;
    }

    public void setBranch(AppParamsDTO branch) {
        this.branch = branch;
    }

    public AppParamsDTO getUnit() {
        return unit;
    }

    public void setUnit(AppParamsDTO unit) {
        this.unit = unit;
    }

    public AppParamsDTO getContractType() {
        return contractType;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public void setContractType(AppParamsDTO contractType) {
        this.contractType = contractType;
    }

    public MultipartFile[] getCvFiles() {
        return cvFiles;
    }

    public void setCvFiles(MultipartFile[] cvFiles) {
        this.cvFiles = cvFiles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        HrmEmployeeDTO hrmEmployeeDTO = (HrmEmployeeDTO) o;
        if (hrmEmployeeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), hrmEmployeeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HrmEmployeeDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", fullname='" + getFullname() + "'" +
            ", salPmtType='" + getSalPmtType() + "'" +
            ", supportAmt=" + getSupportAmt() +
            ", cntVldFrmDt='" + getCntVldFrmDt() + "'" +
            ", cntVldToDt='" + getCntVldToDt() + "'" +
            ", slrBefore='" + getSlrBefore() + "'" +
            ", slrAmtEnc='" + getSlrAmtEnc() + "'" +
            ", dob='" + getDob() + "'" +
            ", edu='" + getEdu() + "'" +
            ", gender='" + getGender() + "'" +
            ", idNo='" + getIdNo() + "'" +
            ", idIssueDtm='" + getIdIssueDtm() + "'" +
            ", status='" + getStatus() + "'" +
            ", email='" + getEmail() + "'" +
            ", phone='" + getPhone() + "'" +
            ", phone2='" + getPhone2() + "'" +
            ", salaryDate='" + getSalaryDate() + "'" +
            ", isInsource='" + getIsInsource() + "'" +
            ", taxNo='" + getTaxNo() + "'" +
            ", tgtgBhxh=" + getTgtgBhxh() +
            ", bhxhNo='" + getBhxhNo() + "'" +
            ", bhytNo='" + getBhytNo() + "'" +
            ", bhThatNghiep='" + getBhThatNghiep() + "'" +
            ", lastUpdate='" + getLastUpdate() + "'" +
            ", updateBy='" + getUpdateBy() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", cv='" + getCv() + "'" +
            ", level=" + getLevelId() +
            ", role=" + getRoleId() +
            ", branch=" + getBranchId() +
            ", unit=" + getUnitId() +
            ", contractType=" + getContractTypeId() +
            ", permProvince=" + getPermProvinceId() +
            ", permDistrict=" + getPermDistrictId() +
            ", permTown=" + getPermTownId() +
            "}";
    }
}
