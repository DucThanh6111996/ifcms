package com.ifw.cms.service.dto;

public class ExpEmployeeDTO {
    private String code;
    private String name;
    private Integer countExpEmployee;

    public ExpEmployeeDTO() {
    }

    public ExpEmployeeDTO(String code, String name, Integer countExpEmployee) {
        this.code = code;
        this.name = name;
        this.countExpEmployee = countExpEmployee;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCountExpEmployee() {
        return countExpEmployee;
    }

    public void setCountExpEmployee(Integer countExpEmployee) {
        this.countExpEmployee = countExpEmployee;
    }
}
