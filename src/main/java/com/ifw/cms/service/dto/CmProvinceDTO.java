package com.ifw.cms.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 */
public class CmProvinceDTO implements Serializable {

    private Long id;

    private String code;

    private String name;

    private String description;


    private Long parentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long cmProvinceId) {
        this.parentId = cmProvinceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CmProvinceDTO cmProvinceDTO = (CmProvinceDTO) o;
        if (cmProvinceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), cmProvinceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CmProvinceDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", parent=" + getParentId() +
            "}";
    }
}
