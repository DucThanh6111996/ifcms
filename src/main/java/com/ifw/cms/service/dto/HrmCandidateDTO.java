package com.ifw.cms.service.dto;

import com.ifw.cms.entity.*;
import org.springframework.web.multipart.MultipartFile;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.*;

public class HrmCandidateDTO implements Serializable {

    private Long id;

    @NotEmpty
    @Size(max = 50, min = 1)
    private String code;

    @NotEmpty
    private String fullname;

    @NotEmpty
    private String phone;

    private LocalDate dob;

    @NotEmpty
    private String email;

    @NotEmpty
    private String gender;

    private Long provinceId;

    private Long districtId;

    private Long wardId;


    private MultipartFile image;

    private Set<Skill> skills = new HashSet<>();

    private List<HrmCandidateExperience> experiences = new ArrayList<>();

    private List<HrmCandidateProject> projects = new ArrayList<>();

    private MultipartFile[] cvFiles;

    public HrmCandidateDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public Long getWardId() {
        return wardId;
    }

    public void setWardId(Long wardId) {
        this.wardId = wardId;
    }



    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public Set<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Set<Skill> skills) {
        this.skills = skills;
    }

    public List<HrmCandidateExperience> getExperiences() {
        return experiences;
    }

    public void setExperiences(List<HrmCandidateExperience> experiences) {
        this.experiences = experiences;
    }

    public List<HrmCandidateProject> getProjects() {
        return projects;
    }

    public void setProjects(List<HrmCandidateProject> projects) {
        this.projects = projects;
    }

    public MultipartFile[] getCvFiles() {
        return cvFiles;
    }

    public void setCvFiles(MultipartFile[] cvFiles) {
        this.cvFiles = cvFiles;
    }

    @Override
    public String toString() {
        return "HrmCandidateDTO{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", fullname='" + fullname + '\'' +
                ", dob=" + dob +
                ", gender='" + gender + '\'' +
                ", provinceId=" + provinceId +
                ", districtId=" + districtId +
                ", wardId=" + wardId +
                ", image=" + image +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", skills=" + skills +
                ", experiences=" + experiences +
                ", projects=" + projects +
                ", cvFiles=" + Arrays.toString(cvFiles) +
                '}';
    }
}
