package com.ifw.cms.service.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

public class ContractDTO {
    private Long id;
    @NotEmpty
    private String code;

    @NotEmpty
    private String name;

    @NotEmpty
    private String money;

    @NotNull
    private LocalDate start;

    @NotNull
    private LocalDate end;

    @NotNull
    private Integer number;

    @NotNull
    private Long biddingDocumentsId;

    public ContractDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Long getBiddingDocumentsId() {
        return biddingDocumentsId;
    }

    public void setBiddingDocumentsId(Long biddingDocumentsId) {
        this.biddingDocumentsId = biddingDocumentsId;
    }
}
