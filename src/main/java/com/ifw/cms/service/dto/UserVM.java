package com.ifw.cms.service.dto;

import javax.validation.constraints.Null;
import java.util.List;

/**
 * Created by ITSOL SWIB Dev Team.
 */
public class UserVM {
    private Long userId;
    private String userName;
    private String email;
    private String encrytedPassword;
    private boolean enabled;

    @Null
    private List<RoleVM> roles;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<RoleVM> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleVM> roles) {
        this.roles = roles;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEncrytedPassword() {
        return encrytedPassword;
    }

    public void setEncrytedPassword(String encrytedPassword) {
        this.encrytedPassword = encrytedPassword;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "UserVM{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", roles=" + roles +
                '}';
    }
}
