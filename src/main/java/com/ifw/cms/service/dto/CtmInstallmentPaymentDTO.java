package com.ifw.cms.service.dto;

import java.time.LocalDate;

public class CtmInstallmentPaymentDTO {

    private Long id;
    private String name;
    private String content;
    private  String money;
    private int installment;
    private LocalDate dop;
//    private Long customerPaymentId;

    public CtmInstallmentPaymentDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public int getInstallment() {
        return installment;
    }

    public void setInstallment(int installment) {
        this.installment = installment;
    }

    public LocalDate getDop() {
        return dop;
    }

    public void setDop(LocalDate dop) {
        this.dop = dop;
    }
}
