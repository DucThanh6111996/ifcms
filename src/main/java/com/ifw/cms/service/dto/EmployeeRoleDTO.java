package com.ifw.cms.service.dto;

public class EmployeeRoleDTO {
    private String code;
    private String name;
    private Integer countRoleEmployee;

    public EmployeeRoleDTO() {
    }

    public EmployeeRoleDTO(String code, String name, Integer countRoleEmployee) {
        this.code = code;
        this.name = name;
        this.countRoleEmployee = countRoleEmployee;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCountRoleEmployee() {
        return countRoleEmployee;
    }

    public void setCountRoleEmployee(Integer countRoleEmployee) {
        this.countRoleEmployee = countRoleEmployee;
    }
}
