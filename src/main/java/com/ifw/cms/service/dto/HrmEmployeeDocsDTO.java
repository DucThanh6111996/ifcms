package com.ifw.cms.service.dto;

import javax.persistence.Lob;
import java.io.Serializable;
import java.util.Objects;

/**
 */
public class HrmEmployeeDocsDTO implements Serializable {

    private Long id;

    @Lob
    private byte[] syll;

    private String syllContentType;
    @Lob
    private byte[] cmnd;

    private String cmndContentType;
    @Lob
    private byte[] sohk;

    private String sohkContentType;
    @Lob
    private byte[] giayKs;

    private String giayKsContentType;
    @Lob
    private byte[] giayKhamSk;

    private String giayKhamSkContentType;
    private String khac;


    private Long employeeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getSyll() {
        return syll;
    }

    public void setSyll(byte[] syll) {
        this.syll = syll;
    }

    public String getSyllContentType() {
        return syllContentType;
    }

    public void setSyllContentType(String syllContentType) {
        this.syllContentType = syllContentType;
    }

    public byte[] getCmnd() {
        return cmnd;
    }

    public void setCmnd(byte[] cmnd) {
        this.cmnd = cmnd;
    }

    public String getCmndContentType() {
        return cmndContentType;
    }

    public void setCmndContentType(String cmndContentType) {
        this.cmndContentType = cmndContentType;
    }

    public byte[] getSohk() {
        return sohk;
    }

    public void setSohk(byte[] sohk) {
        this.sohk = sohk;
    }

    public String getSohkContentType() {
        return sohkContentType;
    }

    public void setSohkContentType(String sohkContentType) {
        this.sohkContentType = sohkContentType;
    }

    public byte[] getGiayKs() {
        return giayKs;
    }

    public void setGiayKs(byte[] giayKs) {
        this.giayKs = giayKs;
    }

    public String getGiayKsContentType() {
        return giayKsContentType;
    }

    public void setGiayKsContentType(String giayKsContentType) {
        this.giayKsContentType = giayKsContentType;
    }

    public byte[] getGiayKhamSk() {
        return giayKhamSk;
    }

    public void setGiayKhamSk(byte[] giayKhamSk) {
        this.giayKhamSk = giayKhamSk;
    }

    public String getGiayKhamSkContentType() {
        return giayKhamSkContentType;
    }

    public void setGiayKhamSkContentType(String giayKhamSkContentType) {
        this.giayKhamSkContentType = giayKhamSkContentType;
    }

    public String getKhac() {
        return khac;
    }

    public void setKhac(String khac) {
        this.khac = khac;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long hrmEmployeeId) {
        this.employeeId = hrmEmployeeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        HrmEmployeeDocsDTO hrmEmployeeDocsDTO = (HrmEmployeeDocsDTO) o;
        if (hrmEmployeeDocsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), hrmEmployeeDocsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HrmEmployeeDocsDTO{" +
            "id=" + getId() +
            ", syll='" + getSyll() + "'" +
            ", cmnd='" + getCmnd() + "'" +
            ", sohk='" + getSohk() + "'" +
            ", giayKs='" + getGiayKs() + "'" +
            ", giayKhamSk='" + getGiayKhamSk() + "'" +
            ", khac='" + getKhac() + "'" +
            ", employee=" + getEmployeeId() +
            "}";
    }
}
