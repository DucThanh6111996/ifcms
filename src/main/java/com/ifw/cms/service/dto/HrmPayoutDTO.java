package com.ifw.cms.service.dto;

import com.ifw.cms.entity.enumeration.PaymentStatus;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 */
public class HrmPayoutDTO implements Serializable {

    private Long id;

    @NotNull
    private String payType;

    @NotNull
    private String payTo;

    @NotNull
    private String amount;

    private String prdId;

    private PaymentStatus status;

    private String description;


    private Long employeeId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPayTo() {
        return payTo;
    }

    public void setPayTo(String payTo) {
        this.payTo = payTo;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public PaymentStatus getStatus() {
        return status;
    }

    public void setStatus(PaymentStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long hrmEmployeeId) {
        this.employeeId = hrmEmployeeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        HrmPayoutDTO hrmPayoutDTO = (HrmPayoutDTO) o;
        if (hrmPayoutDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), hrmPayoutDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HrmPayoutDTO{" +
            "id=" + getId() +
            ", payType='" + getPayType() + "'" +
            ", payTo='" + getPayTo() + "'" +
            ", amount='" + getAmount() + "'" +
            ", prdId='" + getPrdId() + "'" +
            ", status='" + getStatus() + "'" +
            ", description='" + getDescription() + "'" +
            ", employee=" + getEmployeeId() +
            "}";
    }
}
