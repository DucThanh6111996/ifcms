package com.ifw.cms.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by ITSOL SWIB Dev Team.
 */
public class AppUserDTO {
    private Long userId;
    private String userName;

    @Size(max = 255, min = 3)
    private String oldPassword;

    @NotNull
    @Size(max = 255, min = 6)
    private String password;

    @NotNull
    @Size(max = 255, min = 6)
    private String confirmPassword;

    public Long getUserId() {
        return userId;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
