package com.ifw.cms.service.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 */
public class AppParamsDTO implements Serializable {

    private Long id;

    @NotNull
    @NotEmpty
    @Size(max = 10)
    private String code;

    @NotNull
    @NotEmpty
    @Size(max = 50)
    private String name;

    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String value;

    @NotNull
    @NotEmpty
    @Size(max = 255)
    private String fullname;

    @NotNull
    @NotEmpty
    @Size(max = 10)
    private String groupCode;

    @NotNull
    @NotEmpty
    @Size(max = 1)
    private String status;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
        this.groupCode = groupCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AppParamsDTO appParamsDTO = (AppParamsDTO) o;
        if (appParamsDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), appParamsDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AppParamsDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            ", value='" + getValue() + "'" +
            ", fullname='" + getFullname() + "'" +
            ", groupCode='" + getGroupCode() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
