package com.ifw.cms.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

/**
 */
public class AppParamsExtraDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 10)
    private String key;

    @NotNull
    @Size(max = 255)
    private String value;


    private Long paramId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getParamId() {
        return paramId;
    }

    public void setParamId(Long appParamsId) {
        this.paramId = appParamsId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AppParamsExtraDTO appParamsExtraDTO = (AppParamsExtraDTO) o;
        if (appParamsExtraDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), appParamsExtraDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "AppParamsExtraDTO{" +
            "id=" + getId() +
            ", key='" + getKey() + "'" +
            ", value='" + getValue() + "'" +
            ", param=" + getParamId() +
            "}";
    }
}
