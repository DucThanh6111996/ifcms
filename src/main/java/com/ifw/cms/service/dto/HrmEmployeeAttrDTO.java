package com.ifw.cms.service.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 */
public class HrmEmployeeAttrDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 10)
    private String key;

    @NotNull
    @Size(max = 500)
    private String value;


    private Set<AppParamsDTO> params = new HashSet<>();

    private Long employeeIdId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Set<AppParamsDTO> getParams() {
        return params;
    }

    public void setParams(Set<AppParamsDTO> appParams) {
        this.params = appParams;
    }

    public Long getEmployeeIdId() {
        return employeeIdId;
    }

    public void setEmployeeIdId(Long hrmEmployeeId) {
        this.employeeIdId = hrmEmployeeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        HrmEmployeeAttrDTO hrmEmployeeAttrDTO = (HrmEmployeeAttrDTO) o;
        if (hrmEmployeeAttrDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), hrmEmployeeAttrDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "HrmEmployeeAttrDTO{" +
            "id=" + getId() +
            ", key='" + getKey() + "'" +
            ", value='" + getValue() + "'" +
            ", employeeId=" + getEmployeeIdId() +
            "}";
    }
}
