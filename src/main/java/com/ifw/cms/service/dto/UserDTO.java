package com.ifw.cms.service.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by ITSOL SWIB Dev Team.
 */
@Getter
@Setter
public class UserDTO {
    private Long userId;
    private String userName;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
