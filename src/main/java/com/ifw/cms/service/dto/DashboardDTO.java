package com.ifw.cms.service.dto;

public class DashboardDTO {
  private Long totalEmp;
  private Long totalEmpFemale;
  private Long totalEmpMale;
  private Long totalDev;
  private Long totalTest;
  private Long totalOffice;
  private String kei;
  private String val;

  /**
   * @return the totalEmp
   */
  public Long getTotalEmp() {
    return totalEmp;
  }

  /**
   * @param totalEmp the totalEmp to set
   */
  public void setTotalEmp(Long totalEmp) {
    this.totalEmp = totalEmp;
  }

  /**
   * @return the totalEmpFemale
   */
  public Long getTotalEmpFemale() {
    return totalEmpFemale;
  }

  /**
   * @param totalEmpFemale the totalEmpFemale to set
   */
  public void setTotalEmpFemale(Long totalEmpFemale) {
    this.totalEmpFemale = totalEmpFemale;
  }

  /**
   * @return the totalEmpMale
   */
  public Long getTotalEmpMale() {
    return totalEmpMale;
  }

  /**
   * @param totalEmpMale the totalEmpMale to set
   */
  public void setTotalEmpMale(Long totalEmpMale) {
    this.totalEmpMale = totalEmpMale;
  }

  /**
   * @return the totalDev
   */
  public Long getTotalDev() {
    return totalDev;
  }

  /**
   * @param totalDev the totalDev to set
   */
  public void setTotalDev(Long totalDev) {
    this.totalDev = totalDev;
  }

  /**
   * @return the totalTest
   */
  public Long getTotalTest() {
    return totalTest;
  }

  /**
   * @param totalTest the totalTest to set
   */
  public void setTotalTest(Long totalTest) {
    this.totalTest = totalTest;
  }

  /**
   * @return the totalOffice
   */
  public Long getTotalOffice() {
    return totalOffice;
  }

  /**
   * @param totalOffice the totalOffice to set
   */
  public void setTotalOffice(Long totalOffice) {
    this.totalOffice = totalOffice;
  }

}
