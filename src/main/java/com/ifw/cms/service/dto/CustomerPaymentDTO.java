package com.ifw.cms.service.dto;

import com.ifw.cms.entity.CtmInstallmentPayment;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CustomerPaymentDTO implements Serializable {
    private Long id;

    @NotEmpty
    private String code;

    @NotEmpty
    private String name;

    @NotEmpty
    private String type;

//    @NotNull
    private Long customerId;

    @NotNull
    private Long contractId;

    private List<CtmInstallmentPayment> ctmInstallmentPayments = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public Long getContractId() {
        return contractId;
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public List<CtmInstallmentPayment> getCtmInstallmentPayments() {
        return ctmInstallmentPayments;
    }

    public void setCtmInstallmentPayments(List<CtmInstallmentPayment> ctmInstallmentPayments) {
        this.ctmInstallmentPayments = ctmInstallmentPayments;
    }

}
