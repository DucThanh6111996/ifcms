package com.ifw.cms.service.dto;

public class EmployeeStatisticDto {
    private String code;
    private String name;
    private Integer totalEmployee;

    public EmployeeStatisticDto() {
    }

    public EmployeeStatisticDto(String code, String name, Integer totalEmployee) {
        this.code = code;
        this.name = name;
        this.totalEmployee = totalEmployee;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTotalEmployee() {
        return totalEmployee;
    }

    public void setTotalEmployee(Integer totalEmployee) {
        this.totalEmployee = totalEmployee;
    }
}
