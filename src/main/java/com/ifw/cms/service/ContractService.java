package com.ifw.cms.service;

import com.ifw.cms.entity.Contract;
import com.ifw.cms.service.dto.ContractDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ContractService {
    ContractDTO save(ContractDTO contractDTO);
    void delete (Long id);
    Page<ContractDTO> findAll(Pageable pageable);
    ContractDTO findById(Long id);
    Optional<Contract> findByIdEntity(Long id);
    Page<ContractDTO> search(Pageable pageable, String code, String name);
    List<Integer> checkFieldExists(String code);
}
