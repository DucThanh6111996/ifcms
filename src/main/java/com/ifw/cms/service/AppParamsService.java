package com.ifw.cms.service;

import com.ifw.cms.entity.AppParams;
import com.ifw.cms.entity.Province;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.ifw.cms.service.dto.*;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

/**
 */
public interface AppParamsService {

    /**
     * Save a appParams.
     *
     * @param appParamsDTO the entity to save.
     * @return the persisted entity.
     */
     AppParamsDTO save(AppParamsDTO appParamsDTO);

    /**
     * Get all the appParams.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AppParamsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" appParams.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AppParamsDTO> findOne(Long id);

    /**
     * Delete the "id" appParams.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Find by group code
     */
    List<AppParamsDTO> findByGroupCode(String groupCode);
    AppParamsDTO findById(Long id);

    List<Integer> checkFieldExists(String code, String name);

    Page<AppParamsDTO> findByGroupCodePaged(String groupCode, String key, Pageable pageable);

}
