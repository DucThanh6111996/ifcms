package com.ifw.cms.service;

import com.ifw.cms.entity.Customer;
import com.ifw.cms.entity.HrmEmployee;
import com.ifw.cms.service.dto.CustomerDTO;
import com.ifw.cms.service.dto.ExpEmployeeDTO;
import com.ifw.cms.service.dto.HrmEmployeeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.internet.MimeMessage;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;

/**
 * Service for sending emails.
 * <p>
 * We use the {@link Async} annotation to send emails asynchronously.
 */
@Service
public class MailService {

    private final Logger log = LoggerFactory.getLogger(MailService.class);

    private static final String USER = "user";

    private static final String EMPLOYEES = "employees";

    private static final String TOTAL = "total";

    private static final String BASE_URL = "baseUrl";

    private final JavaMailSender javaMailSender;

    private final MessageSource messageSource;

    private final SpringTemplateEngine templateEngine;

    @Value("${application.notification.email.salary.to}")
    private String[] salaryNotifyTo;

    public MailService(JavaMailSender javaMailSender,
                       MessageSource messageSource, SpringTemplateEngine templateEngine) {
        this.javaMailSender = javaMailSender;
        this.messageSource = messageSource;
        this.templateEngine = templateEngine;
    }

    /*
     * Send to multiple user
     *
     * @param to
     * @param subject
     * @param content
     * @param isMultipart
     * @param isHtml
     */
    @Async
    public void sendEmail(String[] to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
                isMultipart, isHtml, to, subject, content);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
            message.setTo(to);
            message.setFrom("itsupport@itsol.vn");
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", to);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.warn("Email could not be sent to user '{}'", to, e);
            } else {
                log.warn("Email could not be sent to user '{}': {}", to, e.getMessage());
            }
        }
    }

    /**
     * Send to one
     *
     * @param to
     * @param subject
     * @param content
     * @param isMultipart
     * @param isHtml
     */
    @Async
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
                isMultipart, isHtml, to, subject, content);
        String[] arrTo = {to};

        // send email
        sendEmail(arrTo, subject, content, isMultipart, isHtml);
    }

    @Async
    public void sendEmailFromTemplate(List<HrmEmployee> employees, BigDecimal total, String templateName, String titleKey) {
        Locale locale = Locale.forLanguageTag("en");
        Context context = new Context(locale);
        context.setVariable(EMPLOYEES, employees);
        context.setVariable(TOTAL, total);
        context.setVariable(BASE_URL, "http://itsol.vn");
        String content = templateEngine.process(templateName, context);
        String subject = messageSource.getMessage(titleKey, null, locale);
        sendEmail(salaryNotifyTo, subject, content, false, true);
    }

    @Async
    public void sendAPaymentByPeriodEmail(List<HrmEmployee> employees, BigDecimal total) {
        sendEmailFromTemplate(employees, total, "mail/paymentByPeriod", "email.payout.title");
    }

    /**
     * Send new password to user
     *
     * @param userName
     * @param newPassword
     */

    @Async
    public void sendMailResetPassword(String email, String userName, String newPassword) {
        Locale locale = Locale.forLanguageTag("en");
        Context context = new Context(locale);
        context.setVariable("userName", userName);
        context.setVariable("password", newPassword);
        context.setVariable(BASE_URL, "http://itsol.vn");
        String content = templateEngine.process("mail/resetpwd", context);
        String subject = "Password Reset";
        sendEmail(email, subject, content, false, true);
    }

    /**
     * ThanhDT
     * gui mail nhan vien sap het han hop dong
     */

    @Async
    public void sendMailExpired(HrmEmployeeDTO admin, List<HrmEmployeeDTO> listEmployeeExpired) {
        Locale locale = Locale.forLanguageTag("en");
        Context context = new Context(locale);
        context.setVariable(BASE_URL, "http://itsol.vn");
        context.setVariable("employees", listEmployeeExpired);
        String content = templateEngine.process("mail/employeeManager", context);
        String subject = messageSource.getMessage("Thông báo hết hạn hợp đồng", null, locale);
        sendEmail(admin.getEmail(), subject, content, false, true);
    }

    // gui mail cho khach hang
    @Async
    public void sendMailCustomer(CustomerDTO customerDTO) {
        Locale locale = Locale.forLanguageTag("en");
        Context context = new Context(locale);
        context.setVariable(BASE_URL, "http://itsol.vn");
        context.setVariable("contracts", customerDTO);
        String content = templateEngine.process("mail/contractManager", context);
        String subject = messageSource.getMessage("Thông báo hết hạn", null, locale);
        sendEmail(customerDTO.getEmail(), subject, content, false, true);
    }
}
