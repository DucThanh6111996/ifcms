package com.ifw.cms.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.ifw.cms.service.dto.*;
import java.util.Optional;

/**
 */
public interface CmActionLogService {

    /**
     * Save a cmActionLog.
     *
     * @param cmActionLogDTO the entity to save.
     * @return the persisted entity.
     */
    CmActionLogDTO save(CmActionLogDTO cmActionLogDTO);

    /**
     * Get all the cmActionLogs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CmActionLogDTO> findAll(Pageable pageable);


    /**
     * Get the "id" cmActionLog.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CmActionLogDTO> findOne(Long id);

    /**
     * Delete the "id" cmActionLog.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
