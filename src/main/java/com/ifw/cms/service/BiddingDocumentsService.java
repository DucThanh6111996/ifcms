package com.ifw.cms.service;

import com.ifw.cms.service.dto.BiddingDocumentsDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BiddingDocumentsService {
    BiddingDocumentsDTO save(BiddingDocumentsDTO biddingDocumentsDTO);
    void delete (Long id);
    Page<BiddingDocumentsDTO> findAll(Pageable pageable);
    BiddingDocumentsDTO findById(Long id);
    Page<BiddingDocumentsDTO> search(Pageable pageable, String code, String name);
    List<BiddingDocumentsDTO> findByStatus();
    List<Integer> checkFieldExists(String code);
}
