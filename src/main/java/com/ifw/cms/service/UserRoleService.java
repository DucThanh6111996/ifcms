package com.ifw.cms.service;
/**
 */
public interface UserRoleService {

    /**
     * deleteRoleByUserId
     * @param userId
     * @return
     */
    int deleteRoleByUserId(Long userId);

    void saveAll(Iterable items);

    void addUserRole(Long userId, Long roleId);

    /**
     * Delete the "id" skill.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
