package com.ifw.cms.service;

import com.ifw.cms.service.dto.*;
import java.util.Optional;

public interface DashboardService {
  Optional<DashboardDTO> getStat();
}
