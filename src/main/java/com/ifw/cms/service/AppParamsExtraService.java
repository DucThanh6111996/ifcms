package com.ifw.cms.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.ifw.cms.service.dto.*;

import java.util.Optional;

/**
 */
public interface AppParamsExtraService {

    /**
     * Save a appParamsExtra.
     *
     * @param appParamsExtraDTO the entity to save.
     * @return the persisted entity.
     */
    AppParamsExtraDTO save(AppParamsExtraDTO appParamsExtraDTO);

    /**
     * Get all the appParamsExtras.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AppParamsExtraDTO> findAll(Pageable pageable);


    /**
     * Get the "id" appParamsExtra.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AppParamsExtraDTO> findOne(Long id);

    /**
     * Delete the "id" appParamsExtra.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
