package com.ifw.cms.service;

import com.ifw.cms.service.dto.CustomerPaymentDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
public interface CustomerPaymentService {
    CustomerPaymentDTO save(CustomerPaymentDTO customerPaymentDTO);
    void delete (Long id);
    Page<CustomerPaymentDTO> findAll(Pageable pageable);
    CustomerPaymentDTO findById(Long id);
    Page<CustomerPaymentDTO> search(Pageable pageable, String code, String name);

    List<Integer> checkFieldExists(String code);
}
