package com.ifw.cms.service.impl;

import com.ifw.cms.repository.UserRoleRepository;
import com.ifw.cms.service.UserRoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 */
@Service
@Transactional
public class UserRoleServiceImpl implements UserRoleService {

    private final Logger log = LoggerFactory.getLogger(UserRoleServiceImpl.class);

    private final UserRoleRepository userRoleRepository;

    public UserRoleServiceImpl(UserRoleRepository userRoleRepository) {
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public int deleteRoleByUserId(Long userId) {
        return userRoleRepository.deleteRoleByUserId(userId);
    }

    @Override
    public void addUserRole(Long userId, Long roleId) {
        userRoleRepository.addUserRole(userId, roleId);
    }

    @Override
    public void saveAll(Iterable items) {
        userRoleRepository.saveAll(items);
    }

    /**
     * Delete the skill by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Skill : {}", id);
        userRoleRepository.deleteById(id);
    }
}
