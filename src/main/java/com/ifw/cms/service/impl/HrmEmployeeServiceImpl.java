package com.ifw.cms.service.impl;

import com.ifw.cms.entity.HrmEmployee;
import com.ifw.cms.entity.enumeration.EduLevel;
import com.ifw.cms.entity.enumeration.SalaryPaymentType;
import com.ifw.cms.repository.AppParamsRepository;
import com.ifw.cms.repository.HrmEmployeeRepository;
import com.ifw.cms.repository.HrmEmployeeStatisticRepository;
import com.ifw.cms.service.HrmEmployeeService;
import com.ifw.cms.service.dto.*;
import com.ifw.cms.service.mapper.HrmEmployeeMapper;
import com.ifw.cms.utils.DateUtils;
import com.ifw.cms.utils.EncryptUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link HrmEmployee}.
 */
@Service
@Transactional
public class HrmEmployeeServiceImpl implements HrmEmployeeService {

    private final Logger log = LoggerFactory.getLogger(HrmEmployeeServiceImpl.class);

    private final HrmEmployeeRepository hrmEmployeeRepository;

    private final AppParamsRepository appParamsRepository;

    private final HrmEmployeeMapper hrmEmployeeMapper;

    private final HrmEmployeeStatisticRepository hrmEmployeeStatistic;

    public HrmEmployeeServiceImpl(HrmEmployeeRepository hrmEmployeeRepository,
                                  HrmEmployeeMapper hrmEmployeeMapper,
                                  AppParamsRepository appParamsRepository,
                                  HrmEmployeeStatisticRepository hrmEmployeeStatistic) {
        this.hrmEmployeeRepository = hrmEmployeeRepository;
        this.hrmEmployeeMapper = hrmEmployeeMapper;
        this.appParamsRepository = appParamsRepository;
        this.hrmEmployeeStatistic = hrmEmployeeStatistic;
    }

    /**
     * Save a hrmEmployee.
     *
     * @param hrmEmployeeDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public HrmEmployeeDTO save(HrmEmployeeDTO hrmEmployeeDTO) {
        log.debug("Request to save HrmEmployee : {}", hrmEmployeeDTO);
        HrmEmployee hrmEmployee = hrmEmployeeMapper.toEntity(hrmEmployeeDTO);
        //Encrypt sensitive info
        if (StringUtils.isNotEmpty(hrmEmployee.getSlrBefore())) {
            hrmEmployee.setSlrBefore(EncryptUtils.encrypt(hrmEmployee.getSlrBefore()));
        }
        if (StringUtils.isNotEmpty(hrmEmployee.getSlrAmtEnc())) {
            hrmEmployee.setSlrAmtEnc(EncryptUtils.encrypt(hrmEmployee.getSlrAmtEnc()));
        }

        hrmEmployee = hrmEmployeeRepository.save(hrmEmployee);
        return hrmEmployeeMapper.toDto(hrmEmployee);
    }

    /**
     * Get all the hrmEmployees.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<HrmEmployeeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all HrmEmployees");
        //TODO: Encrypt info
        return hrmEmployeeRepository.findAll(pageable)
                .map(hrmEmployeeMapper::toDto);
    }

    /**
     * Get all the hrmEmployees with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<HrmEmployeeDTO> findAllWithEagerRelationships(Pageable pageable) {
        return hrmEmployeeRepository.findAllWithEagerRelationships(pageable).map(hrmEmployeeMapper::toDto);
    }


    /**
     * Get one hrmEmployee by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<HrmEmployeeDTO> findOne(Long id) {
        log.debug("Request to get HrmEmployee : {}", id);
        Optional<HrmEmployeeDTO> dto = hrmEmployeeRepository.findOneWithEagerRelationships(id)
                .map(hrmEmployeeMapper::toDto);
        if (dto.isPresent()) {
            if (StringUtils.isNotEmpty(dto.get().getSlrBefore())) {
                dto.get().setSlrBefore(EncryptUtils.decrypt(dto.get().getSlrBefore()));
            }
            if (StringUtils.isNotEmpty(dto.get().getSlrAmtEnc())) {
                dto.get().setSlrAmtEnc(EncryptUtils.decrypt(dto.get().getSlrAmtEnc()));
            }
        }
        return dto;
    }

    /**
     * Delete the hrmEmployee by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete HrmEmployee : {}", id);
        hrmEmployeeRepository.deleteById(id);
    }

    @Override
    public Long importEmployee(HrmEmployeeDTO hrmEmployeeDTO) {
        log.debug("Prepare import imployee");
        try {
            ByteArrayInputStream inputFilestream = new ByteArrayInputStream(null);
            BufferedReader br = new BufferedReader(new InputStreamReader(inputFilestream, "UTF-8"));
            String line = "";
            int cnt = 0;

            HrmEmployee e;
            List<HrmEmployee> toInsertLst = new ArrayList();

            while ((line = br.readLine()) != null) {
                cnt++;
                if (cnt > 1) {
                    String[] r = line.split(",");
                    e = new HrmEmployee();
                    e.setCode(r[1]);
                    e.setFullname(r[2]);
                    e.setLevel(appParamsRepository.findByCodeAndGroupCode(r[3], "EM_LEVEL"));
                    e.setRole(appParamsRepository.findByCodeAndGroupCode(r[4], "EM_ROLE"));
                    e.setBranch(appParamsRepository.findByCodeAndGroupCode(r[5], "BRANCH"));
                    e.setUnit(appParamsRepository.findByCodeAndGroupCode(r[6], "UNIT"));

                    if (StringUtils.isNotEmpty(r[7])) {
                        if (r[7].equalsIgnoreCase("GROSS")) {
                            e.setSalPmtType(SalaryPaymentType.GROSS);
                        } else if (r[7].equalsIgnoreCase("NET")) {
                            e.setSalPmtType(SalaryPaymentType.NET);
                        }
                    }

                    e.setSupportAmt(null);
                    e.setContractType(appParamsRepository.findByCodeAndGroupCode(r[10], "EM_CNT_TYP"));

                    e.setCntVldFrmDt(DateUtils.convertStringToLocalDate(DateUtils.DATE_FORMAT_DDMMYYYY, r[11]));
                    e.setCntVldToDt(DateUtils.convertStringToLocalDate(DateUtils.DATE_FORMAT_DDMMYYYY, r[12]));
                    e.setPhone(r[19]);
                    e.setEmail(r[20]);
                    e.setDob(DateUtils.convertStringToLocalDate(DateUtils.DATE_FORMAT_DDMMYYYY, r[21]));

                    if (StringUtils.isNotEmpty(r[22])) {
                        if (r[22].equalsIgnoreCase("CAODANG")) {
                            e.setEdu(EduLevel.CAODANG);
                        } else if (r[22].equalsIgnoreCase("DAIHOC")) {
                            e.setEdu(EduLevel.DAIHOC);
                        } else if (r[22].equalsIgnoreCase("TRUNGCAP")) {
                            e.setEdu(EduLevel.TRUNGCAP);
                        } else if (r[22].equalsIgnoreCase("KHAC")) {
                            e.setEdu(EduLevel.KHAC);
                        }
                    }

                    if (StringUtils.isNotEmpty(r[23])) {
                        try {
                            e.setTgtgBhxh(Long.parseLong(r[23]));
                        } catch (Exception pe) {
                        }
                    }

                    e.setTaxNo(r[25]);
                    e.setBhxhNo(r[26]);
                    e.setBhThatNghiep(r[27]);
                    e.setBhytNo(r[28]);
                    e.setSlrAmtEnc(EncryptUtils.encrypt(r[30]));
                    e.setStatus("A");
                    e.setGender("M");

                    toInsertLst.add(e);
                }
                System.out.println(line);
            }
            br.close();
            hrmEmployeeRepository.saveAll(toInsertLst);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0L;
    }

    @Override
    public Page<HrmEmployeeDTO> search(Pageable pageable, String fullname, String email, String status) {
        return hrmEmployeeRepository.search(pageable, fullname, email, status)
                .map(hrmEmployeeMapper::toDto);
    }

    /**
     * ThanhDT
     **/
    @Override
    public List<EmployeeStatisticDto> statisticByUnit() {
        log.trace("Service to request statistic employee by unit.");
        return hrmEmployeeStatistic.statisticByUnit();
    }

    @Override
    public List<EmployeeRoleDTO> statisticByRole() {
        log.trace("Service to request statistic employee by role. ");
        return hrmEmployeeStatistic.statisticByRole();
    }

    @Override
    public List<ExpEmployeeDTO> statisticByExpEmployee() {
        log.trace("Service to request statistic employee by constract expiration. ");
        return hrmEmployeeStatistic.statisticByExpEmployee();
    }

    @Override
    public List<InsourceEmployeeDTO> statisticByInsourceEmployee() {
        log.trace("Service to request statistic employee by insource employee. ");
        return hrmEmployeeStatistic.statisticByInsourceEmployee();
    }


    @Override
    public boolean checkEqualFieldHrmEmployee(HrmEmployeeDTO hrmEmployeeDTO) {
        HrmEmployee hrmEmployee = hrmEmployeeMapper.toEntity(hrmEmployeeDTO);
        if (hrmEmployeeRepository.findUserByCondition(hrmEmployee.getCode(),
                hrmEmployee.getEmail(), hrmEmployee.getPhone()).size() > 0){
            return true;
        }
        return false;
    }

    /** ThanhDT - Thống kê những người hết hạn hợp đồng*/
    @Override
    public Page<HrmEmployeeDTO> getEmployeeContactExpired(Pageable pageable) {
        return hrmEmployeeRepository.findByEndDateBefore(pageable, LocalDate.now())
                .map(hrmEmployeeMapper::toDto);
    }

    @Override
    public List<HrmEmployeeDTO> getEmployeeExpired() {
        return hrmEmployeeStatistic.findEmployeeExpired();
    }

    @Override
    public List<HrmEmployeeDTO> getAdmin() {
        return hrmEmployeeStatistic.findAdmin();
    }
}
