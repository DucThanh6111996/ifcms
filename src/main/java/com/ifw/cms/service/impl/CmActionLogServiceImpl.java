package com.ifw.cms.service.impl;

import com.ifw.cms.entity.CmActionLog;
import com.ifw.cms.repository.CmActionLogRepository;
import com.ifw.cms.service.CmActionLogService;
import com.ifw.cms.service.dto.CmActionLogDTO;
import com.ifw.cms.service.mapper.CmActionLogMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 */
@Service
@Transactional
public class CmActionLogServiceImpl implements CmActionLogService {

    private final Logger log = LoggerFactory.getLogger(CmActionLogServiceImpl.class);

    private final CmActionLogRepository cmActionLogRepository;

    private final CmActionLogMapper cmActionLogMapper;

    public CmActionLogServiceImpl(CmActionLogRepository cmActionLogRepository, CmActionLogMapper cmActionLogMapper) {
        this.cmActionLogRepository = cmActionLogRepository;
        this.cmActionLogMapper = cmActionLogMapper;
    }

    /**
     * Save a cmActionLog.
     *
     * @param cmActionLogDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CmActionLogDTO save(CmActionLogDTO cmActionLogDTO) {
        log.debug("Request to save CmActionLog : {}", cmActionLogDTO);
        CmActionLog cmActionLog = cmActionLogMapper.toEntity(cmActionLogDTO);
        cmActionLog = cmActionLogRepository.save(cmActionLog);
        return cmActionLogMapper.toDto(cmActionLog);
    }

    /**
     * Get all the cmActionLogs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CmActionLogDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CmActionLogs");
        return cmActionLogRepository.findAll(pageable)
            .map(cmActionLogMapper::toDto);
    }


    /**
     * Get one cmActionLog by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CmActionLogDTO> findOne(Long id) {
        log.debug("Request to get CmActionLog : {}", id);
        return cmActionLogRepository.findById(id)
            .map(cmActionLogMapper::toDto);
    }

    /**
     * Delete the cmActionLog by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CmActionLog : {}", id);
        cmActionLogRepository.deleteById(id);
    }
}
