package com.ifw.cms.service.impl;

import com.ifw.cms.repository.DashboardRepository;
import com.ifw.cms.service.DashboardService;
import com.ifw.cms.service.dto.DashboardDTO;
import com.ifw.cms.service.mapper.DashboardMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class DashboardServiceImpl implements DashboardService {

  private final DashboardRepository dashboardRepository;
  private final DashboardMapper dashboardMapper;

  public DashboardServiceImpl(DashboardRepository dashboardRepository,
      DashboardMapper dashboardMapper) {
    this.dashboardRepository = dashboardRepository;
    this.dashboardMapper = dashboardMapper;
  }

  @Override
  public Optional<DashboardDTO> getStat() {
    DashboardDTO dto = dashboardMapper.toDto(dashboardRepository.getStat());
    return Optional.ofNullable(dto);
  }
}
