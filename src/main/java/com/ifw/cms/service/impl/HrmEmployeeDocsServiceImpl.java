package com.ifw.cms.service.impl;

import com.ifw.cms.entity.HrmEmployeeDocs;
import com.ifw.cms.repository.HrmEmployeeDocsRepository;
import com.ifw.cms.service.HrmEmployeeDocsService;
import com.ifw.cms.service.dto.HrmEmployeeDocsDTO;
import com.ifw.cms.service.mapper.HrmEmployeeDocsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 */
@Service
@Transactional
public class HrmEmployeeDocsServiceImpl implements HrmEmployeeDocsService {

    private final Logger log = LoggerFactory.getLogger(HrmEmployeeDocsServiceImpl.class);

    private final HrmEmployeeDocsRepository hrmEmployeeDocsRepository;

    private final HrmEmployeeDocsMapper hrmEmployeeDocsMapper;

    public HrmEmployeeDocsServiceImpl(HrmEmployeeDocsRepository hrmEmployeeDocsRepository, HrmEmployeeDocsMapper hrmEmployeeDocsMapper) {
        this.hrmEmployeeDocsRepository = hrmEmployeeDocsRepository;
        this.hrmEmployeeDocsMapper = hrmEmployeeDocsMapper;
    }

    /**
     * Save a hrmEmployeeDocs.
     *
     * @param hrmEmployeeDocsDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public HrmEmployeeDocsDTO save(HrmEmployeeDocsDTO hrmEmployeeDocsDTO) {
        log.debug("Request to save HrmEmployeeDocs : {}", hrmEmployeeDocsDTO);
        HrmEmployeeDocs hrmEmployeeDocs = hrmEmployeeDocsMapper.toEntity(hrmEmployeeDocsDTO);
        hrmEmployeeDocs = hrmEmployeeDocsRepository.save(hrmEmployeeDocs);
        return hrmEmployeeDocsMapper.toDto(hrmEmployeeDocs);
    }

    /**
     * Get all the hrmEmployeeDocs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<HrmEmployeeDocsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all HrmEmployeeDocs");
        return hrmEmployeeDocsRepository.findAll(pageable)
            .map(hrmEmployeeDocsMapper::toDto);
    }


    /**
     * Get one hrmEmployeeDocs by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<HrmEmployeeDocsDTO> findOne(Long id) {
        log.debug("Request to get HrmEmployeeDocs : {}", id);
        return hrmEmployeeDocsRepository.findById(id)
            .map(hrmEmployeeDocsMapper::toDto);
    }

    /**
     * Delete the hrmEmployeeDocs by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete HrmEmployeeDocs : {}", id);
        hrmEmployeeDocsRepository.deleteById(id);
    }
}
