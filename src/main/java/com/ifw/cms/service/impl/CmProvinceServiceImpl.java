package com.ifw.cms.service.impl;

import com.ifw.cms.entity.CmProvince;
import com.ifw.cms.repository.CmProvinceRepository;
import com.ifw.cms.service.CmProvinceService;
import com.ifw.cms.service.dto.CmProvinceDTO;
import com.ifw.cms.service.mapper.CmProvinceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 */
@Service
@Transactional
public class CmProvinceServiceImpl implements CmProvinceService {

    private final Logger log = LoggerFactory.getLogger(CmProvinceServiceImpl.class);

    private final CmProvinceRepository cmProvinceRepository;

    private final CmProvinceMapper cmProvinceMapper;

    public CmProvinceServiceImpl(CmProvinceRepository cmProvinceRepository, CmProvinceMapper cmProvinceMapper) {
        this.cmProvinceRepository = cmProvinceRepository;
        this.cmProvinceMapper = cmProvinceMapper;
    }

    /**
     * Save a cmProvince.
     *
     * @param cmProvinceDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CmProvinceDTO save(CmProvinceDTO cmProvinceDTO) {
        log.debug("Request to save CmProvince : {}", cmProvinceDTO);
        CmProvince cmProvince = cmProvinceMapper.toEntity(cmProvinceDTO);
        cmProvince = cmProvinceRepository.save(cmProvince);
        return cmProvinceMapper.toDto(cmProvince);
    }

    /**
     * Get all the cmProvinces.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CmProvinceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CmProvinces");
        return cmProvinceRepository.findAll(pageable)
            .map(cmProvinceMapper::toDto);
    }


    /**
     * Get all the cmProvinces.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<CmProvinceDTO> findAll() {
        log.debug("Request to get all CmProvinces");
        List<CmProvince> provinces = cmProvinceRepository.findAll();
        return cmProvinceMapper.toListDTO(provinces);
    }

    /**
     * Get one cmProvince by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CmProvinceDTO> findOne(Long id) {
        log.debug("Request to get CmProvince : {}", id);
        return cmProvinceRepository.findById(id)
            .map(cmProvinceMapper::toDto);
    }

    /**
     * Delete the cmProvince by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CmProvince : {}", id);
        cmProvinceRepository.deleteById(id);
    }
}
