package com.ifw.cms.service.impl;

import com.ifw.cms.entity.CustomerBranch;
import com.ifw.cms.repository.CustomerBranchRepository;
import com.ifw.cms.service.CustomerBranchService;
import com.ifw.cms.service.dto.CustomerBranchDTO;
import com.ifw.cms.service.mapper.CustomerBranchMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Ninh Trang An 28/10/2019
 */
@Service
public class CustomerBranchServiceImpl implements CustomerBranchService {

    @Autowired
    private CustomerBranchRepository customerBranchRepository;

    @Autowired
    private CustomerBranchMapper customerBranchMapper;

    @Override
    public List<CustomerBranch> findByCustomerId(Long id) {
        return customerBranchRepository.findByCustomerId(id);
    }

    @Override
    public CustomerBranchDTO findById(Long id) {
        return customerBranchMapper.toDto(customerBranchRepository.findById(id).get());
    }

    @Override
    public Page<CustomerBranchDTO> findAll(Pageable pageable) {
        return customerBranchRepository.findAll(pageable).map(customerBranchMapper::toDto);
    }
}
