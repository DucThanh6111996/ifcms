package com.ifw.cms.service.impl;

import com.ifw.cms.entity.Customer;
import com.ifw.cms.repository.CustomerManagerRepository;
import com.ifw.cms.repository.CustomerRepository;
import com.ifw.cms.service.CustomerService;
import com.ifw.cms.service.dto.CustomerDTO;
import com.ifw.cms.service.mapper.CustomerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    private final CustomerManagerRepository customerManagerRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerMapper customerMapper;

    public CustomerServiceImpl(CustomerManagerRepository customerManagerRepository,
                               CustomerRepository customerRepository,
                               CustomerMapper customerMapper) {
        this.customerManagerRepository = customerManagerRepository;
        this.customerRepository = customerRepository;
        this.customerMapper = customerMapper;
    }

    @Override
    public CustomerDTO save(CustomerDTO customerDTO)
    {
        Customer customer = customerMapper.toEntity(customerDTO);
        customerRepository.save(customer);
        return customerDTO;
    }

    @Override
    public void delete(Long id) {
        customerRepository.deleteById(id);
    }

    @Override
    public Page<CustomerDTO> search(Pageable pageable, String code, String name) {
        return customerRepository.search(pageable, code, name).map(customerMapper::toDto);
    }

    @Override
    public Page<CustomerDTO> findAll(Pageable pageable) {
        return customerRepository.findAll(pageable).map(customerMapper::toDto);
    }

    @Override
    public CustomerDTO findById(Long id) {
        return customerMapper.toDto(customerRepository.findById(id).get());
    }

    @Override
    public Optional<Customer> findByCode(String code) {
        return customerRepository.findByCode(code);
    }

    @Override
    public List<Integer> checkFieldExists(String code, String name) {
        List<Customer> customerList = customerRepository.findAllByCodeOrName(code, name);
        List<Integer> result = new ArrayList<>();
        for(Customer customer : customerList)
        {
            if(customer.getCode().equals(code))
            {
                result.add(1);
            }
            if(customer.getName().equals(name))
            {
                result.add(2);
            }
        }
        return result;
    }

    // lấy danh sách email khách hàng
    @Override
    public List<CustomerDTO> getListCustomer() {
        return customerManagerRepository.getAllEmailCustomer();
    }

    // lấy danh sách hợp đồng
    @Override
    public List<CustomerDTO> getListContract() {
        return customerManagerRepository.getAllContract();
    }


}
