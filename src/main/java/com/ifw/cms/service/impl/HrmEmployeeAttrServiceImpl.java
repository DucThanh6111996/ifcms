package com.ifw.cms.service.impl;

import com.ifw.cms.entity.HrmEmployeeAttr;
import com.ifw.cms.repository.HrmEmployeeAttrRepository;
import com.ifw.cms.service.HrmEmployeeAttrService;
import com.ifw.cms.service.dto.HrmEmployeeAttrDTO;
import com.ifw.cms.service.mapper.HrmEmployeeAttrMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 */
@Service
@Transactional
public class HrmEmployeeAttrServiceImpl implements HrmEmployeeAttrService {

    private final Logger log = LoggerFactory.getLogger(HrmEmployeeAttrServiceImpl.class);

    private final HrmEmployeeAttrRepository hrmEmployeeAttrRepository;

    private final HrmEmployeeAttrMapper hrmEmployeeAttrMapper;

    public HrmEmployeeAttrServiceImpl(HrmEmployeeAttrRepository hrmEmployeeAttrRepository, HrmEmployeeAttrMapper hrmEmployeeAttrMapper) {
        this.hrmEmployeeAttrRepository = hrmEmployeeAttrRepository;
        this.hrmEmployeeAttrMapper = hrmEmployeeAttrMapper;
    }

    /**
     * Save a hrmEmployeeAttr.
     *
     * @param hrmEmployeeAttrDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public HrmEmployeeAttrDTO save(HrmEmployeeAttrDTO hrmEmployeeAttrDTO) {
        log.debug("Request to save HrmEmployeeAttr : {}", hrmEmployeeAttrDTO);
        HrmEmployeeAttr hrmEmployeeAttr = hrmEmployeeAttrMapper.toEntity(hrmEmployeeAttrDTO);
        hrmEmployeeAttr = hrmEmployeeAttrRepository.save(hrmEmployeeAttr);
        return hrmEmployeeAttrMapper.toDto(hrmEmployeeAttr);
    }

    /**
     * Get all the hrmEmployeeAttrs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<HrmEmployeeAttrDTO> findAll(Pageable pageable) {
        log.debug("Request to get all HrmEmployeeAttrs");
        return hrmEmployeeAttrRepository.findAll(pageable)
            .map(hrmEmployeeAttrMapper::toDto);
    }

    /**
     * Get all the hrmEmployeeAttrs with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<HrmEmployeeAttrDTO> findAllWithEagerRelationships(Pageable pageable) {
        return hrmEmployeeAttrRepository.findAllWithEagerRelationships(pageable).map(hrmEmployeeAttrMapper::toDto);
    }


    /**
     * Get one hrmEmployeeAttr by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<HrmEmployeeAttrDTO> findOne(Long id) {
        log.debug("Request to get HrmEmployeeAttr : {}", id);
        return hrmEmployeeAttrRepository.findOneWithEagerRelationships(id)
            .map(hrmEmployeeAttrMapper::toDto);
    }

    /**
     * Delete the hrmEmployeeAttr by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete HrmEmployeeAttr : {}", id);
        hrmEmployeeAttrRepository.deleteById(id);
    }

    @Override
    public List<HrmEmployeeAttrDTO> findByEmployeeId(Long id) {
        return hrmEmployeeAttrMapper.mapToListDTO(hrmEmployeeAttrRepository.findByEmployeeId(id));
    }
}
