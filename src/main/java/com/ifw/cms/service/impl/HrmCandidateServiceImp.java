package com.ifw.cms.service.impl;

import com.ifw.cms.entity.HrmCandidate;
import com.ifw.cms.repository.HrmCandidateRepository;
import com.ifw.cms.service.HrmCandidateService;
import com.ifw.cms.service.dto.HrmCandidateDTO;
import com.ifw.cms.service.mapper.HrmCandidateMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class HrmCandidateServiceImp implements HrmCandidateService {

    private final Logger log = LoggerFactory.getLogger(HrmCandidateServiceImp.class);


    @Autowired
    HrmCandidateRepository hrmCandidateRepository;

    @Autowired
    HrmCandidateMapper hrmCandidateMapper;

    @Override
    public HrmCandidateDTO save(HrmCandidateDTO hrmCandidateDTO) {
        log.info("Save candidate " + hrmCandidateDTO.toString());
        HrmCandidate hrmCandidate = hrmCandidateMapper.toEntity(hrmCandidateDTO);
        hrmCandidateRepository.save(hrmCandidate);
        return hrmCandidateDTO;
    }

    @Override
    public Page<HrmCandidateDTO> findAll(Pageable pageable) {
        log.debug("Request to get all HrmEmployees");
        return hrmCandidateRepository.findAll(pageable).map(hrmCandidateMapper::toDto);
    }

    @Override
    public Page<HrmCandidateDTO> search(Pageable pageable, String code, String fullname, String email) {
        return hrmCandidateRepository.search(pageable,code, fullname, email).map(hrmCandidateMapper::toDto);
    }

    @Override
    public HrmCandidateDTO findById(Long id) {
        log.info("Find candidate id = " + id);
        return hrmCandidateMapper.toDto(hrmCandidateRepository.findById(id).get());
    }

    @Override
    public void deleteById(Long id) {
        log.info("Delete candidate id = " + id);
        hrmCandidateRepository.deleteById(id);
    }

    @Override
    public List<Integer> checkFieldExists(String code, String phone, String email) {
        List<HrmCandidate> hrmCandidateList = hrmCandidateRepository.findAllByCodeOrPhoneOrEmail(code, phone, email);
        List<Integer> result = new ArrayList<>();
        for (HrmCandidate hrmCandidate : hrmCandidateList) {
            if (hrmCandidate.getCode().equals(code)){
                result.add(new Integer(1));
            }
            if (hrmCandidate.getPhone().equals(phone)){
                result.add(new Integer(2));
            }
            if (hrmCandidate.getEmail().equals(email)) {
                result.add(new Integer(3));
            }
        }
        return result;
    }
    /**
     * NinhTrangAn
     //     */
    @Override
    @Transactional(readOnly = true)
    public Optional<HrmCandidate> findByCode(String code) {
        log.debug("Request to get all Candidate by code {}");
        return hrmCandidateRepository.findByCode(code);
    }

}
