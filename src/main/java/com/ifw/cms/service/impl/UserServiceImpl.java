package com.ifw.cms.service.impl;

import com.ifw.cms.entity.AppUser;
import com.ifw.cms.repository.AppUserRepository;
import com.ifw.cms.service.UserService;
import com.ifw.cms.service.dto.UserVM;
import com.ifw.cms.service.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * Created by ITSOL SWIB Dev Team.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private UserMapper userMapper;

    @Override
    public Page<UserVM> findAllUsers(Pageable pageable, String key) {
        return appUserRepository.findPaginatedUsers(pageable, key).map(userMapper::toVM);
    }

    @Override
    public UserVM save(UserVM user) {
        AppUser entity = userMapper.toEntity(user);

        return userMapper.toVM(appUserRepository.save(entity));
    }
}
