package com.ifw.cms.service.impl;

import com.ifw.cms.entity.AppParamsExtra;
import com.ifw.cms.repository.AppParamsExtraRepository;
import com.ifw.cms.service.AppParamsExtraService;
import com.ifw.cms.service.dto.AppParamsExtraDTO;
import com.ifw.cms.service.mapper.AppParamsExtraMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 */
@Service
@Transactional
public class AppParamsExtraServiceImpl implements AppParamsExtraService {

    private final Logger log = LoggerFactory.getLogger(AppParamsExtraServiceImpl.class);

    private final AppParamsExtraRepository appParamsExtraRepository;

    private final AppParamsExtraMapper appParamsExtraMapper;

    public AppParamsExtraServiceImpl(AppParamsExtraRepository appParamsExtraRepository, AppParamsExtraMapper appParamsExtraMapper) {
        this.appParamsExtraRepository = appParamsExtraRepository;
        this.appParamsExtraMapper = appParamsExtraMapper;
    }

    /**
     * Save a appParamsExtra.
     *
     * @param appParamsExtraDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AppParamsExtraDTO save(AppParamsExtraDTO appParamsExtraDTO) {
        log.debug("Request to save AppParamsExtra : {}", appParamsExtraDTO);
        AppParamsExtra appParamsExtra = appParamsExtraMapper.toEntity(appParamsExtraDTO);
        appParamsExtra = appParamsExtraRepository.save(appParamsExtra);
        return appParamsExtraMapper.toDto(appParamsExtra);
    }

    /**
     * Get all the appParamsExtras.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AppParamsExtraDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AppParamsExtras");
        return appParamsExtraRepository.findAll(pageable)
            .map(appParamsExtraMapper::toDto);
    }


    /**
     * Get one appParamsExtra by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AppParamsExtraDTO> findOne(Long id) {
        log.debug("Request to get AppParamsExtra : {}", id);
        return appParamsExtraRepository.findById(id)
            .map(appParamsExtraMapper::toDto);
    }

    /**
     * Delete the appParamsExtra by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AppParamsExtra : {}", id);
        appParamsExtraRepository.deleteById(id);
    }
}
