package com.ifw.cms.service.impl;

import com.ifw.cms.entity.Province;
import com.ifw.cms.repository.ProvinceRepository;
import com.ifw.cms.service.ProvinceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProvinceServiceImpl implements ProvinceService {

    @Autowired
    private ProvinceRepository provinceRepository;
    @Override
    public Province save(Province province) {
        return provinceRepository.save(province);
    }

    @Override
    public void delete(Long id) {
        provinceRepository.deleteById(id);
    }

    @Override
    public Optional<Province> findById(Long id) {
        return provinceRepository.findById(id);
    }

    @Override
    public List<Integer> checkFieldExists(Long id, String name) {
        List<Province> province = provinceRepository.findAllByIdOrName(id,name);
        List<Integer> result = new ArrayList<>();
        for (Province pro : province)
        {
            if(pro.getId().equals(id))
            {
                result.add(1);
            }
            if (pro.getName().equals(name))
            {
                result.add(2);
            }
        }
        return result;
    }
}
