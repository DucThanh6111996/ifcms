package com.ifw.cms.service.impl;

import com.ifw.cms.entity.Contract;
import com.ifw.cms.repository.ContractRepository;
import com.ifw.cms.service.ContractService;
import com.ifw.cms.service.dto.ContractDTO;
import com.ifw.cms.service.mapper.ContractMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContractServiceImpl implements ContractService {
    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    private ContractMapper contractMapper;

    @Override
    public ContractDTO save(ContractDTO contractDTO) {
        Contract contract = contractMapper.toEntity(contractDTO);
        contractRepository.save(contract);
        return contractDTO;
    }

    @Override
    public void delete(Long id) {
        contractRepository.deleteById(id);

    }

    @Override
    public Page<ContractDTO> findAll(Pageable pageable) {
        return contractRepository.findAll(pageable).map(contractMapper::toDto);

    }

    @Override
    public ContractDTO findById(Long id) {
        return contractMapper.toDto(contractRepository.findById(id).get());

    }

    @Override
    public Optional<Contract> findByIdEntity(Long id)

    {
        return contractRepository.findById(id);
    }

    @Override
    public Page<ContractDTO> search(Pageable pageable, String code, String name) {
        return contractRepository.search(pageable, code, name).map(contractMapper::toDto);
    }

    @Override
    public List<Integer> checkFieldExists(String code) {
        List<Contract> contractList = contractRepository.findAllByCode(code);
        List<Integer> result = new ArrayList<>();
        for (Contract contract : contractList)
        {
            if(contract.getCode().equals(code))
            {
                result.add(new Integer(1));
            }
        }
        return result;
    }
}
