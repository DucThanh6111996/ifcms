package com.ifw.cms.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.ifw.cms.dao.AppRoleDAO;
import com.ifw.cms.dao.AppUserDAO;
import com.ifw.cms.entity.AppUser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

  @Autowired
  private AppUserDAO appUserDAO;

  @Autowired
  private AppRoleDAO appRoleDAO;

  @Override
  public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
    AppUser appUser = this.appUserDAO.findUserAccount(userName);

    if (appUser == null) {
      Logger.getLogger("User not found! " + userName);
      throw new UsernameNotFoundException("User " + userName + " was not found in the database");
    }

    // [ROLE_USER, ROLE_ADMIN,..]
    List<String> roleNames = this.appRoleDAO.getRoleNames(appUser.getUserId());

    List<GrantedAuthority> grantList = new ArrayList<>();
    if (roleNames != null) {
      for (String role : roleNames) {
        GrantedAuthority authority = new SimpleGrantedAuthority(role);
        grantList.add(authority);
      }
    }

    UserDetails userDetails = new User(appUser.getUserName(),
        appUser.getEncrytedPassword(), grantList);

    List<String> menu = new ArrayList<>();
    menu.add("dashboard");
    menu.add("profile");

    return userDetails;
  }

}
