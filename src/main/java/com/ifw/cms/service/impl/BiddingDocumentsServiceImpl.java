package com.ifw.cms.service.impl;

import com.ifw.cms.entity.BiddingDocuments;
import com.ifw.cms.repository.BiddingDocumentsRepository;
import com.ifw.cms.service.BiddingDocumentsService;
import com.ifw.cms.service.dto.BiddingDocumentsDTO;
import com.ifw.cms.service.mapper.BiddingDocumentsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BiddingDocumentsServiceImpl implements BiddingDocumentsService {
    @Autowired
    private BiddingDocumentsRepository biddingDocumentsRepository;

    @Autowired
    private BiddingDocumentsMapper biddingDocumentsMapper;

    @Override
    public BiddingDocumentsDTO save(BiddingDocumentsDTO biddingDocumentsDTO) {
        BiddingDocuments biddingDocuments = biddingDocumentsMapper.toEntity(biddingDocumentsDTO);
        biddingDocumentsRepository.save(biddingDocuments);
        return biddingDocumentsDTO;
    }

    @Override
    public void delete(Long id) {
        biddingDocumentsRepository.deleteById(id);
    }

    @Override
    public Page<BiddingDocumentsDTO> findAll(Pageable pageable) {
        return biddingDocumentsRepository.findAll(pageable).map(biddingDocumentsMapper::toDto);
    }

    @Override
    public BiddingDocumentsDTO findById(Long id) {
        return biddingDocumentsMapper.toDto(biddingDocumentsRepository.findById(id).get());
    }

    @Override
    public Page<BiddingDocumentsDTO> search(Pageable pageable, String code, String name) {
        return biddingDocumentsRepository.search(pageable, code, name).map(biddingDocumentsMapper::toDto);

    }

    @Override
    public List<BiddingDocumentsDTO> findByStatus() {
        return biddingDocumentsMapper.toDto(biddingDocumentsRepository.findByStatus());
    }

    @Override
    public List<Integer> checkFieldExists(String code) {
        List<BiddingDocuments> biddingDocumentsList = biddingDocumentsRepository.findAllByCode(code);
        List<Integer> result = new ArrayList<>();
        for (BiddingDocuments biddingDocuments : biddingDocumentsList )
        {
            if(biddingDocuments.getCode().equals(code))
            {
                result.add(1);
            }
        }
        return result;
    }
}
