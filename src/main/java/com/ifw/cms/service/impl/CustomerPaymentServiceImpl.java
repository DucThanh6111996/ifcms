package com.ifw.cms.service.impl;

import com.ifw.cms.entity.CustomerPayment;
import com.ifw.cms.repository.CustomerPaymentRepository;
import com.ifw.cms.service.CustomerPaymentService;
import com.ifw.cms.service.dto.CustomerPaymentDTO;
import com.ifw.cms.service.mapper.CustomerPaymentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Ninh Trang An 28/10/2019
 */
@Service
public class CustomerPaymentServiceImpl implements CustomerPaymentService {

    @Autowired
    private CustomerPaymentRepository customerPaymentRepository;

    @Autowired
    private CustomerPaymentMapper customerPaymentMapper;


    @Override
    public void delete(Long id) {
        customerPaymentRepository.deleteById(id);
    }

    @Override
    public CustomerPaymentDTO save(CustomerPaymentDTO customerPaymentDTO)
    {
        CustomerPayment customerPayment = customerPaymentMapper.toEntity(customerPaymentDTO);
        customerPaymentRepository.save(customerPayment);
        return customerPaymentDTO;
    }

    @Override
    public Page<CustomerPaymentDTO> findAll(Pageable pageable) {
        return customerPaymentRepository.findAll(pageable).map(customerPaymentMapper::toDto);
    }

    @Override
    public CustomerPaymentDTO findById(Long id) {
        return customerPaymentMapper.toDto(customerPaymentRepository.findById(id).get());
    }

    @Override
    public Page<CustomerPaymentDTO> search(Pageable pageable, String code, String name) {
        return customerPaymentRepository.search(pageable, code, name).map(customerPaymentMapper::toDto);
    }

    @Override
    public List<Integer> checkFieldExists(String code) {

        List<CustomerPayment> paymentList = customerPaymentRepository.findAllByCode(code);
        List<Integer> result = new ArrayList<>();
        for (CustomerPayment customerPayment :  paymentList )
        {
            if(customerPayment.getCode().equals(code))
            {
                result.add(new Integer(1));
            }
        }
        return result;

    }
}
