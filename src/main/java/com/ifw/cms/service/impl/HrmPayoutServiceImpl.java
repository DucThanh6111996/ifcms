package com.ifw.cms.service.impl;

import com.ifw.cms.entity.HrmPayout;
import com.ifw.cms.entity.enumeration.PaymentStatus;
import com.ifw.cms.repository.HrmPayoutRepository;
import com.ifw.cms.service.HrmPayoutService;
import com.ifw.cms.service.dto.HrmPayoutDTO;
import com.ifw.cms.service.mapper.HrmPayoutMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 */
@Service
@Transactional
public class HrmPayoutServiceImpl implements HrmPayoutService {

    private final Logger log = LoggerFactory.getLogger(HrmPayoutServiceImpl.class);

    private final HrmPayoutRepository hrmPayoutRepository;

    private final HrmPayoutMapper hrmPayoutMapper;

    public HrmPayoutServiceImpl(HrmPayoutRepository hrmPayoutRepository, HrmPayoutMapper hrmPayoutMapper) {
        this.hrmPayoutRepository = hrmPayoutRepository;
        this.hrmPayoutMapper = hrmPayoutMapper;
    }

    /**
     * Save a hrmPayout.
     *
     * @param hrmPayoutDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public HrmPayoutDTO save(HrmPayoutDTO hrmPayoutDTO) {
        log.debug("Request to save HrmPayout : {}", hrmPayoutDTO);
        HrmPayout hrmPayout = hrmPayoutMapper.toEntity(hrmPayoutDTO);
        hrmPayout = hrmPayoutRepository.save(hrmPayout);
        return hrmPayoutMapper.toDto(hrmPayout);
    }

    /**
     * Get all the hrmPayouts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<HrmPayoutDTO> findAll(Pageable pageable) {
        log.debug("Request to get all HrmPayouts");
        return hrmPayoutRepository.findAll(pageable)
            .map(hrmPayoutMapper::toDto);
    }


    /**
     * Get all the hrmPayouts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<HrmPayoutDTO> findByStatus(Pageable pageable, PaymentStatus status) {
        log.debug("Request to get all HrmPayouts");
        return hrmPayoutRepository.findByStatus(pageable, status)
                .map(hrmPayoutMapper::toDto);
    }


    /**
     * Get one hrmPayout by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<HrmPayoutDTO> findOne(Long id) {
        log.debug("Request to get HrmPayout : {}", id);
        return hrmPayoutRepository.findById(id)
            .map(hrmPayoutMapper::toDto);
    }

    /**
     * Delete the hrmPayout by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete HrmPayout : {}", id);
        hrmPayoutRepository.deleteById(id);
    }
}
