package com.ifw.cms.service.impl;

import com.ifw.cms.entity.AppParams;
import com.ifw.cms.repository.AppParamsRepository;
import com.ifw.cms.service.AppParamsService;
import com.ifw.cms.service.dto.AppParamsDTO;
import com.ifw.cms.service.mapper.AppParamsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 */
@Service
@Transactional
public class AppParamsServiceImpl implements AppParamsService {

    private final Logger log = LoggerFactory.getLogger(AppParamsServiceImpl.class);

    private final AppParamsRepository appParamsRepository;

    private final AppParamsMapper appParamsMapper;

    @Autowired
    public AppParamsServiceImpl(AppParamsRepository appParamsRepository, AppParamsMapper appParamsMapper) {
        this.appParamsRepository = appParamsRepository;
        this.appParamsMapper = appParamsMapper;
    }

    /**
     * Save a appParams.
     *
     * @param appParamsDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public AppParamsDTO save(AppParamsDTO appParamsDTO) {
        log.debug("Request to save AppParams : {}", appParamsDTO);
        AppParams appParams = appParamsMapper.toEntity(appParamsDTO);
        appParams = appParamsRepository.save(appParams);
        return appParamsMapper.toDto(appParams);
    }

    /**
     * Get all the appParams.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AppParamsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AppParams");
        return appParamsRepository.findAll(pageable)
            .map(appParamsMapper::toDto);
    }


    /**
     * Get one appParams by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<AppParamsDTO> findOne(Long id) {
        log.debug("Request to get AppParams : {}", id);
        return appParamsRepository.findById(id)
            .map(appParamsMapper::toDto);
    }

    /**
     * Delete the appParams by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete AppParams : {}", id);
        appParamsRepository.deleteById(id);
    }

    /**
     * Get all the appParams.
     *
     * @param groupCode group code.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<AppParamsDTO> findByGroupCode(String groupCode) {
        log.debug("Request to get all AppParams by group code {}", groupCode);
        return appParamsMapper.mapToDTO(appParamsRepository.findByGroupCode(groupCode));
    }

    /**
     * Get all the appParams.
     *
     * @param groupCode group code.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<AppParamsDTO> findByGroupCodePaged(String groupCode, String key, Pageable pageable) {
        log.debug("Request to get all AppParams by group code {}", groupCode);
        return appParamsRepository.findByGroupCodePaged(groupCode, key, pageable).map(appParamsMapper::toDto);
    }

    @Override
    public List<Integer> checkFieldExists(String code, String name) {
        List<AppParams> appParamsList = appParamsRepository.findAllByCodeOrName(code, name);
        List<Integer> result = new ArrayList<>();
        for (AppParams appParams : appParamsList)
        {
            if(appParams.getCode().equals(code))
            {
                result.add(new Integer(1));
            }
            if(appParams.getName().equals(name))
            {
                result.add(new Integer(2));
            }
        }
        return result;
    }

    /**
     * NinhTrangAn
//     */
    @Override
    @Transactional(readOnly = true)
    public AppParamsDTO  findById(Long id) {
        log.debug("Request to get all AppParams by group code {}");
        return  appParamsMapper.toDto(appParamsRepository.findById(id).get());
    }


}
