package com.ifw.cms.service;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;
import java.util.List;

/**
 * Created by ITSOL SWIB Dev Team.
 */
public class CmsUserDetails extends User {
    private List<String> userMenus;

    public CmsUserDetails(String username, String password,
                          Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public List<String> getUserMenus() {
        return userMenus;
    }

    public void setUserMenus(List<String> userMenus) {
        this.userMenus = userMenus;
    }
}
