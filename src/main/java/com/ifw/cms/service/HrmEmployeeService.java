package com.ifw.cms.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.ifw.cms.service.dto.*;

import java.util.List;
import java.util.Optional;

/**
 */
public interface HrmEmployeeService {

    /**
     * Save a hrmEmployee.
     *
     * @param hrmEmployeeDTO the entity to save.
     * @return the persisted entity.
     */
    HrmEmployeeDTO save(HrmEmployeeDTO hrmEmployeeDTO);

    /**
     * Get all the hrmEmployees.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<HrmEmployeeDTO> findAll(Pageable pageable);

    /**
     * Get all the hrmEmployees with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<HrmEmployeeDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" hrmEmployee.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HrmEmployeeDTO> findOne(Long id);

    /**
     * Delete the "id" hrmEmployee.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    Long importEmployee(HrmEmployeeDTO hrmEmployeeDTO);

    Page<HrmEmployeeDTO> search(Pageable pageable, String fullname, String email, String status);

    boolean checkEqualFieldHrmEmployee(HrmEmployeeDTO hrmEmployeeDTO);

    /**
     * ThanhDT
     */

    List<EmployeeStatisticDto> statisticByUnit(); // Thống kế theo đơn vị

    List<EmployeeRoleDTO> statisticByRole(); // Thống kế theo vai trò

    List<ExpEmployeeDTO> statisticByExpEmployee(); // Thống kế nhân viên sắp hết hạn

    List<InsourceEmployeeDTO> statisticByInsourceEmployee(); // thống kê nhân viên làm insource

    Page<HrmEmployeeDTO> getEmployeeContactExpired(Pageable pageable); // báo cáo nhân viên hết hạn hợp đồng

    List<HrmEmployeeDTO> getEmployeeExpired(); // Lấy danh sách nhân viên sắp hết hạn hợp đồng

    List<HrmEmployeeDTO> getAdmin(); // Lấy danh sách quản trị viên
}