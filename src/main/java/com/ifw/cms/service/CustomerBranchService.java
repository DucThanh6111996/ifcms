package com.ifw.cms.service;

import com.ifw.cms.entity.CustomerBranch;
import com.ifw.cms.service.dto.CustomerBranchDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CustomerBranchService {
    List<CustomerBranch> findByCustomerId(Long id);
    CustomerBranchDTO findById(Long id);
    Page<CustomerBranchDTO> findAll(Pageable pageable);

}
