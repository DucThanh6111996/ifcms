package com.ifw.cms.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.ifw.cms.service.dto.*;
import java.util.Optional;

/**
 */
public interface HrmEmployeeDocsService {

    /**
     * Save a hrmEmployeeDocs.
     *
     * @param hrmEmployeeDocsDTO the entity to save.
     * @return the persisted entity.
     */
    HrmEmployeeDocsDTO save(HrmEmployeeDocsDTO hrmEmployeeDocsDTO);

    /**
     * Get all the hrmEmployeeDocs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<HrmEmployeeDocsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" hrmEmployeeDocs.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HrmEmployeeDocsDTO> findOne(Long id);

    /**
     * Delete the "id" hrmEmployeeDocs.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
