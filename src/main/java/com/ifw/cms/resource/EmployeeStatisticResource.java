package com.ifw.cms.resource;

import com.ifw.cms.resource.service.ImportEmployeeFileService;
import com.ifw.cms.service.HrmEmployeeService;
import com.ifw.cms.service.dto.EmployeeRoleDTO;
import com.ifw.cms.service.dto.EmployeeStatisticDto;
import com.ifw.cms.service.dto.ExpEmployeeDTO;
import com.ifw.cms.service.dto.InsourceEmployeeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api")
public class EmployeeStatisticResource {
    @Autowired
    private HrmEmployeeService hrmEmployeeService;

    @Autowired
    private ImportEmployeeFileService importEmployeeFileService;

    @GetMapping("/report/employee-by-unit")
    public List<EmployeeStatisticDto> reportEmployeeByUnit() {
        return hrmEmployeeService.statisticByUnit();
    }

    @GetMapping("/report/employee-by-role")
    public List<EmployeeRoleDTO> reportEmployeeByRole() {
        return hrmEmployeeService.statisticByRole();
    }

    @GetMapping("/report/insource-employee")
    public List<InsourceEmployeeDTO> reportInsourceEmployee(){
        return hrmEmployeeService.statisticByInsourceEmployee();
    }

    @GetMapping("/report/exp-employee")
    public List<ExpEmployeeDTO> reportExpEmployee(){
        return hrmEmployeeService.statisticByExpEmployee();
    }

}
