package com.ifw.cms.resource;

import com.ifw.cms.resource.service.ExportEmployeeService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;

@RestController
@RequestMapping("/resource")
public class ExportEmployeeResource {
    @Autowired
    private ExportEmployeeService exportEmployeeService;

    @PostMapping(value = "/exportEmployeeFile", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody
    ResponseEntity<FileSystemResource> exportFile() {
        String path = exportEmployeeService.exportEmployeeService();
        if (path != null) {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            File file = FileUtils.getFile(path);
            FileSystemResource fileSystemResource = new FileSystemResource(file);
            headers.set("File", file.getName());
            headers.set("Content-Disposition", "attachment; filename=" + file.getName());
            headers.set("Access-Control-Expose-Headers", "File");
            return new ResponseEntity<>(fileSystemResource, headers, HttpStatus.OK);
        }
        return null;
    }
}
