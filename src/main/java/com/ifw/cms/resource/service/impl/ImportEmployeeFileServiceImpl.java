package com.ifw.cms.resource.service.impl;

import com.ifw.cms.entity.HrmEmployee;
import com.ifw.cms.repository.HrmEmployeeRepository;
import com.ifw.cms.resource.service.ImportEmployeeFileService;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ImportEmployeeFileServiceImpl implements ImportEmployeeFileService {
    @Autowired
    HrmEmployeeRepository hrmEmployeeRepository;

    @Override
    public void importFileDataToEmployeeTable(MultipartFile fileData) {
        if (fileData == null) return;
        readExcel(fileData);
    }

    private void readExcel(MultipartFile fileData) {
        try {
            Workbook workbook = new XSSFWorkbook(fileData.getInputStream());
            Sheet sheet = workbook.getSheetAt(0);
            Row row;
            DataFormatter format = new DataFormatter();

            List<HrmEmployee> hrmEmployees = new ArrayList<>();
            for (int i = 6; i <= sheet.getLastRowNum(); i++) {
                row = sheet.getRow(i);

                HrmEmployee hrmEmployee = new HrmEmployee();
                hrmEmployee.setCode(format.formatCellValue(row.getCell(3)));
                hrmEmployee.setFullname(format.formatCellValue(row.getCell(4)));
                hrmEmployee.setPhone(format.formatCellValue(row.getCell(5)));
                hrmEmployee.setEmail(format.formatCellValue(row.getCell(6)));
                hrmEmployee.setGender(format.formatCellValue(row.getCell(7)));
                hrmEmployee.setStatus(format.formatCellValue(row.getCell(8)));
                hrmEmployees.add(hrmEmployee);
            }
            hrmEmployeeRepository.saveAll(hrmEmployees);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
