package com.ifw.cms.resource.service;

import org.springframework.web.multipart.MultipartFile;

public interface ImportEmployeeFileService {
    void importFileDataToEmployeeTable(MultipartFile fileData);
}
