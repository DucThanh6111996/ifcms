package com.ifw.cms.resource.service.impl;

import com.ifw.cms.resource.dao.ExportEmployeeDAO;
import com.ifw.cms.resource.dto.ExportEmployeeDTO;
import com.ifw.cms.resource.service.ExportEmployeeService;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ExportEmployeeServiceImpl implements ExportEmployeeService {
    private final ExportEmployeeDAO exportEmployeeDAO;

    @Override
    public String exportEmployeeService() {
        String path = "./report/"; /* Folder chứa đường dẫn lưu file excel */
        File dir = new File(path);

        if (!dir.exists()) {
            dir.mkdirs();
        }

        path = path + "export_file" + ".xlsx";
        InputStream excelFile;
        XSSFWorkbook workbook = null;
        try {
            excelFile = new ClassPathResource("/template-export/export-employee.xlsx").getInputStream();
            workbook = new XSSFWorkbook(excelFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
// fill data to file
        int index = 1;
        int rowNum = 3;
        XSSFSheet sheetData = workbook.getSheetAt(0);
        XSSFCellStyle textCellStyle = this.getTextCellStyle(workbook);
        XSSFCellStyle dateCellStyle = this.getDateCellStyle(workbook);

        // get list data
        List<ExportEmployeeDTO> listData = exportEmployeeDAO.getAllEmployee();
        for (ExportEmployeeDTO exportEmployeeDTO : listData) {
            int i = 0;
            Row row = sheetData.createRow(rowNum++);

            // STT
            createCell(row, i++, "" + index++, dateCellStyle);
            // UserName
            createCell(row, i++, exportEmployeeDTO.getCode() == null ? "" : exportEmployeeDTO.getCode(), textCellStyle);

            createCell(row, i++, exportEmployeeDTO.getFullName() == null ? "" : exportEmployeeDTO.getFullName(), textCellStyle);
            createCell(row, i++, exportEmployeeDTO.getEmail() == null ? "" : exportEmployeeDTO.getEmail(), textCellStyle);
            createCell(row, i++, exportEmployeeDTO.getPhone() == null ? "" : exportEmployeeDTO.getPhone(), textCellStyle);
            createCell(row, i++, exportEmployeeDTO.getSlr_before() == null ? "" : exportEmployeeDTO.getSlr_before(), textCellStyle);
        }

        // write file
        try {
            FileOutputStream outputStream = new FileOutputStream(path);
            workbook.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            return "";
        }
        return path;
    }

    private static void createCell(Row row, Integer i, String value, CellStyle cellStyle) {
        Cell cell = row.createCell(i++);
        cell.setCellValue(value == null ? "" : value);
        cell.setCellStyle(cellStyle);
    }

    private static XSSFCellStyle getDateCellStyle(XSSFWorkbook workbook) {
        XSSFCellStyle cellStyle = workbook.createCellStyle();
        getBorderStyle(cellStyle);
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyle.setWrapText(true);
        cellStyle.setFont(getDataFontStyle(workbook));
        DataFormat fmt = workbook.createDataFormat();
        cellStyle.setDataFormat(fmt.getFormat("@"));
        return cellStyle;
    }

    private static XSSFCellStyle getTextCellStyle(XSSFWorkbook workbook) {
        XSSFCellStyle cellStyle = workbook.createCellStyle();
        getBorderStyle(cellStyle);
        cellStyle.setAlignment(HorizontalAlignment.LEFT);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        cellStyle.setWrapText(true);
        cellStyle.setFont(getDataFontStyle(workbook));
        DataFormat fmt = workbook.createDataFormat();
        cellStyle.setDataFormat(fmt.getFormat("@"));
        return cellStyle;
    }

    private static Font getDataFontStyle(Workbook workbook) {
        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        font.setFontName("Time New Romance");
        return font;
    }

    private static void getBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
    }

}
