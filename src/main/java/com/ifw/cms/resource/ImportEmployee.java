package com.ifw.cms.resource;

import com.ifw.cms.resource.service.ImportEmployeeFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value = "/import")
public class ImportEmployee {
    @Autowired
    private ImportEmployeeFileService importEmployeeFileService;

    @PostMapping(value = "/uploadEmployee")
    public String uploadOneFileHandlerPOST(HttpServletRequest request,
                                           @ModelAttribute("fileData") MultipartFile fileData) {
        importEmployeeFileService.importFileDataToEmployeeTable(fileData);
        return "admin/hrm/employee-list-subheader";
    }
}
