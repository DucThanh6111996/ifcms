package com.ifw.cms.resource.dao.impl;

import com.ifw.cms.resource.dao.ExportEmployeeDAO;
import com.ifw.cms.resource.dto.ExportEmployeeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ExportEmployeeDaoImpl implements ExportEmployeeDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<ExportEmployeeDTO> getAllEmployee() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(" SELECT id, code, fullName, phone, email, slr_before");
        stringBuilder.append(" FROM hrm_employee ");

        List<ExportEmployeeDTO> data = null;
        data = jdbcTemplate.query(stringBuilder.toString(), new ExportEmployeeMapper());
        return data;
    }

    class ExportEmployeeMapper implements RowMapper<ExportEmployeeDTO> {

        @Nullable
        @Override
        public ExportEmployeeDTO mapRow(ResultSet resultSet, int i) throws SQLException {
            ExportEmployeeDTO exportEmployeeDTO = new ExportEmployeeDTO();
            exportEmployeeDTO.setCode(resultSet.getString("code"));
            exportEmployeeDTO.setFullName(resultSet.getString("fullName"));
            exportEmployeeDTO.setEmail(resultSet.getString("email"));
            exportEmployeeDTO.setPhone(resultSet.getString("phone"));
            exportEmployeeDTO.setSlr_before(resultSet.getString("slr_before"));
            return exportEmployeeDTO;
        }
    }
}
