package com.ifw.cms.resource.dao;

import com.ifw.cms.resource.dto.ExportEmployeeDTO;

import java.util.List;

public interface ExportEmployeeDAO {
    List<ExportEmployeeDTO> getAllEmployee();
}
