package com.ifw.cms.resource.dto;

public class ExportEmployeeDTO {
    private String code;
    private String fullName;
    private String phone;
    private String email;
    private String slr_before;

    public ExportEmployeeDTO(String code, String fullName, String phone, String email, String slr_before) {
        this.code = code;
        this.fullName = fullName;
        this.phone = phone;
        this.email = email;
        this.slr_before = slr_before;
    }

    public ExportEmployeeDTO() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSlr_before() {
        return slr_before;
    }

    public void setSlr_before(String slr_before) {
        this.slr_before = slr_before;
    }
}
