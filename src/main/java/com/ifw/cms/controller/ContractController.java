package com.ifw.cms.controller;

import com.ifw.cms.entity.Contract;
import com.ifw.cms.service.BiddingDocumentsService;
import com.ifw.cms.service.ContractService;
import com.ifw.cms.service.dto.BiddingDocumentsDTO;
import com.ifw.cms.service.dto.ContractDTO;
import com.ifw.cms.service.dto.CustomerDTO;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/admin")
public class ContractController {

    private ContractService contractService;
    private BiddingDocumentsService biddingDocumentsService;

    public ContractController(ContractService contractService,
                              BiddingDocumentsService biddingDocumentsService) {
        this.contractService = contractService;
        this.biddingDocumentsService = biddingDocumentsService;
    }

    /**
     * Ninh Trang An
     * List all contract, filter by key
     */
    @RequestMapping(value = "/contract/list", method = RequestMethod.GET)
    public String listAllContract(Model model, Pageable pageable,
                                  @RequestParam("key") Optional<String> key) {
        Page<ContractDTO> page = contractService.search(pageable, key.orElse("")
                , key.orElse(""));
        model.addAttribute("page", page);
        return "admin/contract/contract-list";
    }

    /**
     * Ninh Trang An
     * Show create new contract screen
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/contract/new", method = RequestMethod.GET)
    public String getAddContractForm(Model model, String code, Pageable pageable) {
        Contract contract = new Contract();
        contract.setCode(code);
        model.addAttribute("contract", contract);
        getListBiddocByStatus(model);
        return "admin/contract/contract-add";
    }

    private void getListBiddocByStatus(Model model) {
        List<BiddingDocumentsDTO> list = biddingDocumentsService.findByStatus();
        model.addAttribute("listBiddoc", list);
    }

    /**
     * Ninh Trang An
     * Save contract
     *
     * @param contractDTO
     * @param result
     * @param model
     * @param pageable
     * @return
     */
    @PostMapping("/contract/save")
    public String addContract(@ModelAttribute("contract") @Valid final ContractDTO contractDTO,
                              final BindingResult result, Model model, Pageable pageable) {
        boolean error = false;

        if (result.hasErrors()) {
            model.addAttribute("contract", contractDTO);
            error = true;
        }

        for (Integer integer : contractService.checkFieldExists(
                contractDTO.getCode())) {
            if (integer.equals(Integer.parseInt("1"))) {
                model.addAttribute("errorCode", "Mã hợp đồng đã tồn tại");
                error = true;
            }
            model.addAttribute("contract", contractDTO);
        }
        if (error) {
            getListBiddocByStatus(model);
            model.addAttribute("contract", contractDTO);
            return "admin/contract/contract-add";
        }
        contractService.save(contractDTO);

        return "redirect:/admin/contract/list";
    }


    /**
     * Ninh Trang An
     * Get contract detail
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/contract/detail/{id}", method = RequestMethod.GET)
    public String getContractDetail(Model model, @PathVariable Long id) {

        ContractDTO contractDTO = contractService.findById(id);

        if (contractDTO != null) {
            model.addAttribute("contract", contractDTO);
        }

        BiddingDocumentsDTO biddingDocumentsDTO = biddingDocumentsService.findById(contractDTO.getBiddingDocumentsId());
        model.addAttribute("biddoc", biddingDocumentsDTO);
        return "admin/contract/contract-detail";
    }

    /**
     * NinhTrangAn
     * Show update contract screen
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/contract/update/{id}", method = RequestMethod.GET)
    public String getUpdateContractForm(Model model, @PathVariable("id") Long id) {

        ContractDTO contractDTO = contractService.findById(id);
        if (contractDTO != null) {
            getListBiddocByStatus(model);
            model.addAttribute("contract", contractDTO);
        }
        return "admin/contract/contract-update";
    }

    /**
     * Ninh Trang An
     *
     * @param
     * @param result
     * @param model
     * @return
     */
    @PostMapping("/contract/update")
    public String updateContract(@ModelAttribute("contract") @Valid final ContractDTO contractDTO,
                                 final BindingResult result, Model model) {
        boolean error = false;

        if (result.hasErrors()) {
            error = true;
        }

        ContractDTO contractDTO1 = contractService.findById(contractDTO.getId());
        for (Integer i : contractService.checkFieldExists(
                contractDTO.getCode())) {
            if (i.equals(1) && !contractDTO1.getCode().equals(contractDTO.getCode())) {
                model.addAttribute("errorCode", "Mã hợp đồng đã tồn tại");
                error = true;
            }
        }
        if (error) {
            getListBiddocByStatus(model);
            model.addAttribute("contract", contractDTO);
            return "admin/contract/contract-update";
        }

        contractService.save(contractDTO);
        return "redirect:/admin/contract/list";
    }

    /**
     * Ninh Trang An
     * Delete contract screen
     *//*
    @PostMapping("/contract/delete")
    public String deleteContract(@ModelAttribute("contract") Contract contract) {

        contractService.delete(contract.getId());
        return "redirect:/admin/contract/list";
    }

    public String importContract(Model model, MultipartFile file, @RequestParam(name = "url") String url,
                                 @RequestParam(required = false) String code) throws IOException {
        Workbook workbook = null;
        DataFormatter dataFormatter = new DataFormatter();
        int success = 0;
        int failed = 0;

        if (code == null) {
            try {
                workbook = WorkbookFactory.create(file.getInputStream());
                Sheet sheet = workbook.getSheetAt(0);
                Iterator<Row> rowIterator = sheet.rowIterator();

                if (rowIterator.hasNext()) {
                    rowIterator.next();
                }

                while (rowIterator.hasNext()) {
                    Row row = rowIterator.next();
                    CustomerDTO customerDTO = new CustomerDTO();
                    customerDTO.setCode(dataFormatter.formatCellValue(row.getCell(0)));
                    customerDTO.setName(dataFormatter.formatCellValue(row.getCell(1)));
                    customerDTO.setEmail(dataFormatter.formatCellValue(row.getCell(2)));
                }
            } catch (InvalidFormatException e) {
                e.printStackTrace();
            }
        }
    }*/
}
