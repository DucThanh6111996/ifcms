package com.ifw.cms.controller;

import com.ifw.cms.entity.*;
import com.ifw.cms.filestorage.FileStorage;
import com.ifw.cms.repository.*;
import com.ifw.cms.service.*;
import com.ifw.cms.service.dto.HrmCandidateDTO;
import com.ifw.cms.service.dto.SkillDTO;
import com.ifw.cms.service.mapper.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;

@Controller
@RequestMapping("/admin")
public class HrmController {

    private AppParamsService appParamsService;

    private AppParamsRepository appParamsRepository;

    private AppParamsMapper appParamsMapper;

    private ProvinceRepository provinceRepository;

    private DistrictRepository districtRepository;

    private WardRepository wardRepository;

    private HrmCandidateService hrmCandidateService;

    private HrmCandidateRepository hrmCandidateRepository;

    private SkillService skillService;

    private FileStorage fileStorage;

    private SkillMapper skillMapper;

    private HrmCandidateMapper hrmCandidateMapper;

    public HrmController(AppParamsService appParamsService,
                           AppParamsRepository appParamsRepository,
                           AppParamsMapper appParamsMapper,
                           ProvinceRepository provinceRepository,
                           DistrictRepository districtRepository,
                           WardRepository wardRepository,
                           HrmCandidateService hrmCandidateService,
                           HrmCandidateRepository hrmCandidateRepository,
                           SkillService skillService,
                           SkillMapper skillMapper,
                           HrmCandidateMapper hrmCandidateMapper,
                         FileStorage fileStorage) {
        this.appParamsService = appParamsService;
        this.appParamsRepository = appParamsRepository;
        this.appParamsMapper = appParamsMapper;
        this.provinceRepository = provinceRepository;
        this.districtRepository = districtRepository;
        this.wardRepository = wardRepository;
        this.hrmCandidateService = hrmCandidateService;
        this.hrmCandidateRepository = hrmCandidateRepository;
        this.skillService = skillService;
        this.skillMapper = skillMapper;
        this.hrmCandidateMapper = hrmCandidateMapper;
        this.fileStorage = fileStorage;
    }

    /**
     * Ninh Trang An
     * List all candidate, filter by key
     *

     */
    @RequestMapping(value = "/hrm/candidate/list", method = RequestMethod.GET)
    public String listAllCandidate(Model model, Pageable pageable,
                                   @RequestParam("key") Optional<String> key) {
        Page<HrmCandidateDTO> page = hrmCandidateService.search(pageable, key.orElse("")
                , key.orElse(""), key.orElse(""));
        model.addAttribute("page", page);

        return "admin/hrm/candidate-list";
    }
    /**
     * Show create new candidate screen
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/hrm/candidate/new", method = RequestMethod.GET)
    public String getAddCandidateForm(Model model,String code,Pageable pageable) {
        HrmCandidateDTO hrmCandidateDTO = new HrmCandidateDTO();
        hrmCandidateDTO.setCode(code);
        model.addAttribute("candidate", hrmCandidateDTO);
        bindDataForAddCandidateForm(model);
        List<SkillDTO> skillDTOS = getListSkill(PageRequest.of(0, Integer.MAX_VALUE), model);
        if (skillDTOS != null && skillDTOS.size() > 0) {
            model.addAttribute("skill", skillDTOS);
        }
        return "admin/hrm/candidate-add";
    }

    private void bindDataForAddCandidateForm(Model model) {
        // get province
        List provinces = provinceRepository.findAll();
        model.addAttribute("provinces", provinces);

        model.addAttribute("units", appParamsService.findByGroupCode("UNIT"));
        model.addAttribute("roles", appParamsService.findByGroupCode("EM_ROLE"));
        model.addAttribute("levels", appParamsService.findByGroupCode("EM_LEVEL"));
        model.addAttribute("branches", appParamsService.findByGroupCode("BRANCH"));
        model.addAttribute("posits", appParamsService.findByGroupCode("EM_POSIT"));
        model.addAttribute("contractTypes", appParamsService.findByGroupCode("EM_CNT_TYP"));
    }

    /**
     * save candidate
     * NinhTrangAn
     * /**
     *
     * @param model
     */
    @PostMapping("/hrm/candidate/save")
    public String addCandidate(@ModelAttribute("candidate") @Valid final HrmCandidateDTO hrmCandidateDTO,
                               final BindingResult result, Model model, HttpServletRequest request) {
        bindDataForAddCandidateForm(model);
        boolean error = false;

        if (result.hasErrors()) {
            model.addAttribute("candidate", hrmCandidateDTO);
            List<SkillDTO> skillDTOS = getListSkill(PageRequest.of(0, Integer.MAX_VALUE), model);
            if (skillDTOS != null && skillDTOS.size() > 0) {
                model.addAttribute("skill", skillDTOS);
            }
            error = true;
        }

        for (Integer integer : hrmCandidateService.checkFieldExists(hrmCandidateDTO.getCode(), hrmCandidateDTO.getPhone(), hrmCandidateDTO.getEmail())) {
            if (integer.equals(Integer.parseInt("1"))) {
                model.addAttribute("errorCode", "Mã ứng viên đã tồn tại");
                error = true;
            }
            if (integer.equals(Integer.parseInt("2"))) {
                model.addAttribute("errorPhone", "Số điện thoại đã tồn tại");
                error = true;
            }
            if (integer.equals(Integer.parseInt("3"))) {
                model.addAttribute("errorEmail", "Email đã tồn tại");
                error = true;
            }
            model.addAttribute("candidate", hrmCandidateDTO);
        }
        if(error) {
            bindDataForAddCandidateForm(model);
            model.addAttribute("candidate", hrmCandidateDTO);
            displayDistrictsAndTowns(hrmCandidateDTO,model);
            displaySkill(hrmCandidateDTO,model);
            return "admin/hrm/candidate-add";
        }
        hrmCandidateService.save(hrmCandidateDTO);
        //handle file uploads
//        if (hrmCandidateDTO.getImage().length>0) {
//            for (MultipartFile fileData : hrmCandidateDTO.getImage()) {
//                if (StringUtils.isNotEmpty(fileData.getOriginalFilename())) {
//                    String image = "avatar-" + hrmCandidateDTO.getPhone()
//                            + "."
//                            + fileData.getOriginalFilename().substring(fileData.getOriginalFilename().lastIndexOf(".") + 1);
////                    hrmCandidateDTO.setI
//                    fileStorage.store(fileData, image);
//                }
//            }
//        }
        return "redirect:/admin/hrm/candidate/list";
    }

    private List<SkillDTO> getListSkill(Pageable page, Model model) {
        Page<SkillDTO> pageSkill = skillService.findAll(page);
        List<SkillDTO> skillDTOS = pageSkill.getContent();
        return skillDTOS;
    }
    /**
     * Get candidate detail
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/hrm/candidate/detail/{id}", method = RequestMethod.GET)
    public String getCandidateDetail(Model model, @PathVariable Long id) {

        bindDataForAddCandidateForm(model);
        HrmCandidateDTO hrmCandidateDTO = hrmCandidateService.findById(id);
//        Optional<HrmCandidateDTO> hrm = hrmCandidateRepository.findById(id).map(hrmCandidateMapper::toDto);

        if (hrmCandidateDTO != null) {
            Optional<Province> province = provinceRepository.findById(hrmCandidateDTO.getProvinceId());
            if(province.isPresent())
            {
                Optional<District> district = districtRepository.findById(hrmCandidateDTO.getDistrictId());
                if(district.isPresent()){
                    Optional<Ward> ward = wardRepository.findById(hrmCandidateDTO.getWardId());
                    if(ward.isPresent()){
                        model.addAttribute("ward",ward.get().getName());
                    }
                    model.addAttribute("district",district.get().getName());
                }
                model.addAttribute("province",province.get().getName());
            }
            model.addAttribute("candidate", hrmCandidateDTO);
        }
        return "admin/hrm/candidate-detail";
    }

    /**
     * NinhTrangAn
     * Show update candidate screen
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/hrm/candidate/update/{id}", method = RequestMethod.GET)
    public String getUpdateCandidateForm(Model model, @PathVariable("id") Long id) {

        HrmCandidateDTO hrmCandidateDTO = hrmCandidateService.findById(id);
        bindDataForAddCandidateForm(model);
        if (hrmCandidateDTO != null) {
            model.addAttribute("candidate", hrmCandidateDTO);
            displayDistrictsAndTowns(hrmCandidateDTO,model);
            displaySkill(hrmCandidateDTO,model);
        }
        return "admin/hrm/candidate-update";
    }

    /**
     * NinhTrangAn
     * Do update candidate
     *
     * @param hrmCandidateDTO
     * @param result
     * @param model
     * @return
     */
    @PostMapping("/hrm/candidate/update")
    public String updateCandidate(
            @ModelAttribute("candidate") @Valid final HrmCandidateDTO hrmCandidateDTO, final BindingResult result,
            Model model) {
        boolean error = false;

        if(result.hasErrors()){
            error = true;
        }

        HrmCandidateDTO hrmCandidateDTOPrototype = hrmCandidateService.findById(hrmCandidateDTO.getId());

        for (Integer i : hrmCandidateService.checkFieldExists(
                hrmCandidateDTO.getCode(),
                hrmCandidateDTO.getPhone(),
                hrmCandidateDTO.getEmail())) {
            if (i.equals(1) && !hrmCandidateDTOPrototype.getCode().equals(hrmCandidateDTO.getCode())) {
                model.addAttribute("errCode", "Mã ứng viên đã tồn tại");
                error = true;
            }
            if (i.equals(2) && !hrmCandidateDTOPrototype.getPhone().equals(hrmCandidateDTO.getPhone())) {
                model.addAttribute("errPhone", "Số điện thoại đã tồn tại");
                error = true;
            }
            if (i.equals(3) && !hrmCandidateDTOPrototype.getEmail().equals(hrmCandidateDTO.getEmail())) {
                model.addAttribute("errEmail", "Email đã tồn tại");
                error = true;
            }
        }

        if (error) {
            bindDataForAddCandidateForm(model);
            model.addAttribute("candidate", hrmCandidateDTO);
            displayDistrictsAndTowns(hrmCandidateDTO,model);
            displaySkill(hrmCandidateDTO,model);
            return "admin/hrm/candidate-update";
        }

        fileStorage.store(hrmCandidateDTO.getImage(), "abc");
        hrmCandidateService.save(hrmCandidateDTO);

        return "redirect:/admin/hrm/candidate/list";
    }
    private void displayDistrictsAndTowns(HrmCandidateDTO hrmCandidateDTO, Model model)
    {
        model.addAttribute("districts", districtRepository.findByProvinceId(hrmCandidateDTO.getProvinceId()));
        model.addAttribute("towns", wardRepository.findByDistrictId(hrmCandidateDTO.getDistrictId()));
    }

    private void displaySkill(HrmCandidateDTO hrmCandidateDTO, Model model)
    {
        model.addAttribute("skills", hrmCandidateDTO.getSkills());
        Page<SkillDTO> SkillDto = skillService.findAll(PageRequest.of(0, Integer.MAX_VALUE));
        List<SkillDTO> listSkillDto = SkillDto.getContent();
        if (listSkillDto != null && listSkillDto.size() > 0) {
            model.addAttribute("allSkills", listSkillDto);
        }
        List<String> uSkills = new ArrayList<>();
        for (Skill skill: hrmCandidateDTO.getSkills()) {
            uSkills.add(skill.getName());
        }
        model.addAttribute("candidateSkillsName", uSkills);
    }
    /**
     * Ninh Trang An
     * Delete param screen
     *
     */
    @PostMapping("/hrm/candidate/delete")
    public String deleteCandidate(@ModelAttribute("candidate") HrmCandidateDTO hrmCandidateDTO) {

        hrmCandidateService.deleteById(hrmCandidateDTO.getId());
        return "redirect:/admin/hrm/candidate/list";
    }
}
