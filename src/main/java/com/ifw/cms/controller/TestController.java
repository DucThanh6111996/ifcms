package com.ifw.cms.controller;

import com.ifw.cms.entity.*;
import com.ifw.cms.filestorage.FileStorage;
import com.ifw.cms.repository.DistrictRepository;
import com.ifw.cms.repository.HrmCandidateRepository;
import com.ifw.cms.repository.ProvinceRepository;
import com.ifw.cms.repository.WardRepository;
import com.ifw.cms.service.HrmEmployeeService;
import com.ifw.cms.service.SkillService;
import com.ifw.cms.service.dto.HrmCandidateDTO;
import com.ifw.cms.service.mapper.HrmCandidateMapper;
import com.ifw.cms.service.mapper.SkillMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

@Controller
@RequestMapping(value = "/test")
public class TestController {

    @Autowired
    HrmEmployeeService hrmEmployeeService;

    @Autowired
    SkillService skillService;

    @Autowired
    SkillMapper skillMapper;

    @Autowired
    HrmCandidateRepository hrmCandidateRepository;

    @Autowired
    HrmCandidateMapper hrmCandidateMapper;

    @Autowired
    ProvinceRepository provinceRepository;

    @Autowired
    DistrictRepository districtRepository;

    @Autowired
    WardRepository wardRepository;

    @Autowired
    FileStorage fileStorage;

    @GetMapping
    public void saveCandidate() {
        HrmCandidate hrmCandidate = new HrmCandidate();
        hrmCandidate.setCode("1");
        hrmCandidate.setDob(LocalDate.of(1996, 9, 21));
        Set<Skill> skills = new HashSet<>();
        Set<HrmCandidateExperience> hrmCandidateExperiences = new HashSet<>();
        Set<HrmCandidateProject> hrmCandidateProjects = new HashSet<>();
        skills.add(skillMapper.toEntity(skillService.findOne(Long.parseLong("1")).get()));
        skills.add(skillMapper.toEntity(skillService.findOne(Long.parseLong("2")).get()));

        HrmCandidateExperience hrmCandidateExperience = new HrmCandidateExperience();
        HrmCandidateExperience hrmCandidateExperience1 = new HrmCandidateExperience();
        HrmCandidateExperience hrmCandidateExperience2 = new HrmCandidateExperience();

        hrmCandidateExperience.setCompany("Viettel");
        hrmCandidateExperience.setDescription("Nang cap he thong");
        hrmCandidateExperience.setStart(LocalDate.now());
        hrmCandidateExperience.setEnd(LocalDate.now());

        HrmCandidateProject hrmCandidateProject = new HrmCandidateProject();
        hrmCandidateProject.setMember(Integer.parseInt("5"));
        hrmCandidateProject.setName("Quan ly nhan su itsol");

        hrmCandidateProjects.add(hrmCandidateProject);

        hrmCandidateExperiences.add(hrmCandidateExperience);

        hrmCandidate.setSkills(skills);
        hrmCandidate.setFullname("Mr Cuong");
        hrmCandidate.setGender("M");
        hrmCandidate.setExperiences(hrmCandidateExperiences);
        hrmCandidate.setProjects(hrmCandidateProjects);
        hrmCandidate.setPhone("012423");
        hrmCandidate.setEmail("doxuancuong96@gmail.com");

        Province province = provinceRepository.getOne(2L);
        District district = districtRepository.getOne(2L);

        hrmCandidate.setProvince(province);
        hrmCandidate.setDistrict(district);
        HrmCandidateDTO testDTO = hrmCandidateMapper.toDto(hrmCandidate);

        HrmCandidate test = hrmCandidateMapper.toEntity(testDTO);

        hrmCandidateRepository.save(test);
    }

    @PostMapping("/up")
    public void importEmployee(Model model, MultipartFile file){
        fileStorage.storeAvatar(file, file.getOriginalFilename());
    }

    @GetMapping("find")
    public void findAllByCodeOrPhoneOrEmail() {
        List<HrmCandidate> hrmCandidateList = hrmCandidateRepository.findAllByCodeOrPhoneOrEmail("as", "012423", "");
        for (HrmCandidate hrmCandidate : hrmCandidateList) {
            System.out.println(hrmCandidate);
        }
        Logger.getLogger("abc");
    }
}
