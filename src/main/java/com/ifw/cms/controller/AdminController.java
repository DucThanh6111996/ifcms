package com.ifw.cms.controller;

import com.ifw.cms.entity.AppRole;
import com.ifw.cms.entity.AppUser;
import com.ifw.cms.entity.*;
import com.ifw.cms.entity.enumeration.PaymentStatus;
import com.ifw.cms.filestorage.FileStorage;
import com.ifw.cms.repository.*;
import com.ifw.cms.service.*;
import com.ifw.cms.service.dto.*;
import com.ifw.cms.service.mapper.*;
import com.ifw.cms.utils.EncryptUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.*;
import java.security.Principal;
import java.util.*;

/**
 * Created by ITSOL SWIB Dev Team.
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminController {
    private UserService userService;

    private HrmEmployeeService hrmEmployeeService;

    private AppUserRepository appUserRepository;

    private AppRoleRepository appRoleRepository;

    private HrmEmployeeRepository hrmEmployeeRepository;

    private AppParamsService appParamsService;

    private AppParamsRepository appParamsRepository;

    private HrmPayoutService hrmPayoutService;

    private UserRoleService userRoleService;

    private FileStorage fileStorage;

    private UserMapper userMapper;

    private HrmEmployeeMapper hrmEmployeeMapper;

    private AppParamsMapper appParamsMapper;

    private BCryptPasswordEncoder passwordEncoder;

    private MailService mailService;

    private ProvinceRepository provinceRepository;

    private DistrictRepository districtRepository;

    private WardRepository wardRepository;

    private HrmCandidateService hrmCandidateService;

    private HrmCandidateRepository hrmCandidateRepository;

    private SkillService skillService;

    private SkillMapper skillMapper;

    private HrmCandidateMapper hrmCandidateMapper;
   
    private ProvinceService provinceService;

    public AdminController(UserService userService,
                           HrmEmployeeService hrmEmployeeService,
                           AppUserRepository appUserRepository,
                           AppRoleRepository appRoleRepository,
                           HrmEmployeeRepository hrmEmployeeRepository,
                           AppParamsService appParamsService,
                           AppParamsRepository appParamsRepository,
                           HrmPayoutService hrmPayoutService,
                           UserRoleService userRoleService,
                           FileStorage fileStorage,
                           UserMapper userMapper,
                           HrmEmployeeMapper hrmEmployeeMapper,
                           AppParamsMapper appParamsMapper,
                           BCryptPasswordEncoder passwordEncoder,
                           MailService mailService,
                           ProvinceRepository provinceRepository,
                           DistrictRepository districtRepository,
                           WardRepository wardRepository,
                           HrmCandidateService hrmCandidateService,
                           HrmCandidateRepository hrmCandidateRepository,
                           SkillService skillService,
                           SkillMapper skillMapper,
                           HrmCandidateMapper hrmCandidateMapper,
                           ProvinceService provinceService) {
        this.userService = userService;
        this.hrmEmployeeService = hrmEmployeeService;
        this.appUserRepository = appUserRepository;
        this.appRoleRepository = appRoleRepository;
        this.hrmEmployeeRepository = hrmEmployeeRepository;
        this.appParamsService = appParamsService;
        this.appParamsRepository = appParamsRepository;
        this.hrmPayoutService = hrmPayoutService;
        this.userRoleService = userRoleService;
        this.fileStorage = fileStorage;
        this.userMapper = userMapper;
        this.hrmEmployeeMapper = hrmEmployeeMapper;
        this.appParamsMapper = appParamsMapper;
        this.passwordEncoder = passwordEncoder;
        this.mailService = mailService;
        this.provinceRepository = provinceRepository;
        this.districtRepository = districtRepository;
        this.wardRepository = wardRepository;
        this.hrmCandidateService = hrmCandidateService;
        this.hrmCandidateRepository = hrmCandidateRepository;
        this.skillService = skillService;
        this.skillMapper = skillMapper;
        this.hrmCandidateMapper = hrmCandidateMapper;
        this.provinceService = provinceService;
    }

    /**
     * List all users, filter by key
     *
     * @param model
     * @param key
     * @param pageable
     * @return
     */
    @RequestMapping(value = "/user/list", method = RequestMethod.GET)
    public String listAllUsers(Model model, @RequestParam("key") Optional<String> key, Pageable pageable) {
        Page<UserVM> page = userService.findAllUsers(pageable, key.orElse(""));
        model.addAttribute("page", page);

        return "admin/user/user-list";
    }

    /**
     * Reset user's password
     *
     * @param model
     * @param uid
     * @return OK or NOK
     */
    @RequestMapping(value = "/user/resetpwd/{uid}", method = RequestMethod.POST)
    @ResponseBody
    public String resetpwd(Model model, @PathVariable Long uid) {
        Optional<UserVM> user = appUserRepository.findById(uid).map(userMapper::toVM);
        if (user.isPresent()) {
            String newPassword = RandomStringUtils.randomAlphanumeric(6);
            user.get().setEncrytedPassword(passwordEncoder.encode(newPassword));

            // send email
            mailService.sendMailResetPassword(user.get().getEmail(), user.get().getUserName(), newPassword);

            // save new password
            userService.save(user.get());
            return "OK";
        }
        return "NOK";
    }


    @RequestMapping(value = "/user/new", method = RequestMethod.GET)
    public String preAddUser(Model model) {
        model.addAttribute("user", new UserVM());

        List<AppRole> allRoles = appRoleRepository.findAllRoles();

        if (allRoles != null && allRoles.size() > 0) {
            model.addAttribute("allRoles", allRoles);
        }

        return "admin/user/user-add";
    }

    /**
     * Save user
     *
     * @param model
     * @param user
     * @param request
     * @param result
     * @return
     */
    @RequestMapping(value = "/user/save", method = RequestMethod.POST)
    public String saveUser(Model model,
                           @ModelAttribute("user") @Valid final UserVM user,
                           HttpServletRequest request,
                           final BindingResult result) {
        String[] selectedRoles = request.getParameterValues("uroles");

        String newPassword = RandomStringUtils.randomAlphanumeric(6);
        user.setEncrytedPassword(passwordEncoder.encode(newPassword));
        user.setEnabled(true);

        // validate user name
        AppUser check = appUserRepository.findByUserName(user.getUserName());
        if (check != null) {
            FieldError ssoError = new FieldError("userName", "userName", "Ten dang nhap ton tai");
            result.addError(ssoError);

            List<AppRole> allRoles = appRoleRepository.findAllRoles();

            if (allRoles != null && allRoles.size() > 0) {
                model.addAttribute("allRoles", allRoles);
            }

            return "admin/user/user-add";
        }

        // save
        UserVM newUser = userService.save(user);

        //save user's roles
        if (newUser != null) {
            if (user.getEmail() != null && !user.getEmail().isEmpty()) {
                mailService.sendMailResetPassword(user.getEmail(), user.getUserName(), newPassword);
            }

            // assign new roles, may duplicate last time
            for (String u : selectedRoles) {
                userRoleService.addUserRole(newUser.getUserId(), Long.parseLong(u));
            }

            return "redirect:/admin/user/detail/" + newUser.getUserId();
        }

        return "admin/user/user-add";
    }

    /**
     * Update user's info
     *
     * @param uid
     * @param request
     * @return
     */
    @RequestMapping(value = "/user/update/{uid}", method = RequestMethod.POST)
    public String updateUser(@PathVariable Long uid, HttpServletRequest request) {
        String[] selectedRoles = request.getParameterValues("roles");
        if (selectedRoles == null && selectedRoles.length == 0) {
            return "redirect:/admin/user/detail/" + uid;
        }

        // find to make sure user exists
        Optional<UserVM> user = appUserRepository.findById(uid).map(userMapper::toVM);
        if (user.isPresent()) {
            user.get().setEmail(request.getParameter("email"));
            userService.save(user.get());

            // truncate current roles
            userRoleService.deleteRoleByUserId(uid);

            // assign new roles, may duplicate last time
            for (String u : selectedRoles) {
                userRoleService.addUserRole(uid, Long.parseLong(u));
            }
        }

        return "redirect:/admin/user/detail/" + uid;
    }

    /**
     * View user detail by id
     *
     * @param model
     * @param uid
     * @return
     */
    @RequestMapping(value = "/user/detail/{uid}", method = RequestMethod.GET)
    public String getUserDetail(Model model, @PathVariable Long uid) {
        Optional<UserVM> user = appUserRepository.findById(uid).map(userMapper::toVM);
        if (user.isPresent()) {
            model.addAttribute("user", user.get());

            // find user's roles
            List<AppRole> userRoles = appRoleRepository.findByUserRole(user.get().getUserId());
            List<String> uRoles = new ArrayList<>();
            if (userRoles != null && userRoles.size() > 0) {
                for (AppRole u : userRoles) {
                    uRoles.add(u.getRoleName());
                }
                model.addAttribute("userRoleNames", uRoles);
                model.addAttribute("userRoles", userRoles);
            }

            List<AppRole> allRoles = appRoleRepository.findAllRoles();

            if (allRoles != null && allRoles.size() > 0) {
                model.addAttribute("allRoles", allRoles);
            }
        }
        return "admin/user/user-detail";
    }


    /**
     * List all employee, filter by key
     *
     * @param model
     * @param pageable
     * @param key
     * @return
     */
    @RequestMapping(value = "/hrm/employee/list", method = RequestMethod.GET)
    public String listAllEmployee(Model model, Pageable pageable, @RequestParam("key") Optional<String> key) {
        Page<HrmEmployeeDTO> page = hrmEmployeeService.search(pageable, key.orElse(""), key.orElse(""),
                "A").map(u -> {
            u.setSlrAmtEnc(EncryptUtils.decrypt(u.getSlrAmtEnc()));
            u.setSlrBefore(EncryptUtils.decrypt(u.getSlrBefore()));
            return u;
        });
        model.addAttribute("page", page);

        return "admin/hrm/employee-list";
    }

    /**
     * Get employee detail
     *
     * @param model
     * @param uid
     * @return
     */
    @RequestMapping(value = "/hrm/employee/detail/{uid}", method = RequestMethod.GET)
    public String getEmloyeeDetail(Model model, @PathVariable Long uid) {
        Optional<HrmEmployeeDTO> em = hrmEmployeeRepository.findById(uid).map(hrmEmployeeMapper::toDto);
        if (em.isPresent()) {
            HrmEmployeeDTO data = em.get();
            Optional<Province> province = provinceRepository.findById(em.get().getPermProvinceId());
            if (province.isPresent()) {
                Optional<District> district = districtRepository.findById(em.get().getPermDistrictId());
                if (district.isPresent()) {
                    Optional<Ward> ward = wardRepository.findById(em.get().getPermTownId());
                    if (ward.isPresent()) {
                        model.addAttribute("ward", ward.get().getName());
                    }
                    model.addAttribute("district", district.get().getName());
                }
                model.addAttribute("province", province.get().getName());
            }
            data.setSlrAmtEnc(EncryptUtils.decrypt(data.getSlrAmtEnc()));
            data.setSlrBefore(EncryptUtils.decrypt(data.getSlrBefore()));
            model.addAttribute("employee", data);
        }
        return "admin/hrm/employee-detail";
    }

    /**
     * Show create new employee screen
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/hrm/employee/new", method = RequestMethod.GET)
    public String getAddEmployeeForm(Model model) {
        HrmEmployeeDTO employeeDTO = new HrmEmployeeDTO();
        model.addAttribute("employee", employeeDTO);
        bindDataForAddEmpForm(model);
        return "admin/hrm/employee-add";
    }

    /**
     * Save employee
     *
     * @param employee
     * @param result
     * @param model
     * @param pageable
     * @return
     */
    @PostMapping("/hrm/employee/save")
    public String addEmployee(@ModelAttribute("employee") @Valid final HrmEmployeeDTO employee,
                              final BindingResult result, Model model, Pageable pageable) {
        if (result.hasErrors()) {
            model.addAttribute("employee", employee);
            return "admin/hrm/employee-add";
        }

        //handle file uploads
        if (employee.getCvFiles().length > 0) {
            for (MultipartFile fileData : employee.getCvFiles()) {
                if (StringUtils.isNotEmpty(fileData.getOriginalFilename())) {
                    String newName = "CV-" + employee.getId()
                            + "-" + employee.getFullname()
                            + "."
                            + fileData.getOriginalFilename().substring(fileData.getOriginalFilename().lastIndexOf(".") + 1);
                    employee.setCv(newName);
                    fileStorage.store(fileData, newName);
                }
            }
        }

        hrmEmployeeService.save(employee);

        return "redirect:/admin/hrm/employee/list";
    }

    /**
     * Show update employee screen
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/hrm/employee/pre-update/{id}", method = RequestMethod.GET)
    public String getUpdateEmployeeForm(Model model, @PathVariable("id") Long id) {
        Optional<HrmEmployeeDTO> employeeDTO = hrmEmployeeService.findOne(id);
        bindDataForAddEmpForm(model);


        if (employeeDTO.isPresent()) {
            model.addAttribute("employee", employeeDTO.get());

            model.addAttribute("districts", districtRepository.findByProvinceId(employeeDTO.get().getPermProvinceId()));
            model.addAttribute("towns", wardRepository.findByDistrictId(employeeDTO.get().getPermDistrictId()));
        }

        return "admin/hrm/employee-update";
    }

    /**
     * Do update employee
     *
     * @param employee
     * @param result
     * @param model
     * @param principal
     * @param request
     * @return
     */
    @PostMapping("/hrm/employee/update")
    public String updateEmployee(@ModelAttribute("employee") @Valid final HrmEmployeeDTO employee,
                                 final BindingResult result, Model model, Principal principal, HttpServletRequest request) {
        if (result.hasErrors()) {
            model.addAttribute("employee", employee);
            return "redirect:/admin/hrm/employee/pre-update/" + employee.getId();
        }

        Collection<? extends GrantedAuthority> authorities = ((Authentication) principal).getAuthorities();

        boolean isAuthorizedToUpdateSlr = false;
        for (GrantedAuthority grantedAuthority : authorities) {
            if (grantedAuthority.getAuthority().equals("ROLE_SAL")) {
                isAuthorizedToUpdateSlr = true;
                break;
            }
        }

        if (!isAuthorizedToUpdateSlr) {
            Optional<HrmEmployeeDTO> ori = hrmEmployeeService.findOne(employee.getId());
            if (ori.isPresent()) {
                employee.setSlrBefore(ori.get().getSlrBefore());
                employee.setSlrAmtEnc(ori.get().getSlrAmtEnc());
            }
        }

        //handle file uploads
        if (employee.getCvFiles().length > 0) {
            for (MultipartFile fileData : employee.getCvFiles()) {
                if (StringUtils.isNotEmpty(fileData.getOriginalFilename())) {
                    String newName = "CV-" + employee.getId()
                            + "-" + employee.getFullname()
                            + "."
                            + fileData.getOriginalFilename().substring(fileData.getOriginalFilename().lastIndexOf(".") + 1);
                    employee.setCv(newName);
                    fileStorage.store(fileData, newName);
                }
            }
        }

        hrmEmployeeService.save(employee);

        return "redirect:/admin/hrm/employee/list";
    }

    /**
     * Ninh Trang An
     * delete Emplyee by Id
     *
     * @param hrmCandidateDTO
     * @return
     */
    @PostMapping("/hrm/employee/delete")
    public String deleteCandidate(@ModelAttribute("employee") HrmCandidateDTO hrmCandidateDTO) {
        hrmEmployeeService.delete(hrmCandidateDTO.getId());
        return "redirect:/admin/hrm/employee/list";
    }

    /**
     * Change status of payment
     *
     * @param model
     * @param id
     * @param status
     * @return
     */
    @RequestMapping(value = "/hrm/payout/change-status/{id}/{status}", method = RequestMethod.GET)
    public String changePayoutStatus(Model model, @PathVariable("id") Long id, @PathVariable("status") String status) {
        Optional<HrmPayoutDTO> payout = hrmPayoutService.findOne(id);
        if (payout.isPresent()) {
            if (status.equalsIgnoreCase(PaymentStatus.PAID.toString())) {
                payout.get().setStatus(PaymentStatus.PAID);
            }

            hrmPayoutService.save(payout.get());
        }

        return "redirect:/";
    }

    /**
     * ThanhDT
     **/
    @RequestMapping(value = "/report/employee-by-unit", method = RequestMethod.GET)
    public String getReportByUnit() {
        return "admin/report/employee-by-unit";
    }

    /**
     * ThanhDT
     **/
    @RequestMapping(value = "/report/employee-by-role", method = RequestMethod.GET)
    public String getReportByRole() {
        return "admin/report/employee-by-role";
    }

    /**
     * ThanhDT
     **/
    @RequestMapping(value = "/report/exp-employee", method = RequestMethod.GET)
    public String getReportByExpEmployee(Model model, Pageable pageable) {
        Page<HrmEmployeeDTO> page = hrmEmployeeService.getEmployeeContactExpired(pageable);
        model.addAttribute("page", page);
        return "admin/report/exp-employee";
    }

    @RequestMapping(value = "/report/insource-employee", method = RequestMethod.GET)
    public String getReportByInsourceEmployee() {
        return "admin/report/employeeInsource";
    }


    /**
     * Show create new param screen
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/system/app-param/new", method = RequestMethod.GET)
    public String getAddParamForm(Model model, @RequestParam(name = "groupCode") String groupCode) {
        AppParamsDTO appParamsDTO = new AppParamsDTO();
        appParamsDTO.setGroupCode(groupCode);
        model.addAttribute("groupCode", appParamsDTO.getGroupCode());
        model.addAttribute("param", appParamsDTO);
        bindDataForAddParamForm(model);

        return "admin/system/param-add";
    }

    /**
     * save param
     * NinhTrangAn
     * /**
     *
     * @param model
     */
    @PostMapping("/system/app-param/save")
    public String addParam(@ModelAttribute("param") @Valid final AppParamsDTO appParamsDTO,
                           final BindingResult result, Model model) {

        boolean error = false;
        if (result.hasErrors()) {
            error = true;
        }

        for (Integer integer : appParamsService.checkFieldExists(appParamsDTO.getCode(), appParamsDTO.getName())) {
            if (integer.equals(Integer.parseInt("1"))) {
                model.addAttribute("errorCode", "Mã danh mục đã tồn tại");
                error = true;
            }
            if (integer.equals(Integer.parseInt("2"))) {
                model.addAttribute("errorName", "Tên danh mục đã tồn tại");
                error = true;
            }
        }
        if (error) {
            model.addAttribute("param", appParamsDTO);
            model.addAttribute("groupCode", appParamsDTO.getGroupCode());
            return "admin/system/param-add";
        }
        bindDataForAddParamForm(model);
        appParamsService.save(appParamsDTO);
        return "redirect:/admin/system/app-param/" + appParamsDTO.getGroupCode();
    }

    /**
     * NinhTrangAn
     * Get param detail
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/system/app-param/detail/{id}", method = RequestMethod.GET)
    public String getParamDetail(Model model, @PathVariable Long id) {
        AppParamsDTO appParamsDTO = appParamsService.findById(id);
        if (appParamsDTO != null) {
            model.addAttribute("pr", appParamsDTO);
            model.addAttribute("groupCode", appParamsDTO.getGroupCode());
        }
        return "admin/system/param-detail";
    }


    /**
     * NinhTrangAn
     * Show update param screen
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/system/app-param/pre-update/{id}", method = RequestMethod.GET)
    public String getUpdateParamForm(Model model, @PathVariable("id") Long id) {

        AppParamsDTO appParamsDTO = appParamsService.findById(id);
        if (appParamsDTO != null) {
            model.addAttribute("param", appParamsDTO);
        }
        bindDataForAddParamForm(model);
        model.addAttribute("groupCode", appParamsDTO.getGroupCode());

        return "admin/system/param-update";
    }

    /**
     * NinhTrangAn
     * Do update param
     *
     * @param appParamsDTO
     * @param result
     * @param model
     * @return
     */
    @PostMapping("/system/app-param/update")
    public String updateParam(
            @ModelAttribute("param") @Valid final AppParamsDTO appParamsDTO, final BindingResult result,
            Model model) {
        boolean error = false;

        if (result.hasErrors()) {
            error = true;
        }

        AppParamsDTO appParamsDTO1 = appParamsService.findById(appParamsDTO.getId());

        for (Integer i : appParamsService.checkFieldExists(
                appParamsDTO.getCode(), appParamsDTO.getName())) {
            if (i.equals(1) && !appParamsDTO.getCode().equals(appParamsDTO1.getCode())) {
                model.addAttribute("errCode", "Mã danh mục đã tồn tại");
                error = true;
            }
            if (i.equals(2) && !appParamsDTO.getName().equals(appParamsDTO1.getName())) {
                model.addAttribute("errName", "Tên danh mục đã tồn tại");
                error = true;
            }
        }

        if (error) {
            model.addAttribute("param", appParamsDTO);
            model.addAttribute("groupCode", appParamsDTO.getGroupCode());
            return "admin/system/param-update";
        }

        appParamsService.save(appParamsDTO);
        return "redirect:/admin/system/app-param/" + appParamsDTO.getGroupCode();
    }

    /**
     * Ninh Trang An
     * Delete param screen
     *
     * @param
     */
    @PostMapping("/system/app-param/delete")
    public String deleteParam(@ModelAttribute("apps") AppParamsDTO paramsDTO) {
        appParamsService.delete(paramsDTO.getId());
        return "redirect:/admin/system/app-param/" + paramsDTO.getGroupCode();
    }

    @RequestMapping(value = "/system/province/new", method = RequestMethod.GET)
    public String getAddProvinceForm(Model model, @RequestParam(name = "province") Long id) {
        Province province = new Province();
        province.setId(id);
        model.addAttribute("province", province);
        return "admin/system/province-add";
    }


    /**
     * Ninh Trang An
     * Province detail
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/system/province/detail/{id}", method = RequestMethod.GET)
    public String getProvinceDetail(Model model, @PathVariable Long id) {
        Optional<Province> province = provinceRepository.findById(id);
        if (province.isPresent()) {
            model.addAttribute("province", province.get());
        }
        return "admin/system/province-detail";
    }

    @RequestMapping(value = "/system/province/update/{id}", method = RequestMethod.GET)
    public String getUpdateProvinceForm(Model model, @PathVariable("id") Long id) {
        Optional<Province> province = provinceRepository.findById(id);
        bindDataForAddParamForm(model);
        model.addAttribute("province", province.get());


        if (province.isPresent()) {
            Province province1 = province.get();
            province1.setId(province1.getId());
            model.addAttribute("province", province1);
        }
        return "admin/system/province-update";
    }

    @PostMapping("/system/province/update")
    public String updateParam(
            @ModelAttribute("province") @Valid final Province province, final BindingResult result,
            Model model) {

        boolean error = false;
        if (result.hasErrors()) {
            error = true;
        }
        Optional<Province> provinceOptional = provinceService.findById(province.getId());
        for (Integer i : provinceService.checkFieldExists(
                province.getId(), province.getName())) {
            if (i.equals(1) && !provinceOptional.get().getId().equals(province.getId())) {
                model.addAttribute("errorId", "Mã tỉnh thành đã tồn tại");
                error = true;
            }
            if (i.equals(2) && !provinceOptional.get().getName().equals(province.getName())) {
                model.addAttribute("errorName", "Tên tỉnh thành đã tồn tại");
                error = true;
            }
        }
        if (error) {
//            model.addAttribute("province",province);
            return "admin/system/province-update";
        }

        provinceService.save(province);
        return "redirect:/admin/system/province/list";
    }

    @PostMapping("/system/province/delete")
    public String deleteProvince(@ModelAttribute("province") Province province) {

        provinceService.delete(province.getId());
        return "redirect:/admin/system/province/list";
    }


    private void bindDataForAddEmpForm(Model model) {
        // get province
        List provinces = provinceRepository.findAll();
        model.addAttribute("provinces", provinces);
        model.addAttribute("districts", null);
        model.addAttribute("towns", null);

        model.addAttribute("units", appParamsService.findByGroupCode("UNIT"));
        model.addAttribute("role", appParamsService.findByGroupCode("EM_ROLE"));
        model.addAttribute("levels", appParamsService.findByGroupCode("EM_LEVEL"));
        model.addAttribute("branches", appParamsService.findByGroupCode("BRANCH"));
        model.addAttribute("posits", appParamsService.findByGroupCode("EM_POSIT"));
        model.addAttribute("contractTypes", appParamsService.findByGroupCode("EM_CNT_TYP"));
    }

    /**
     * NinhTrangAn get list GroupCode
     */
    private void bindDataForAddParamForm(Model model) {
        List groupCode = appParamsRepository.findAll();
        model.addAttribute("groupCode", groupCode);
        model.addAttribute("pa", appParamsRepository.findAll());
    }

}
