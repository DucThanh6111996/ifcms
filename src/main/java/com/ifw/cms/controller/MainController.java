package com.ifw.cms.controller;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import com.ifw.cms.entity.*;
import com.ifw.cms.entity.enumeration.PaymentStatus;
import com.ifw.cms.filestorage.FileStorage;
import com.ifw.cms.repository.AppUserRepository;
import com.ifw.cms.repository.DistrictRepository;
import com.ifw.cms.repository.ProvinceRepository;
import com.ifw.cms.repository.WardRepository;
import com.ifw.cms.service.ContractService;
import com.ifw.cms.service.CustomerBranchService;
import com.ifw.cms.service.DashboardService;
import com.ifw.cms.service.HrmPayoutService;
import com.ifw.cms.service.dto.AppUserDTO;
import com.ifw.cms.service.dto.DashboardDTO;
import com.ifw.cms.service.dto.HrmPayoutDTO;
import com.ifw.cms.utils.EncryptUtils;
import com.ifw.cms.utils.WebUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class MainController {

  @Autowired
  private DashboardService dashboardService;

  @Autowired
  private HrmPayoutService hrmPayoutService;

  @Autowired
  private FileStorage fileStorage;

  @Autowired
  private AppUserRepository appUserRepository;

  @Autowired
  private BCryptPasswordEncoder passwordEncoder;

  @Autowired
  private MessageSource messageSource;

  @Autowired
  private ProvinceRepository provinceRepository;

  @Autowired
  private DistrictRepository districtRepository;

  @Autowired
  private WardRepository wardRepository;

  @Autowired
  private CustomerBranchService customerBranchService;

  @Autowired
  private ContractService contractService;

  /**
   * Login page
   * @param model
   * @return
   */
  @RequestMapping(value = "/login", method = RequestMethod.GET)
  public String loginPage(Model model) {
    return "login";
  }


  /**
   * Default page
   * @param model
   * @param pageable
   * @return
   */
  @RequestMapping(value = { "/", "/welcome" }, method = RequestMethod.GET)
  public String welcomePage(Model model, Pageable pageable) {
    Optional<DashboardDTO> dashboardDTO = dashboardService.getStat();
    if (dashboardDTO.isPresent()) {
      model.addAttribute("dashboard", dashboardDTO.get());
    }

    Page<HrmPayoutDTO> pagePayout = hrmPayoutService.findByStatus(pageable, PaymentStatus.UNPAID).map(u -> {
      u.setAmount(EncryptUtils.decrypt(u.getAmount()));
      return u;
    });

    model.addAttribute("page", pagePayout);

    model.addAttribute("title", "Welcome");
    return "welcome";
  }

  /**
   * Admin page
   * @param model
   * @param principal
   * @return
   */
  @RequestMapping(value = "/admin", method = RequestMethod.GET)
  public String adminPage(Model model, Principal principal) {
    User loginedUser = (User) ((Authentication) principal).getPrincipal();
    String userInfo = WebUtils.toString(loginedUser);
    model.addAttribute("userInfo", userInfo);

    return "admin";
  }

  /**
   * Logout page
   * @param model
   * @return
   */
  @RequestMapping(value = "/logoutSuccessful", method = RequestMethod.GET)
  public String logoutSuccessfulPage(Model model) {
    model.addAttribute("title", "Logout");
    return "logoutSuccessful";
  }

  /**
   * User information page
   * @param model
   * @param principal
   * @return
   */
  @RequestMapping(value = "/userInfo", method = RequestMethod.GET)
  public String userInfo(Model model, Principal principal) {
    // Sau khi user login thanh cong se co principal
    String userName = principal.getName();
    User loginedUser = (User) ((Authentication) principal).getPrincipal();
    String userInfo = WebUtils.toString(loginedUser);
    model.addAttribute("userInfo", userInfo);
    model.addAttribute("userName", userName);

    return "userInfo";
  }

  /**
   * User change password form
   * @param model
   * @param principal
   * @return
   */
  @RequestMapping(value = "/change-password", method = RequestMethod.GET)
  public String changePassword(Model model, Principal principal) {
    User loginedUser = (User) ((Authentication) principal).getPrincipal();
    String userInfo = WebUtils.toString(loginedUser);
    model.addAttribute("userInfo", userInfo);

    AppUserDTO userProfile = new AppUserDTO();
    userProfile.setUserName(loginedUser.getUsername());

    model.addAttribute("profile", userProfile);
    return "modules/profile/change-password";
  }

  /**
   * Handle password change
   * @param model
   * @param principal
   * @return result view
   */
  @PostMapping("/do-change-password")
  public String doChangePassword(
          @ModelAttribute("profile") @Valid final AppUserDTO userProfile,
          final BindingResult result,
          Model model,
          Principal principal) {
    if (result.hasErrors()) {
      model.addAttribute("profile", userProfile);
      return "modules/profile/change-password";
    }

    User loginedUser = (User) ((Authentication) principal).getPrincipal();
    AppUser u = appUserRepository.findByUserName(loginedUser.getUsername());

    // compare old password with user's pw in DB
    if (!passwordEncoder.matches(userProfile.getOldPassword(), u.getEncrytedPassword())) {
      FieldError ssoError = new FieldError("profile", "oldPassword","Old password");
      result.addError(ssoError);
      model.addAttribute("profile", userProfile);
      return "modules/profile/change-password";
    }

    // compare new passwords
    if (!userProfile.getPassword().equals(userProfile.getConfirmPassword())) {
      FieldError ssoError = new FieldError("profile", "confirmPassword","Confirm password");
      result.addError(ssoError);
      model.addAttribute("profile", userProfile);
      return "modules/profile/change-password";
    }

    // replace by new password
    u.setEncrytedPassword(passwordEncoder.encode(userProfile.getPassword()));
    appUserRepository.save(u);

    // return view
    model.addAttribute("message", "OK");
    return "modules/profile/change-password-result";
  }

  /**
   * Return page for unauthenticated requests
   * @param model
   * @param principal
   * @return
   */
  @GetMapping(value = "/403")
  public String accessDenied(Model model, Principal principal) {
    if (principal != null) {
      User loginedUser = (User) ((Authentication) principal).getPrincipal();
      String userInfo = WebUtils.toString(loginedUser);
      model.addAttribute("userInfo", userInfo);
      String message = "Hi " + principal.getName() //
          + "<br> You do not have permission to access this page!";
      model.addAttribute("message", message);
    }

    return "403";
  }

  /**
   * Download file
   * @param filename
   * @return
   */
  @GetMapping(value = "/download/{filename}")
  public ResponseEntity<Resource> downloadFile(@PathVariable String filename) {
    Resource file = fileStorage.loadFile(filename);
    return ResponseEntity.ok()
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
            .body(file);
  }

  @GetMapping(value="/common/district/list/{provinceId}")
  //@ResponseBody
  public ResponseEntity<List<District>> getDistrictByProvinceId(Model model, @PathVariable("provinceId") Long provinceId) {
      List<District> p = districtRepository.findByProvinceId(provinceId);

      return ResponseEntity.ok(p);
  }

  @GetMapping(value = "/common/ward/list/{districtId}")
  public ResponseEntity<List<Ward>> getWardByDistrictId(Model model, @PathVariable("districtId") Long districtId) {
    List<Ward> p = wardRepository.findByDistrictId(districtId);

    return ResponseEntity.ok(p);
  }

  @GetMapping(value = "/common/branch/list/{customerId}")
  public ResponseEntity<List<CustomerBranch>> getBranchByCustomer(Model model, @PathVariable("customerId") Long customerId) {
    List<CustomerBranch> p = customerBranchService.findByCustomerId(customerId);

    return ResponseEntity.ok(p);
  }





}
