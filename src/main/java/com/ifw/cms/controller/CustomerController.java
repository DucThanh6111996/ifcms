package com.ifw.cms.controller;

import com.ifw.cms.entity.Customer;
import com.ifw.cms.service.ContractService;
import com.ifw.cms.service.CustomerBranchService;
import com.ifw.cms.service.CustomerPaymentService;
import com.ifw.cms.service.CustomerService;
import com.ifw.cms.service.dto.ContractDTO;
import com.ifw.cms.service.dto.CustomerDTO;
import com.ifw.cms.service.dto.CustomerPaymentDTO;
import com.ifw.cms.service.mapper.CustomerPaymentMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/admin")
public class CustomerController {

    private CustomerService customerService;
    private CustomerPaymentService customerPaymentService;
    private CustomerPaymentMapper customerPaymentMapper;
    private CustomerBranchService customerBranchService;
    private ContractService contractService;

    public CustomerController (CustomerService customerService,
                               CustomerPaymentService customerPaymentService,
                               CustomerPaymentMapper customerPaymentMapper,
                               CustomerBranchService customerBranchService,
                               ContractService contractService) {
        this.customerService = customerService;
        this.customerPaymentService = customerPaymentService;
        this.customerPaymentMapper = customerPaymentMapper;
        this.customerPaymentService = customerPaymentService;
        this.customerBranchService = customerBranchService;
        this.contractService = contractService;
    }

    /**
     * Ninh Trang An
     * List all customer, filter by key
     *
     */
    @RequestMapping(value = "/customer/list", method = RequestMethod.GET)
    public String listAllCustomer(Model model, Pageable pageable,
                                   @RequestParam("key") Optional<String> key) {
        Page<CustomerDTO> page = customerService.search(pageable, key.orElse("")
                , key.orElse(""));
        model.addAttribute("page", page);
        return "admin/customer/customer-list";
    }

    /**
     * Ninh Trang An
     * Show create new customer screen
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/customer/new", method = RequestMethod.GET)
    public String getAddCustomerForm(Model model,String code,Pageable pageable) {
        Customer customer = new Customer();
        customer.setCode(code);
        model.addAttribute("customer", customer);
        return "admin/customer/customer-add";
    }


    /**
     * save candidate
     * NinhTrangAn
     * /**
     *
     * @param model
     */
    @PostMapping("/customer/save")
    public String addCustomer(@ModelAttribute("customer") @Valid final CustomerDTO customerDTO,
                               final BindingResult result, Model model, HttpServletRequest request) {
        boolean error = false;

        if (result.hasErrors()) {
            model.addAttribute("customer", customerDTO);
            error = true;
        }

        for (Integer integer : customerService.checkFieldExists(customerDTO.getCode(), customerDTO.getName())) {
            if (integer.equals(Integer.parseInt("1"))) {
                model.addAttribute("errorCode", "Mã khách hàng đã tồn tại");
                error = true;
            }
            if (integer.equals(Integer.parseInt("2"))) {
                model.addAttribute("errorName", "Tên khách đã tồn tại");
                error = true;
            }
        }
        if(error) {
            model.addAttribute("customer", customerDTO);
            return "admin/customer/customer-add";
        }
        customerService.save(customerDTO);

        return "redirect:/admin/customer/list";
    }

    /**
     * Ninh Trang An
     * Get customer detail
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/customer/detail/{id}", method = RequestMethod.GET)
    public String getCustomerDetail(Model model, @PathVariable Long id) {

        CustomerDTO customerDTO = customerService.findById(id);

        if (customerDTO != null) {
            model.addAttribute("customer", customerDTO);
        }
        return "admin/customer/customer-detail";
    }

    /**
     * NinhTrangAn
     * Show update candidate screen
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/customer/update/{id}", method = RequestMethod.GET)
    public String getUpdateCustomerForm(Model model, @PathVariable("id") Long id) {

        CustomerDTO customerDTO = customerService.findById(id);
        if (customerDTO!=null) {
            model.addAttribute("customer", customerDTO);
        }
        return "admin/customer/customer-update";
    }

    /**
     * Ninh Trang An
     * @param
     * @param result
     * @param model
     * @return
     */
    @PostMapping("/customer/update")
    public String updateCustomer(@ModelAttribute("customer") @Valid final CustomerDTO customerDTO,
                                 final BindingResult result, Model model) {
        boolean error = false;

        if(result.hasErrors()){
            error = true;
        }

        CustomerDTO customer = customerService.findById(customerDTO.getId());
        for (Integer i : customerService.checkFieldExists(
                customerDTO.getCode(),
                customerDTO.getName())) {
            if (i.equals(1) && !customer.getCode().equals(customerDTO.getCode())) {
                model.addAttribute("errCode", "Mã khách hàng đã tồn tại");
                error = true;
            }
            if (i.equals(2) && !customer.getName().equals(customerDTO.getName())) {
                    model.addAttribute("errName", "Tên khách hàng đã tồn tại");
                error = true;
            }
        }

        if (error) {
            model.addAttribute("customer", customerDTO);
            return "admin/customer/customer-update";
        }

        customerService.save(customerDTO);

        return "redirect:/admin/customer/list";
    }

    /**
     * Ninh Trang An
     * Delete customer screen
     *
     */
    @PostMapping("/customer/delete")
    public String deleteCustomer(@ModelAttribute("customer") Customer customer) {

        customerService.delete(customer.getId());
        return "redirect:/admin/customer/list";
    }

    /**
     * Ninh Trang An
     * List all customer_payment, filter by key
     *
     */
    @RequestMapping(value = "/customer/payment/list", method = RequestMethod.GET)
    public String listAllCustomerPayment(Model model, Pageable pageable,
                                  @RequestParam("key") Optional<String> key) {
        Page<CustomerPaymentDTO> page = customerPaymentService.search(pageable, key.orElse("")
                , key.orElse(""));
        model.addAttribute("page", page);
        for (int i = 0;i<page.getContent().size();i++)
        {
            ContractDTO contractDTO = contractService.findById(page.getContent().get(i).getContractId());
            model.addAttribute("test",contractDTO);

        }
        return "admin/customer/payment-list";
    }
    /**
     * Ninh Trang An
     * Show create new customer_payment screen
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/customer/payment/new", method = RequestMethod.GET)
    public String getAddCustomerPaymentForm(Model model,String code,Pageable pageable) {

        CustomerPaymentDTO customerPaymentDTO = new CustomerPaymentDTO();
        customerPaymentDTO.setCode(code);
        model.addAttribute("ctmpayment", customerPaymentDTO);
        getListCustomer(model,pageable);

        return "admin/customer/payment-add";
    }

    private void getListCustomer(Model model,Pageable pageable)
    {
//        Page<CustomerBranchDTO> customerBranch = customerBranchService.findAll(pageable);
//        model.addAttribute("branch", customerBranch);
        Page<CustomerDTO> customerList = customerService.findAll(pageable);
        model.addAttribute("customerForm", customerList);
        Page<ContractDTO>  contractDTO = contractService.findAll(pageable);
        model.addAttribute("codeContract",contractDTO);
    }

    /**
     * NinhTrangAn
     *
     * save customer_payment
     *
     *
     * @param model
     */
    @PostMapping("/customer/payment/save")
    public String addCustomerPayment(@ModelAttribute("ctmpayment") @Valid final CustomerPaymentDTO customerPaymentDTO,
                              final BindingResult result, Model model, Pageable pageable) {
        boolean error = false;

        if (result.hasErrors()) {
            model.addAttribute("ctmpayment", customerPaymentDTO);
            error = true;
        }

        for (Integer integer : customerPaymentService.checkFieldExists(customerPaymentDTO.getCode())) {
            if (integer.equals(Integer.parseInt("1"))) {
                model.addAttribute("errorCode", "Mã hóa đơn đã tồn tại");
                error = true;
            }

            model.addAttribute("payment", customerPaymentDTO);
        }
        if(error) {
            getListCustomer(model,pageable);
            model.addAttribute("payment", customerPaymentDTO);
            return "admin/customer/payment-add";
        }
        customerPaymentService.save(customerPaymentDTO);

        return "redirect:/admin/customer/payment/list";
    }

    /**
     * Ninh Trang An
     * Get customer detail
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/customer/payment/detail/{id}", method = RequestMethod.GET)
    public String getCustomerPaymentDetail(Model model, @PathVariable Long id) {

       CustomerPaymentDTO  customerPaymentDTO = customerPaymentService.findById(id);
       CustomerDTO customerDTO = customerService.findById(customerPaymentDTO.getCustomerId());
       ContractDTO contractDTO = contractService.findById(customerPaymentDTO.getContractId());

        if (customerPaymentDTO != null) {
            model.addAttribute("customer", customerDTO.getName());
            model.addAttribute("contract", contractDTO.getName());
            model.addAttribute("payment", customerPaymentDTO);
        }
        return "admin/customer/payment-detail";
    }

    /**
     * NinhTrangAn
     * Show update customer_payment screen
     *
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/customer/payment/update/{id}", method = RequestMethod.GET)
    public String getUpdateCustomerPaymentForm(Model model, @PathVariable("id") Long id, Pageable pageable) {

        CustomerPaymentDTO  customerPaymentDTO = customerPaymentService.findById(id);

        getListCustomer(model,pageable);
        if (customerPaymentDTO != null) {
            model.addAttribute("payment", customerPaymentDTO);
        }
        return "admin/customer/payment-update";
    }

    /**
     * Ninh Trang An
     * @param customerPaymentDTO
     * @param result
     * @param model
     * @return
     */
    @PostMapping("/customer/payment/update")
    public String updateCustomerPayment(
            @ModelAttribute("payment") @Valid final CustomerPaymentDTO customerPaymentDTO, final BindingResult result,
            Model model, Pageable pageable) {
        boolean error = false;

        if(result.hasErrors()){
            error = true;
        }

        CustomerPaymentDTO customerDTO = customerPaymentService.findById(customerPaymentDTO.getId());

        for (Integer i : customerPaymentService.checkFieldExists(
                customerPaymentDTO.getCode())) {
            if (i.equals(1) && !customerPaymentDTO.getCode().equals(customerDTO.getCode())) {
                model.addAttribute("errCode", "Mã hóa đơn đã tồn tại");
                error = true;
            }
        }

        if (error) {
            getListCustomer(model,pageable);

            model.addAttribute("payment", customerPaymentDTO);
            return "admin/customer/payment-update";
        }

        customerPaymentService.save(customerPaymentDTO);

        return "redirect:/admin/customer/payment/list";
    }


    /**
     * Ninh Trang An
     * Delete customer_payment screen
     *
     */
    @PostMapping("/customer/payment/delete")
    public String deleteCustomerPayment(@ModelAttribute("payment") CustomerPaymentDTO customerPaymentDTO) {

        customerPaymentService.delete(customerPaymentDTO.getId());
        return "redirect:/admin/customer/payment/list";
    }


}
