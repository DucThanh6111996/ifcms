package com.ifw.cms.controller;

import com.ifw.cms.entity.Province;
import com.ifw.cms.repository.ProvinceRepository;
import com.ifw.cms.service.AppParamsService;
import com.ifw.cms.service.dto.AppParamsDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * Created by ITSOL SWIB Dev Team.
 */
@Controller
@RequestMapping(value = "/admin/system")
public class SystemController {

    @Autowired
    private AppParamsService appParamsService;

    @Autowired
    private ProvinceRepository provinceRepository;

    /**
     * Get app_params filter by key
     * @param model
     * @param pageable
     * @param groupCode
     * @param key
     * @return
     */
    @GetMapping("/app-param/{groupCode}")
    public String getAppParams(Model model,
                               Pageable pageable,
                               @PathVariable("groupCode") Optional<String> groupCode,
                               @RequestParam("key") Optional<String> key) {
        if (groupCode.isPresent()) {
            model.addAttribute("groupCode", groupCode.get());
            Page<AppParamsDTO> results = appParamsService.findByGroupCodePaged(groupCode.get(), key.orElse(""), pageable);
            model.addAttribute("page", results);
        }

        return "admin/system/param-list";
    }

    @GetMapping("/province/list")
    public String getProvinces(Model model,
                               Pageable pageable,
                               @RequestParam("key") Optional<String> key) {
        Page<Province> results = provinceRepository.findAll(pageable);

        model.addAttribute("page", results);

        return "admin/system/province-list";
    }

}
