package com.ifw.cms.controller;

import com.ifw.cms.service.BiddingDocumentsService;
import com.ifw.cms.service.CustomerBranchService;
import com.ifw.cms.service.CustomerService;
import com.ifw.cms.service.dto.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/admin")
public class BiddingDocumentsController {

    private BiddingDocumentsService biddingDocumentsService;
    private CustomerBranchService customerBranchService;
    private CustomerService customerService;

    public BiddingDocumentsController (BiddingDocumentsService biddingDocumentsService,
                                       CustomerBranchService customerBranchService,
                                       CustomerService customerService)
    {
        this.biddingDocumentsService = biddingDocumentsService;
        this.customerBranchService = customerBranchService;
        this.customerService = customerService;
    }

    /**
     * Ninh Trang An
     * List all BiddingDocuments, filter by key
     *
     */
    @RequestMapping(value = "/biddoc/list", method = RequestMethod.GET)
    public String listAllBiddoc(Model model, Pageable pageable,
                                  @RequestParam("key") Optional<String> key) {
        Page<BiddingDocumentsDTO> page = biddingDocumentsService.search(pageable, key.orElse("")
                , key.orElse(""));
        model.addAttribute("page", page);
        return "admin/biddoc/biddingdocuments-list";
    }

    /**
     * Ninh Trang An
     * Show create new biddingdocuments screen
     *
     * @param
     * @return
     */
    @RequestMapping(value = "/biddoc/new", method = RequestMethod.GET)
    public String getAddBiddocForm(Model model,String code,Pageable pageable) {
        BiddingDocumentsDTO biddingDocumentsDTO = new BiddingDocumentsDTO();
        biddingDocumentsDTO.setCode(code);
        model.addAttribute("biddoc", biddingDocumentsDTO);
        getListCustomerAndBranch(model, pageable);
        return "admin/biddoc/biddingdocuments-add";
    }

    private void getListCustomerAndBranch(Model model,Pageable pageable)
    {
        Page<CustomerBranchDTO> customerList = customerBranchService.findAll(pageable);
        Page<CustomerDTO> customerDTOS = customerService.findAll(pageable);
        model.addAttribute("customerBranch", customerList);
        model.addAttribute("customerForm", customerDTOS);
    }

    /**
     * Ninh Trang An
     * save biddoc
     * @param biddingDocumentsDTO
     * @param result
     * @param model
     * @param pageable
     * @return
     */
    @PostMapping("/biddoc/save")
    public String addBiddoc(@ModelAttribute("biddoc") @Valid final BiddingDocumentsDTO biddingDocumentsDTO,
                                     final BindingResult result, Model model, Pageable pageable) {
        boolean error = false;

        if (result.hasErrors()) {
            model.addAttribute("biddoc", biddingDocumentsDTO);
            error = true;
        }

        for (Integer integer : biddingDocumentsService.checkFieldExists(
                biddingDocumentsDTO.getCode())) {
            if (integer.equals(Integer.parseInt("1"))) {
                model.addAttribute("errorCode", "Mã hồ sơ đã tồn tại");
                error = true;
            }
            model.addAttribute("biddoc", biddingDocumentsDTO);
        }
        if(error) {
            model.addAttribute("biddoc", biddingDocumentsDTO);
            getListCustomerAndBranch(model, pageable);
            return "admin/biddoc/biddingdocuments-add";
        }
        biddingDocumentsService.save(biddingDocumentsDTO);

        return "redirect:/admin/biddoc/list";
    }

    /**
     * NinhTrangAn
     * Show update biddingdocuments screen
     *
     * @param model
     * @param id
     * @return
             */
    @RequestMapping(value = "/biddoc/update/{id}", method = RequestMethod.GET)
    public String getUpdateBiddocForm(Model model, @PathVariable("id") Long id, Pageable pageable) {
        BiddingDocumentsDTO biddingDocumentsDTO = biddingDocumentsService.findById(id);
        if (biddingDocumentsDTO != null) {
            getListCustomerAndBranch(model, pageable);
            model.addAttribute("biddoc", biddingDocumentsDTO);
        }
        return "admin/biddoc/biddingdocuments-update";
    }

    /**
     * Ninh Trang An
     * @param biddingDocumentsDTO
     * @param result
     * @param model
     * @param pageable
     * @return
     */
    @PostMapping("/biddoc/update")
    public String updateBiddoc(@ModelAttribute("biddoc") @Valid final BiddingDocumentsDTO biddingDocumentsDTO,
                               final BindingResult result, Model model, Pageable pageable) {
        boolean error = false;
        if(result.hasErrors()){
            error = true;
        }
        BiddingDocumentsDTO biddingDocumentsDTO1 = biddingDocumentsService.findById(biddingDocumentsDTO.getId());
        for (Integer i : biddingDocumentsService.checkFieldExists(
                biddingDocumentsDTO.getCode())) {
            if (i.equals(1) && !biddingDocumentsDTO.getCode().equals(biddingDocumentsDTO1.getCode())) {
                model.addAttribute("errorCode", "Mã hồ sơ đã tồn tại");
                error = true;
            }
        }
        if (error) {
            getListCustomerAndBranch(model, pageable);
            model.addAttribute("biddoc", biddingDocumentsDTO);
            return "admin/biddoc/biddingdocuments-update";
        }
        biddingDocumentsService.save(biddingDocumentsDTO);
        return "redirect:/admin/biddoc/list";

    }
    /**Ninh Trang An
     * Get biddoc detail
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = "/biddoc/detail/{id}", method = RequestMethod.GET)
    public String getBiddocDetail(Model model, @PathVariable Long id) {

        BiddingDocumentsDTO  biddingDocumentsDTO = biddingDocumentsService.findById(id);
        CustomerBranchDTO customerBranchDTO = customerBranchService.findById(biddingDocumentsDTO.getCustomerBranchId());

        if (biddingDocumentsDTO != null) {
            model.addAttribute("nameBranch",customerBranchDTO.getName());
            model.addAttribute("biddoc", biddingDocumentsDTO);
        }
        return "admin/biddoc/biddingdocuments-detail";
    }

    /**
     * Ninh Trang An
     * Delete biddingdocuments screen
     *
     */
    @PostMapping("/biddoc/delete")
    public String deleteBiddoc(@ModelAttribute("biddoc") BiddingDocumentsDTO biddingDocumentsDTO) {

        biddingDocumentsService.delete(biddingDocumentsDTO.getId());
        return "redirect:/admin/biddoc/list";
    }

}
