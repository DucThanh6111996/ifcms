package com.ifw.cms.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Created by ITSOL SWIB Dev Team.
 */
@ControllerAdvice
public class LocalDateControllerAdvice {
    final String vietnameseDatePattern = "MM/dd/yyyy";

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        DateTimeFormatter vietnamDateFormatter = DateTimeFormatter.ofPattern(vietnameseDatePattern);

        binder.registerCustomEditor(LocalDate.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                if (StringUtils.isNotEmpty(text)) {
                    setValue(LocalDate.parse(text, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                }
            }
        });
    }
}
