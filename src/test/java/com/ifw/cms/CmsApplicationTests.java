package com.ifw.cms;

import com.ifw.cms.service.CustomerService;
import com.ifw.cms.service.HrmEmployeeService;
import com.ifw.cms.service.MailService;

import com.ifw.cms.service.dto.CustomerDTO;
import com.ifw.cms.service.dto.HrmEmployeeDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class CmsApplicationTests {

    @Autowired
    MailService mailService;

    @Autowired
    HrmEmployeeService hrmEmployeeService;

    @Autowired
    CustomerService customerService;

    @Test
    public void sout() {
        // Lấy danh sách nhân viên sắp hết hạn hợp đồng
       List<HrmEmployeeDTO> listEmployeeExpired = hrmEmployeeService.getEmployeeExpired();

      // Lấy danh sách quản trị viên
       List<HrmEmployeeDTO> listAdmin = hrmEmployeeService.getAdmin();

       // Gui mail
       for (HrmEmployeeDTO admin : listAdmin) {
           mailService.sendMailExpired(admin, listEmployeeExpired);
       }

        // danh sach khach hang
        List<CustomerDTO> listCustomer = customerService.getListCustomer();

        // Gui mail
        for (CustomerDTO customer : listCustomer) {
            mailService.sendMailCustomer(customer);
        }
    }
/*
        List<CustomerDTO> listCustomer = customerService.getListCustomer();

        List<CustomerDTO> listContract = customerService.getListContract();
        // Gui mail
        for (CustomerDTO customer : listCustomer) {
            mailService.sendMailCustomer(customer);
        }
    }*/
}
